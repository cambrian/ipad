// javaPadCntrItem.cpp : implementation of the CJavaPadCntrItem class
//

#include "stdafx.h"
#include "javaPad.h"

#include "javaPadDoc.h"
#include "javaPadView.h"
#include "javaPadCntrItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CJavaPadCntrItem implementation

IMPLEMENT_SERIAL(CJavaPadCntrItem, CRichEditCntrItem, 0)

CJavaPadCntrItem::CJavaPadCntrItem(REOBJECT* preo, CJavaPadDoc* pContainer)
	: CRichEditCntrItem(preo, pContainer)
{
	// TODO: add one-time construction code here
	
}

CJavaPadCntrItem::~CJavaPadCntrItem()
{
	// TODO: add cleanup code here
	
}

/////////////////////////////////////////////////////////////////////////////
// CJavaPadCntrItem diagnostics

#ifdef _DEBUG
void CJavaPadCntrItem::AssertValid() const
{
	CRichEditCntrItem::AssertValid();
}

void CJavaPadCntrItem::Dump(CDumpContext& dc) const
{
	CRichEditCntrItem::Dump(dc);
}
#endif

/////////////////////////////////////////////////////////////////////////////
