// CntrItem.h : interface of the CJavaPadCntrItem class
//

#if !defined(AFX_CNTRITEM_H__FEF2B14C_36FD_11D3_93E1_00207817FF60__INCLUDED_)
#define AFX_CNTRITEM_H__FEF2B14C_36FD_11D3_93E1_00207817FF60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CJavaPadDoc;
class CJavaPadView;

class CJavaPadCntrItem : public CRichEditCntrItem
{
	DECLARE_SERIAL(CJavaPadCntrItem)

// Constructors
public:
	CJavaPadCntrItem(REOBJECT* preo = NULL, CJavaPadDoc* pContainer = NULL);
		// Note: pContainer is allowed to be NULL to enable IMPLEMENT_SERIALIZE.
		//  IMPLEMENT_SERIALIZE requires the class have a constructor with
		//  zero arguments.  Normally, OLE items are constructed with a
		//  non-NULL document pointer.

// Attributes
public:
	CJavaPadDoc* GetDocument()
		{ return (CJavaPadDoc*)CRichEditCntrItem::GetDocument(); }
	CJavaPadView* GetActiveView()
		{ return (CJavaPadView*)CRichEditCntrItem::GetActiveView(); }

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJavaPadCntrItem)
	public:
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	~CJavaPadCntrItem();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CNTRITEM_H__FEF2B14C_36FD_11D3_93E1_00207817FF60__INCLUDED_)
