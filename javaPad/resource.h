//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by javaPad.rc
//
#define IDR_SRVR_INPLACE                4
#define IDR_SRVR_EMBEDDED               5
#define IDR_CNTR_INPLACE                6
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDP_FAILED_TO_CREATE            102
#define IDR_MAINFRAME                   128
#define IDR_JAVAPATYPE                  129
#define IDB_MENUCHECK                   131
#define IDR_MRUFILE                     149
#define IDR_RESERVOIR                   150
#define IDR_SYSTEMBAR                   151
#define ID_CANCEL_EDIT_CNTR             32768
#define ID_CANCEL_EDIT_SRVR             32769
#define ID_TOOLS_OPTIONS                32781
#define ID_TOOLS_BEAUTIFY               32782
#define ID_BUILD_COMPILE_CPP            32783
#define ID_BUILD_COMPILE_JAVA           32784
#define ID_PROJECT_OPEN                 32785
#define ID_PROJECT_SAVE                 32787
#define ID_DEBUG_GO                     32789
#define ID_DEBUG_STOP                   32790
#define ID_SCRIPT_NEW                   32791
#define ID_SCRIPT_RUN                   32793
#define ID_DEBUG_BREAKPOINTS            32794
#define ID_BUILD_RUN                    32796
#define ID_BUILD_COMPILE                32797

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
