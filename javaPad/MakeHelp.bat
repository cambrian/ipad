@echo off
REM -- First make map file from Microsoft Visual C++ generated resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by JAVAPAD.HPJ. >"hlp\javaPad.hm"
echo. >>"hlp\javaPad.hm"
echo // Commands (ID_* and IDM_*) >>"hlp\javaPad.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\javaPad.hm"
echo. >>"hlp\javaPad.hm"
echo // Prompts (IDP_*) >>"hlp\javaPad.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\javaPad.hm"
echo. >>"hlp\javaPad.hm"
echo // Resources (IDR_*) >>"hlp\javaPad.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\javaPad.hm"
echo. >>"hlp\javaPad.hm"
echo // Dialogs (IDD_*) >>"hlp\javaPad.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\javaPad.hm"
echo. >>"hlp\javaPad.hm"
echo // Frame Controls (IDW_*) >>"hlp\javaPad.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\javaPad.hm"
REM -- Make help for Project JAVAPAD


echo Building Win32 Help files
start /wait hcw /C /E /M "hlp\javaPad.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\javaPad.hlp" goto :Error
if not exist "hlp\javaPad.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\javaPad.hlp" Debug
if exist Debug\nul copy "hlp\javaPad.cnt" Debug
if exist Release\nul copy "hlp\javaPad.hlp" Release
if exist Release\nul copy "hlp\javaPad.cnt" Release
echo.
goto :done

:Error
echo hlp\javaPad.hpj(1) : error: Problem encountered creating help file

:done
echo.
