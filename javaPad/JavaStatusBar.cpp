// JavaStatusBar.cpp : implementation file
//

#include "stdafx.h"
#include "javaPad.h"
#include "JavaStatusBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CJavaStatusBar

CJavaStatusBar::CJavaStatusBar()
{
}

CJavaStatusBar::~CJavaStatusBar()
{
}


BEGIN_MESSAGE_MAP(CJavaStatusBar, CIceStatusBar)
	//{{AFX_MSG_MAP(CJavaStatusBar)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CJavaStatusBar message handlers
