// javaPadView.cpp : implementation of the CJavaPadView class
//

#include "stdafx.h"
#include "javaPad.h"

#include "javaPadDoc.h"
#include "javaPadCntrItem.h"
#include "javaPadView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CJavaPadView

IMPLEMENT_DYNCREATE(CJavaPadView, CIceEditView)

BEGIN_MESSAGE_MAP(CJavaPadView, CIceEditView)
	//{{AFX_MSG_MAP(CJavaPadView)
	ON_WM_DESTROY()
	ON_COMMAND(ID_CANCEL_EDIT_SRVR, OnCancelEditSrvr)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CIceEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CIceEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CIceEditView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CJavaPadView construction/destruction

CJavaPadView::CJavaPadView()
{
	EnableAutomation();

    m_nWordWrap = WrapNone;

    m_chComment = 1;
	m_bCaseSensitive = TRUE;
	m_bChangeCase = FALSE;

	SetStringQuotes(_T("\""));

	SetTextColor(RGB(0,0,0), FALSE);
	SetKeywordColor(RGB(0,0,255), FALSE);
	SetConstantColor(RGB(0,0,0), TRUE);
	SetCommentColor(RGB(0,128,0), FALSE);
	SetNumberColor(RGB(255,0,255), FALSE);
	SetStringColor(RGB(255,0,0), FALSE);

	m_bInForcedChange = FALSE;
	m_changeType = ctUndo;
	m_crOldSel.cpMin = m_crOldSel.cpMax = 0;
}

CJavaPadView::~CJavaPadView()
{
}

BOOL CJavaPadView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

    /*
	cs.style |= WS_HSCROLL;       // No word-wrapping !!!!!
	cs.style |= WS_VSCROLL;
    cs.style |= ES_MULTILINE ;
    cs.style |= ES_AUTOHSCROLL ;
    cs.style |= ES_AUTOVSCROLL ;
    */

    BOOL bPreCreated = CIceEditView::PreCreateWindow(cs);

    // why is this not working?????????
    /*
	cs.style |= WS_HSCROLL;       // No word-wrapping !!!!!
	cs.style |= WS_VSCROLL;
    cs.style |= ES_MULTILINE ;
    cs.style |= ES_AUTOHSCROLL ;
    cs.style |= ES_AUTOVSCROLL ;
    */

	return bPreCreated;

}

void CJavaPadView::OnInitialUpdate()
{
    //GetRichEditCtrl().SetOptions(ECOOP_OR, ECO_AUTOHSCROLL);

	CIceEditView::OnInitialUpdate();


	// Set the printing margins (720 twips = 1/2 inch).
	SetMargins(CRect(720, 720, 720, 720));
}

/////////////////////////////////////////////////////////////////////////////
// CJavaPadView printing

BOOL CJavaPadView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}


void CJavaPadView::OnDestroy()
{
	// Deactivate the item on destruction; this is important
	// when a splitter view is being used.
   CIceEditView::OnDestroy();
   COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
   if (pActiveItem != NULL && pActiveItem->GetActiveView() == this)
   {
      pActiveItem->Deactivate();
      ASSERT(GetDocument()->GetInPlaceActiveItem(this) == NULL);
   }
}


/////////////////////////////////////////////////////////////////////////////
// OLE Server support

// The following command handler provides the standard keyboard
//  user interface to cancel an in-place editing session.  Here,
//  the server (not the container) causes the deactivation.
void CJavaPadView::OnCancelEditSrvr()
{
	GetDocument()->OnDeactivateUI(FALSE);
}

/////////////////////////////////////////////////////////////////////////////
// CJavaPadView diagnostics

#ifdef _DEBUG
void CJavaPadView::AssertValid() const
{
	CIceEditView::AssertValid();
}

void CJavaPadView::Dump(CDumpContext& dc) const
{
	CIceEditView::Dump(dc);
}

CJavaPadDoc* CJavaPadView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CJavaPadDoc)));
	return (CJavaPadDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CJavaPadView message handlers

void CJavaPadView::Initialize() 
{
	PARAFORMAT pf;
	pf.cbSize = sizeof(PARAFORMAT);
	pf.dwMask = PFM_TABSTOPS ;
	pf.cTabCount = MAX_TAB_STOPS;
	for( int itab = 0 ; itab < pf.cTabCount  ; itab++ )
		pf.rgxTabs[itab] = (itab + 1) * 1440/5 ;

	SetParaFormat( pf );

	CHARFORMAT cfDefault;
	cfDefault.cbSize = sizeof(cfDefault);
	cfDefault.dwEffects = CFE_PROTECTED; 
	cfDefault.dwMask = CFM_BOLD | CFM_FACE | CFM_SIZE | CFM_CHARSET | CFM_PROTECTED;
	cfDefault.yHeight = 200;
	cfDefault.bCharSet = 0xEE; 
	strcpy(cfDefault.szFaceName, _T("Courier New")); 

	GetRichEditCtrl().SetDefaultCharFormat(cfDefault);
	GetRichEditCtrl().SetEventMask(ENM_CHANGE | ENM_SELCHANGE | ENM_PROTECTED);
}

static LPCTSTR szKeywords = " "
"abstract boolean break byte byvalue case cast "
"catch char class const continue default do double "
"else extends false final finally float for future "
"generic goto if implements import inner instanceof "
"int interface long native new null operator outer "
"package private protected public rest return short "
"static super switch synchronized this throw throws "
"transient true try var void volatile while ";
 
static LPCTSTR szConstants = " main "
"clone equals finalize getClass hashCode notify "
"notifyAll toString wait ";


int CJavaPadView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceEditView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
    Initialize();

    SetCaseSensitive(TRUE);
	SetStringQuotes(_T("\""));
	SetSLComment(_T('/'),_T('/'));
	SetMLOpenComment(_T('/'),_T('*'));
	SetMLCloseComment(_T('*'),_T('/'));

    ClearKeywords();
	AddKeywords(szKeywords);
    ClearConstants();
	AddConstants(szConstants);
	
	return 0;
}

void CJavaPadView::OnFinalRelease() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CIceEditView::OnFinalRelease();
}
