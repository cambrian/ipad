// javaPadDoc.cpp : implementation of the CJavaPadDoc class
//

#include "stdafx.h"
#include "javaPad.h"

#include "javaPadDoc.h"
#include "javaPadCntrItem.h"
#include "javaPadSrvrItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CJavaPadDoc

IMPLEMENT_DYNCREATE(CJavaPadDoc, CIceEditDoc)

BEGIN_MESSAGE_MAP(CJavaPadDoc, CIceEditDoc)
	//{{AFX_MSG_MAP(CJavaPadDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Enable default OLE container implementation
	ON_UPDATE_COMMAND_UI(ID_OLE_EDIT_LINKS, CIceEditDoc::OnUpdateEditLinksMenu)
	ON_COMMAND(ID_OLE_EDIT_LINKS, CIceEditDoc::OnEditLinks)
	ON_UPDATE_COMMAND_UI_RANGE(ID_OLE_VERB_FIRST, ID_OLE_VERB_LAST, CIceEditDoc::OnUpdateObjectVerbMenu)
	ON_COMMAND(ID_FILE_SEND_MAIL, OnFileSendMail)
	ON_UPDATE_COMMAND_UI(ID_FILE_SEND_MAIL, OnUpdateFileSendMail)
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CJavaPadDoc, CIceEditDoc)
	//{{AFX_DISPATCH_MAP(CJavaPadDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//      DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IJavaPad to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {FEF2B139-36FD-11D3-93E1-00207817FF60}
static const IID IID_IJavaPad =
{ 0xfef2b139, 0x36fd, 0x11d3, { 0x93, 0xe1, 0x0, 0x20, 0x78, 0x17, 0xff, 0x60 } };

BEGIN_INTERFACE_MAP(CJavaPadDoc, CRichEditDoc)
	INTERFACE_PART(CJavaPadDoc, IID_IJavaPad, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CJavaPadDoc construction/destruction

CJavaPadDoc::CJavaPadDoc()
{
	// Use OLE compound files
	//EnableCompoundFile();

	// TODO: add one-time construction code here

	EnableAutomation();

	//AfxOleLockApp();
}

CJavaPadDoc::~CJavaPadDoc()
{
	//AfxOleUnlockApp();
}

BOOL CJavaPadDoc::OnNewDocument()
{
	if (!CIceEditDoc::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

CRichEditCntrItem* CJavaPadDoc::CreateClientItem(REOBJECT* preo) const
{
	// cast away constness of this
	return new CJavaPadCntrItem(preo, (CJavaPadDoc*) this);
}

/////////////////////////////////////////////////////////////////////////////
// CJavaPadDoc server implementation

COleServerItem* CJavaPadDoc::OnGetEmbeddedItem()
{
	// OnGetEmbeddedItem is called by the framework to get the COleServerItem
	//  that is associated with the document.  It is only called when necessary.

	CJavaPadSrvrItem* pItem = new CJavaPadSrvrItem(this);
	ASSERT_VALID(pItem);
	return pItem;
}



/////////////////////////////////////////////////////////////////////////////
// CJavaPadDoc serialization

void CJavaPadDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}

	// Calling the base class CRichEditDoc enables serialization
	//  of the container document's COleClientItem objects.
	// TODO: set CRichEditDoc::m_bRTF = FALSE if you are serializing as text
	CIceEditDoc::Serialize(ar);
}

/////////////////////////////////////////////////////////////////////////////
// CJavaPadDoc diagnostics

#ifdef _DEBUG
void CJavaPadDoc::AssertValid() const
{
	CIceEditDoc::AssertValid();
}

void CJavaPadDoc::Dump(CDumpContext& dc) const
{
	CIceEditDoc::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CJavaPadDoc commands

BOOL CJavaPadDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	if (!CIceEditDoc::OnOpenDocument(lpszPathName))
		return FALSE;
	
	// TODO: Add your specialized creation code here
	
	return TRUE;
}

BOOL CJavaPadDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CIceEditDoc::OnSaveDocument(lpszPathName);
}
