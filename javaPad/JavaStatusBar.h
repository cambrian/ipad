#if !defined(AFX_JAVASTATUSBAR_H__BE152745_3768_11D3_93E5_00207817FF60__INCLUDED_)
#define AFX_JAVASTATUSBAR_H__BE152745_3768_11D3_93E5_00207817FF60__INCLUDED_

#include "../icelib/IceStatusBar.h"


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// JavaStatusBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CJavaStatusBar window

class CJavaStatusBar : public CIceStatusBar
{
// Construction
public:
	CJavaStatusBar();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJavaStatusBar)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CJavaStatusBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CJavaStatusBar)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_JAVASTATUSBAR_H__BE152745_3768_11D3_93E5_00207817FF60__INCLUDED_)
