// javaPadView.h : interface of the CJavaPadView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_JAVAPADVIEW_H__FEF2B145_36FD_11D3_93E1_00207817FF60__INCLUDED_)
#define AFX_JAVAPADVIEW_H__FEF2B145_36FD_11D3_93E1_00207817FF60__INCLUDED_

#include "../icelib/IceEditView.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CJavaPadCntrItem;

class CJavaPadView : public CIceEditView
{
protected: // create from serialization only
	CJavaPadView();
	DECLARE_DYNCREATE(CJavaPadView)

    virtual void Initialize();

// Attributes
public:
	CJavaPadDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJavaPadView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnFinalRelease();
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CJavaPadView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CJavaPadView)
	afx_msg void OnDestroy();
	afx_msg void OnCancelEditSrvr();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in javaPadView.cpp
inline CJavaPadDoc* CJavaPadView::GetDocument()
   { return (CJavaPadDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_JAVAPADVIEW_H__FEF2B145_36FD_11D3_93E1_00207817FF60__INCLUDED_)
