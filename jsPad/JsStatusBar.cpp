// JsStatusBar.cpp : implementation file
//

#include "stdafx.h"
#include "jsPad.h"
#include "JsStatusBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CJsStatusBar

CJsStatusBar::CJsStatusBar()
{
}

CJsStatusBar::~CJsStatusBar()
{
}


BEGIN_MESSAGE_MAP(CJsStatusBar, CIceStatusBar)
	//{{AFX_MSG_MAP(CJsStatusBar)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CJsStatusBar message handlers
