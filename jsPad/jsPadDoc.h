// jsPadDoc.h : interface of the CJsPadDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_JSPADDOC_H__FEF2B106_36FD_11D3_93E1_00207817FF60__INCLUDED_)
#define AFX_JSPADDOC_H__FEF2B106_36FD_11D3_93E1_00207817FF60__INCLUDED_

#include "../icelib/IceEditDoc.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CJsPadSrvrItem;

class CJsPadDoc : public CIceEditDoc
{
protected: // create from serialization only
	CJsPadDoc();
	DECLARE_DYNCREATE(CJsPadDoc)

// Attributes
public:
	CJsPadSrvrItem* GetEmbeddedItem()
		{ return (CJsPadSrvrItem*)CRichEditDoc::GetEmbeddedItem(); }

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJsPadDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	protected:
	virtual COleServerItem* OnGetEmbeddedItem();
	//}}AFX_VIRTUAL
	virtual CRichEditCntrItem* CreateClientItem(REOBJECT* preo) const;

// Implementation
public:
	virtual ~CJsPadDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CJsPadDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CJsPadDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_JSPADDOC_H__FEF2B106_36FD_11D3_93E1_00207817FF60__INCLUDED_)
