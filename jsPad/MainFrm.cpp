// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "jsPad.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CIceFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CIceFrameWnd)
	ON_COMMAND_EX(ID_VIEW_TOOLS_BAR, OnBarCheck)
	ON_UPDATE_COMMAND_UI(ID_VIEW_TOOLS_BAR, OnUpdateControlBarMenu)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_INITMENU()
	ON_COMMAND(ID_TOOLS_BEAUTIFY, OnToolsBeautify)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_BEAUTIFY, OnUpdateToolsBeautify)
	ON_COMMAND(ID_TOOLS_OPTIONS, OnToolsOptions)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OPTIONS, OnUpdateToolsOptions)
	ON_COMMAND(ID_TOOLS_PARSE, OnToolsParse)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_PARSE, OnUpdateToolsParse)
	ON_COMMAND(ID_TOOLS_RUN, OnToolsRun)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_RUN, OnUpdateToolsRun)
	ON_COMMAND(ID_HELP, OnHelp)
	ON_UPDATE_COMMAND_UI(ID_HELP, OnUpdateHelp)
	ON_COMMAND(ID_HELP_FINDER, OnHelpFinder)
	ON_UPDATE_COMMAND_UI(ID_HELP_FINDER, OnUpdateHelpFinder)
	ON_COMMAND(ID_CONTEXT_HELP, OnContextHelp)
	ON_UPDATE_COMMAND_UI(ID_CONTEXT_HELP, OnUpdateContextHelp)
	ON_COMMAND(ID_HELP_CODE_SAMPLE, OnHelpCodeSample)
	ON_UPDATE_COMMAND_UI(ID_HELP_CODE_SAMPLE, OnUpdateHelpCodeSample)
	ON_COMMAND(ID_HELP_JS_WEB, OnHelpJsWeb)
	ON_UPDATE_COMMAND_UI(ID_HELP_JS_WEB, OnUpdateHelpJsWeb)
	//}}AFX_MSG_MAP
	// Global help commands
	//ON_COMMAND(ID_HELP_FINDER, CIceFrameWnd::OnHelpFinder)
	//ON_COMMAND(ID_HELP, CIceFrameWnd::OnHelp)
	//ON_COMMAND(ID_CONTEXT_HELP, CIceFrameWnd::OnContextHelp)
	//ON_COMMAND(ID_DEFAULT_HELP, CIceFrameWnd::OnHelpFinder)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	iceMenu.AddToolBarResource(IDR_TOOLSBAR);
}


CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CIceFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndToolBar.CreateEx(this) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() & ~CBRS_HIDE_INPLACE);
	//if (!m_wndToolsToolBar.CreateEx(this, ID_VIEW_TOOLS_BAR) ||
	if (!m_wndToolsToolBar.CreateEx(this, TBSTYLE_FLAT | TBSTYLE_TRANSPARENT , 
        WS_CHILD | WS_VISIBLE | CBRS_ALIGN_TOP, CRect(0, 0, 0, 0), ID_VIEW_TOOLS_BAR) ||
		!m_wndToolsToolBar.LoadToolBar(IDR_TOOLSBAR))
	{
		TRACE0("Failed to create tools toolbar\n");
		return -1;      // fail to create
	}
	m_wndToolsToolBar.SetBarStyle(m_wndToolsToolBar.GetBarStyle() & ~CBRS_HIDE_INPLACE);
	if (!m_wndDlgBar.Create(this, IDR_MAINFRAME, 
		CBRS_ALIGN_TOP, AFX_IDW_DIALOGBAR))
	{
		TRACE0("Failed to create dialogbar\n");
		return -1;		// fail to create
	}

	if (!m_wndReBar.Create(this) ||
		!m_wndReBar.AddBar(&m_wndToolBar) ||
		!m_wndReBar.AddBar(&m_wndToolsToolBar) ||
		!m_wndReBar.AddBar(&m_wndDlgBar))
	{
		TRACE0("Failed to create rebar\n");
		return -1;      // fail to create
	}
	m_wndReBar.SetBarStyle(m_wndReBar.GetBarStyle() & ~CBRS_HIDE_INPLACE);

	if (!m_wndStatusBar.CreateStatusBar(this, indicators, sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Remove this if you don't want tool tips
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolsToolBar.SetBarStyle(m_wndToolsToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndDlgBar.SetBarStyle(m_wndDlgBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);

    // System menu is somehow corrupted.... This is a quick fix.
    GetSystemMenu(false);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CIceFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CIceFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CIceFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::ActivateFrame(int nCmdShow) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CIceFrameWnd::ActivateFrame(nCmdShow);
}

void CMainFrame::RecalcLayout(BOOL bNotify) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CIceFrameWnd::RecalcLayout(bNotify);
}

void CMainFrame::OnInitMenu(CMenu* pMenu) 
{
	CIceFrameWnd::OnInitMenu(pMenu);
}



void CMainFrame::OnToolsBeautify() 
{
    AfxMessageBox(IDS_NOTIMPLEMENTED, MB_OK);	
}

void CMainFrame::OnUpdateToolsBeautify(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CMainFrame::OnToolsOptions() 
{
    AfxMessageBox(IDS_NOTIMPLEMENTED, MB_OK);	
}

void CMainFrame::OnUpdateToolsOptions(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CMainFrame::OnToolsParse() 
{
    AfxMessageBox(IDS_NOTIMPLEMENTED, MB_OK);	
}

void CMainFrame::OnUpdateToolsParse(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(FALSE);
}

void CMainFrame::OnToolsRun() 
{
    CString strFileName("");
    strFileName = GetActiveDocument()->GetPathName();

    if(!strFileName.IsEmpty())
    {
        int retVal = 0;
        // If the file has been saved
        if(!GetActiveDocument()->IsModified())
        {   
            retVal = (int) ShellExecute(this->GetSafeHwnd(),"open",strFileName,NULL,NULL,SW_SHOWNORMAL);
	        if(retVal > 32)
            {
                // call succeeded
            }
            else
            {
                DisplayError(retVal);
            }
        }
        // Else ask to save it first
        else
        {
            if(GetActiveDocument()->SaveModified())
            {
                retVal = (int) ShellExecute(this->GetSafeHwnd(),"open",strFileName,NULL,NULL,SW_SHOWNORMAL);
	            if(retVal > 32)
                {
                    // call succeeded
                }
                else
                {
                    DisplayError(retVal);
                }
            }
        }
    }
    else 
    {
        // Either the file is empty or it has never been saved since creation
        AfxMessageBox("Please Save Your Code First!", MB_OK);	
    }
}

void CMainFrame::DisplayError(int retVal)
{
    CString strMsg("");
    switch(retVal)
    {
    case 0:
        strMsg = "The operating system is out of memory or resources.";
        break;
    case ERROR_FILE_NOT_FOUND:
    //case SE_ERR_FNF:
        strMsg = "The specified file was not found.";
        break;
    case ERROR_PATH_NOT_FOUND :
    //case SE_ERR_PNF:
        strMsg = "The specified path was not found.";
        break;
    case ERROR_BAD_FORMAT :
        strMsg = "The .exe file is invalid (non-Win32 .exe or error in .exe image).";
        break;
    case SE_ERR_ACCESSDENIED:
        strMsg = "The operating system denied access to the specified file.";
        break;
    case SE_ERR_ASSOCINCOMPLETE:
        strMsg = "The file name association is incomplete or invalid.";
        break;
    case SE_ERR_DDEBUSY:
        strMsg = "The DDE transaction could not be completed because other DDE transactions were being processed.";
        break;
    case SE_ERR_DDEFAIL:
        strMsg = "The DDE transaction failed.";
        break;
    case SE_ERR_DDETIMEOUT:
        strMsg = "The DDE transaction could not be completed because the request timed out.";
        break;
    case SE_ERR_DLLNOTFOUND:
        strMsg = "The specified dynamic-link library was not found. ";
        break;
    case SE_ERR_NOASSOC:
        strMsg = "There is no application associated with the given file name extension.";
        break;
    case SE_ERR_OOM:
        strMsg = "There was not enough memory to complete the operation.";
        break;
    case SE_ERR_SHARE:
        strMsg = "A sharing violation occurred.";
        break;
    default:
        strMsg = "Unknown error occurred.";
        break;
    }
    AfxMessageBox(strMsg, MB_OK);
}

void CMainFrame::OnUpdateToolsRun(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CMainFrame::OnHelp() 
{
    // F1 Context-sensitive help

    //Temporarily
	if(NULL == HtmlHelp(this->GetSafeHwnd(),
		"help\\Jscript5.chm::htm\\about.htm",
		HH_DISPLAY_TOPIC,0))
	{
		CString  strFile = "help\\Jcript5.chm";  // home.htm???
		ShellExecute(NULL,"open",strFile,NULL,NULL,0);
	}

}

void CMainFrame::OnUpdateHelp(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CMainFrame::OnHelpFinder() 
{
    // Help topic
    /*
	if(NULL == HtmlHelp(this->GetSafeHwnd(),
		"help\\jsPad.chm",
		HH_DISPLAY_TOPIC,0))
	{
		CString  strFile = "help\\jsPad.chm";  // home.htm???
		ShellExecute(NULL,"open",strFile,NULL,NULL,0);
	}
    */

    //Temporarily
	if(NULL == HtmlHelp(this->GetSafeHwnd(),
		"help\\Wsh.chm::htm\\wsAboutWSH20.htm",
		HH_DISPLAY_TOPIC,0))
	{
		CString  strFile = "help\\Wsh.chm";  // home.htm???
		ShellExecute(NULL,"open",strFile,NULL,NULL,0);
	}

}

void CMainFrame::OnUpdateHelpFinder(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CMainFrame::OnContextHelp() 
{
	// TODO: Add your command handler code here
	
}

void CMainFrame::OnUpdateContextHelp(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(FALSE);
}

void CMainFrame::OnHelpCodeSample() 
{
	// TODO: Add your command handler code here
	
}

void CMainFrame::OnUpdateHelpCodeSample(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(FALSE);
}

void CMainFrame::OnHelpJsWeb() 
{
}

void CMainFrame::OnUpdateHelpJsWeb(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(FALSE);
}


