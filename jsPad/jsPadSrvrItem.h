// SrvrItem.h : interface of the CJsPadSrvrItem class
//

#if !defined(AFX_SRVRITEM_H__FEF2B10B_36FD_11D3_93E1_00207817FF60__INCLUDED_)
#define AFX_SRVRITEM_H__FEF2B10B_36FD_11D3_93E1_00207817FF60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CJsPadSrvrItem : public COleServerItem
{
	DECLARE_DYNAMIC(CJsPadSrvrItem)

// Constructors
public:
	CJsPadSrvrItem(CJsPadDoc* pContainerDoc);

// Attributes
	CJsPadDoc* GetDocument() const
		{ return (CJsPadDoc*)COleServerItem::GetDocument(); }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJsPadSrvrItem)
	public:
	virtual BOOL OnDraw(CDC* pDC, CSize& rSize);
	virtual BOOL OnGetExtent(DVASPECT dwDrawAspect, CSize& rSize);
	//}}AFX_VIRTUAL

// Implementation
public:
	~CJsPadSrvrItem();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SRVRITEM_H__FEF2B10B_36FD_11D3_93E1_00207817FF60__INCLUDED_)
