////////////////////////////////////////////////////////////
// Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
// This software is distributed on an "AS IS" basis, 
// WITHOUT WARRANTY OF ANY KIND, either express or implied.
////////////////////////////////////////////////////////////

var WshShell = WScript.CreateObject( "WScript.Shell" );
var strEnv = "Windir: " + WshShell.Environment("Process").Item("WINDIR") + "\n";
strEnv += "Path: " + WshShell.Environment("Process").Item("PATH") + "\n";
strEnv += "Prompt: " + WshShell.Environment("Process").Item("PROMPT") + "\n";
strEnv += "Windir: " + WshShell.ExpandEnvironmentStrings("%WINDIR%") + "\n";
WScript.Echo(strEnv);
WScript.DisconnectObject( WshShell );
