// jsPadCntrItem.cpp : implementation of the CJsPadCntrItem class
//

#include "stdafx.h"
#include "jsPad.h"

#include "jsPadDoc.h"
#include "jsPadView.h"
#include "jsPadCntrItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CJsPadCntrItem implementation

IMPLEMENT_SERIAL(CJsPadCntrItem, CRichEditCntrItem, 0)

CJsPadCntrItem::CJsPadCntrItem(REOBJECT* preo, CJsPadDoc* pContainer)
	: CRichEditCntrItem(preo, pContainer)
{
	// TODO: add one-time construction code here
	
}

CJsPadCntrItem::~CJsPadCntrItem()
{
	// TODO: add cleanup code here
	
}

/////////////////////////////////////////////////////////////////////////////
// CJsPadCntrItem diagnostics

#ifdef _DEBUG
void CJsPadCntrItem::AssertValid() const
{
	CRichEditCntrItem::AssertValid();
}

void CJsPadCntrItem::Dump(CDumpContext& dc) const
{
	CRichEditCntrItem::Dump(dc);
}
#endif

/////////////////////////////////////////////////////////////////////////////
