; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CJsPadView
LastTemplate=CToolBarCtrl
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "jsPad.h"
ODLFile=jsPad.odl
LastPage=0

ClassCount=9
Class1=CJsPadApp
Class2=CJsPadDoc
Class3=CJsPadView
Class4=CMainFrame
Class5=CInPlaceFrame

ResourceCount=10
Resource1=IDR_SYSTEMBAR
Resource2=IDR_SRVR_INPLACE
Resource4=IDR_SRVR_EMBEDDED
Resource5=IDR_MRUFILE
Class6=CAboutDlg
Resource3=IDR_RESERVOIR
Class7=CJsStatusBar
Class8=CJsToolBar
Resource6=IDR_CNTR_INPLACE
Resource7=IDR_TOOLSBAR
Resource8=IDD_ABOUTBOX
Class9=CFindDlgBar
Resource9=IDR_MAINFRAME
Resource10="IDR_JSPADTYPE_SRVR_IP"

[CLS:CJsPadApp]
Type=0
HeaderFile=jsPad.h
ImplementationFile=jsPad.cpp
Filter=N

[CLS:CJsPadDoc]
Type=0
HeaderFile=jsPadDoc.h
ImplementationFile=jsPadDoc.cpp
Filter=N
BaseClass=CRichEditDoc
VirtualFilter=DC
LastObject=ID_BUILD_COMPILE_CPP

[CLS:CJsPadView]
Type=0
HeaderFile=jsPadView.h
ImplementationFile=jsPadView.cpp
Filter=C
BaseClass=CIceEditView
VirtualFilter=VWC
LastObject=CJsPadView


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
BaseClass=CIceFrameWnd
VirtualFilter=fWC
LastObject=CMainFrame

[CLS:CInPlaceFrame]
Type=0
HeaderFile=IpFrame.h
ImplementationFile=IpFrame.cpp
Filter=T



[CLS:CAboutDlg]
Type=0
HeaderFile=jsPad.cpp
ImplementationFile=jsPad.cpp
Filter=D
LastObject=CAboutDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=6
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Control5=IDC_PHYSICAL_MEM,static,1342308352
Control6=IDC_DISK_SPACE,static,1342308352

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_PRINT
Command6=ID_FILE_PRINT_PREVIEW
Command7=ID_FILE_PRINT_SETUP
Command8=ID_FILE_SEND_MAIL
Command9=ID_FILE_MRU_FILE1
Command10=ID_APP_EXIT
Command11=ID_EDIT_UNDO
Command12=ID_EDIT_CUT
Command13=ID_EDIT_COPY
Command14=ID_EDIT_PASTE
Command15=ID_EDIT_SELECT_ALL
Command16=ID_EDIT_FIND
Command17=ID_EDIT_REPEAT
Command18=ID_EDIT_REPLACE
Command19=ID_VIEW_TOOLBAR
Command20=ID_VIEW_TOOLS_BAR
Command21=ID_VIEW_STATUS_BAR
Command22=ID_VIEW_OUTPUT
Command23=ID_VIEW_LOG
Command24=ID_TOOLS_BEAUTIFY
Command25=ID_TOOLS_PARSE
Command26=ID_TOOLS_RUN
Command27=ID_TOOLS_OPTIONS
Command28=ID_HELP_FINDER
Command29=ID_HELP_TUTORIAL
Command30=ID_HELP_JS_WEB
Command31=ID_HELP_CODE_SAMPLE
Command32=ID_APP_ABOUT
CommandCount=32

[MNU:IDR_CNTR_INPLACE]
Type=1
Class=CJsPadView
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_PRINT
Command6=ID_FILE_PRINT_PREVIEW
Command7=ID_FILE_PRINT_SETUP
Command8=ID_FILE_SEND_MAIL
Command9=ID_FILE_MRU_FILE1
Command10=ID_APP_EXIT
CommandCount=10

[MNU:IDR_SRVR_INPLACE]
Type=1
Class=CJsPadView
Command1=ID_EDIT_UNDO
Command2=ID_EDIT_CUT
Command3=ID_EDIT_COPY
Command4=ID_EDIT_PASTE
Command5=ID_EDIT_PASTE_SPECIAL
Command6=ID_EDIT_SELECT_ALL
Command7=ID_EDIT_FIND
Command8=ID_EDIT_REPEAT
Command9=ID_EDIT_REPLACE
Command10=ID_OLE_INSERT_NEW
Command11=ID_OLE_EDIT_LINKS
Command12=ID_OLE_EDIT_PROPERTIES
Command13=ID_OLE_VERB_FIRST
Command14=ID_VIEW_TOOLBAR
Command15=ID_HELP_FINDER
Command16=ID_APP_ABOUT
CommandCount=16

[TB:IDR_SRVR_INPLACE]
Type=1
Class=CJsPadView
Command1=ID_EDIT_CUT
Command2=ID_EDIT_COPY
Command3=ID_EDIT_PASTE
Command4=ID_APP_ABOUT
Command5=ID_CONTEXT_HELP
CommandCount=5

[MNU:IDR_SRVR_EMBEDDED]
Type=1
Class=CJsPadView
Command1=ID_FILE_UPDATE
Command2=ID_FILE_SAVE_COPY_AS
Command3=ID_FILE_PRINT
Command4=ID_FILE_PRINT_PREVIEW
Command5=ID_FILE_PRINT_SETUP
Command6=ID_FILE_SEND_MAIL
Command7=ID_APP_EXIT
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_EDIT_PASTE_SPECIAL
Command13=ID_EDIT_SELECT_ALL
Command14=ID_EDIT_FIND
Command15=ID_EDIT_REPEAT
Command16=ID_EDIT_REPLACE
Command17=ID_OLE_INSERT_NEW
Command18=ID_OLE_EDIT_LINKS
Command19=ID_OLE_EDIT_PROPERTIES
Command20=ID_OLE_VERB_FIRST
Command21=ID_VIEW_TOOLBAR
Command22=ID_VIEW_STATUS_BAR
Command23=ID_HELP_FINDER
Command24=ID_APP_ABOUT
CommandCount=24

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_EDIT_SELECT_ALL
Command2=ID_EDIT_COPY
Command3=ID_EDIT_FIND
Command4=ID_EDIT_REPLACE
Command5=ID_FILE_NEW
Command6=ID_FILE_OPEN
Command7=ID_FILE_PRINT
Command8=ID_FILE_SAVE
Command9=ID_EDIT_PASTE
Command10=ID_EDIT_UNDO
Command11=ID_EDIT_CUT
Command12=ID_CANCEL_EDIT_CNTR
Command13=ID_HELP
Command14=ID_CONTEXT_HELP
Command15=ID_EDIT_REPEAT
Command16=ID_TOOLS_RUN
Command17=ID_NEXT_PANE
Command18=ID_PREV_PANE
Command19=ID_TOOLS_PARSE
Command20=ID_EDIT_COPY
Command21=ID_EDIT_PASTE
Command22=ID_OLE_EDIT_PROPERTIES
Command23=ID_EDIT_CUT
Command24=ID_EDIT_UNDO
CommandCount=24

[ACL:IDR_CNTR_INPLACE]
Type=1
Class=CJsPadView
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_NEXT_PANE
Command6=ID_PREV_PANE
Command7=ID_CONTEXT_HELP
Command8=ID_HELP
Command9=ID_CANCEL_EDIT_CNTR
CommandCount=9

[ACL:IDR_SRVR_INPLACE]
Type=1
Class=CJsPadView
Command1=ID_EDIT_UNDO
Command2=ID_EDIT_CUT
Command3=ID_EDIT_COPY
Command4=ID_EDIT_PASTE
Command5=ID_EDIT_SELECT_ALL
Command6=ID_EDIT_FIND
Command7=ID_EDIT_REPEAT
Command8=ID_EDIT_REPLACE
Command9=ID_OLE_EDIT_PROPERTIES
Command10=ID_EDIT_UNDO
Command11=ID_EDIT_CUT
Command12=ID_EDIT_COPY
Command13=ID_EDIT_PASTE
Command14=ID_CONTEXT_HELP
Command15=ID_HELP
Command16=ID_CANCEL_EDIT_SRVR
CommandCount=16

[ACL:IDR_SRVR_EMBEDDED]
Type=1
Class=CJsPadView
Command1=ID_FILE_UPDATE
Command2=ID_FILE_PRINT
Command3=ID_EDIT_UNDO
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_EDIT_SELECT_ALL
Command8=ID_EDIT_FIND
Command9=ID_EDIT_REPEAT
Command10=ID_EDIT_REPLACE
Command11=ID_OLE_EDIT_PROPERTIES
Command12=ID_EDIT_UNDO
Command13=ID_EDIT_CUT
Command14=ID_EDIT_COPY
Command15=ID_EDIT_PASTE
Command16=ID_NEXT_PANE
Command17=ID_PREV_PANE
Command18=ID_CONTEXT_HELP
Command19=ID_HELP
Command20=ID_CANCEL_EDIT_CNTR
CommandCount=20

[DLG:IDR_MAINFRAME]
Type=1
Class=?
ControlCount=2
Control1=IDC_STATIC,static,1342308352
Control2=IDC_FIND_STRING,combobox,1344340226

[DLG:"IDR_JSPADTYPE_SRVR_IP"]
Type=1
Class=?
ControlCount=1
Control1=IDC_STATIC,static,1342308352

[CLS:CJsStatusBar]
Type=0
HeaderFile=JsStatusBar.h
ImplementationFile=JsStatusBar.cpp
BaseClass=CStatusBarCtrl
Filter=W

[CLS:CJsToolBar]
Type=0
HeaderFile=JsToolBar.h
ImplementationFile=JsToolBar.cpp
BaseClass=CToolBarCtrl
Filter=W

[TB:IDR_RESERVOIR]
Type=1
Class=?
Command1=ID_APP_EXIT
Command2=ID_PROJECT_OPEN
Command3=ID_PROJECT_SAVE
Command4=ID_WINDOW_NEW
Command5=ID_EDIT_FIND
Command6=ID_EDIT_REPEAT
Command7=ID_EDIT_CLEAR
Command8=ID_APP_ABOUT
Command9=ID_BUILD_COMPILE
Command10=ID_BUILD_COMPILE_CPP
Command11=ID_BUILD_COMPILE_JAVA
Command12=ID_BUILD_RUN
Command13=ID_DEBUG_GO
Command14=ID_DEBUG_STOP
Command15=ID_DEBUG_BREAKPOINTS
Command16=ID_SCRIPT_NEW
Command17=ID_SCRIPT_RUN
Command18=ID_FILE_SEND_MAIL
CommandCount=18

[TB:IDR_SYSTEMBAR]
Type=1
Class=?
CommandCount=0

[TB:IDR_TOOLSBAR]
Type=1
Class=?
Command1=ID_TOOLS_PARSE
Command2=ID_TOOLS_RUN
Command3=ID_TOOLS_OPTIONS
CommandCount=3

[CLS:CFindDlgBar]
Type=0
HeaderFile=FindDlgBar.h
ImplementationFile=FindDlgBar.cpp
BaseClass=CToolBarCtrl
Filter=W
VirtualFilter=YWC
LastObject=CFindDlgBar

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_FILE_PRINT_PREVIEW
Command6=ID_EDIT_UNDO
Command7=ID_EDIT_CUT
Command8=ID_EDIT_COPY
Command9=ID_EDIT_PASTE
Command10=ID_HELP_FINDER
Command11=ID_HELP
Command12=ID_CONTEXT_HELP
CommandCount=12

[TB:IDR_MRUFILE]
Type=1
Class=?
Command1=ID_FILE_MRU_FILE1
Command2=ID_FILE_MRU_FILE2
Command3=ID_FILE_MRU_FILE3
Command4=ID_FILE_MRU_FILE4
Command5=ID_FILE_MRU_FILE5
Command6=ID_FILE_MRU_FILE1
Command7=ID_FILE_MRU_FILE7
Command8=ID_FILE_MRU_FILE8
Command9=ID_FILE_MRU_FILE1
Command10=ID_FILE_MRU_FILE10
CommandCount=10

