#if !defined(AFX_FINDDLGBAR_H__A1F1E7A4_39C8_11D3_93E5_00207817FF60__INCLUDED_)
#define AFX_FINDDLGBAR_H__A1F1E7A4_39C8_11D3_93E5_00207817FF60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FindDlgBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFindDlgBar window

class CFindDlgBar : public CDialogBar
{
// Construction
public:
	CFindDlgBar();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFindDlgBar)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFindDlgBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CFindDlgBar)
	afx_msg void OnSelchangeFindString();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FINDDLGBAR_H__A1F1E7A4_39C8_11D3_93E5_00207817FF60__INCLUDED_)
