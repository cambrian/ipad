@echo off
REM -- First make map file from Microsoft Visual C++ generated resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by JSPAD.HPJ. >"hlp\jsPad.hm"
echo. >>"hlp\jsPad.hm"
echo // Commands (ID_* and IDM_*) >>"hlp\jsPad.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\jsPad.hm"
echo. >>"hlp\jsPad.hm"
echo // Prompts (IDP_*) >>"hlp\jsPad.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\jsPad.hm"
echo. >>"hlp\jsPad.hm"
echo // Resources (IDR_*) >>"hlp\jsPad.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\jsPad.hm"
echo. >>"hlp\jsPad.hm"
echo // Dialogs (IDD_*) >>"hlp\jsPad.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\jsPad.hm"
echo. >>"hlp\jsPad.hm"
echo // Frame Controls (IDW_*) >>"hlp\jsPad.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\jsPad.hm"
REM -- Make help for Project JSPAD


echo Building Win32 Help files
start /wait hcw /C /E /M "hlp\jsPad.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\jsPad.hlp" goto :Error
if not exist "hlp\jsPad.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\jsPad.hlp" Debug
if exist Debug\nul copy "hlp\jsPad.cnt" Debug
if exist Release\nul copy "hlp\jsPad.hlp" Release
if exist Release\nul copy "hlp\jsPad.cnt" Release
echo.
goto :done

:Error
echo hlp\jsPad.hpj(1) : error: Problem encountered creating help file

:done
echo.
