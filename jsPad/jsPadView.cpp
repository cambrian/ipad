// jsPadView.cpp : implementation of the CJsPadView class
//

#include "stdafx.h"
#include "jsPad.h"

#include "jsPadDoc.h"
#include "jsPadCntrItem.h"
#include "jsPadView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CJsPadView

IMPLEMENT_DYNCREATE(CJsPadView, CIceEditView)

BEGIN_MESSAGE_MAP(CJsPadView, CIceEditView)
	//{{AFX_MSG_MAP(CJsPadView)
	ON_WM_DESTROY()
	ON_COMMAND(ID_CANCEL_EDIT_SRVR, OnCancelEditSrvr)
	ON_WM_CREATE()
	ON_COMMAND(ID_BUILD_RUN, OnBuildRun)
	ON_UPDATE_COMMAND_UI(ID_BUILD_RUN, OnUpdateBuildRun)
	ON_COMMAND(ID_HELP_TUTORIAL, OnHelpTutorial)
	ON_UPDATE_COMMAND_UI(ID_HELP_TUTORIAL, OnUpdateHelpTutorial)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CIceEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CIceEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CIceEditView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CJsPadView construction/destruction

CJsPadView::CJsPadView()
{
	EnableAutomation();

    m_nWordWrap = WrapNone;

    m_chComment = 1;
	m_bCaseSensitive = TRUE;
	m_bChangeCase = FALSE;

	SetStringQuotes(_T("\"\'"));

	SetTextColor(RGB(0,0,0), FALSE);
	SetKeywordColor(RGB(0,0,255), FALSE);
	SetConstantColor(RGB(0,0,0), TRUE);
	SetCommentColor(RGB(0,128,0), FALSE);
	SetNumberColor(RGB(255,0,255), FALSE);
	SetStringColor(RGB(255,0,0), FALSE);

	m_bInForcedChange = FALSE;
	m_changeType = ctUndo;
	m_crOldSel.cpMin = m_crOldSel.cpMax = 0;
}

CJsPadView::~CJsPadView()
{
}

BOOL CJsPadView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CIceEditView::PreCreateWindow(cs);
}

void CJsPadView::OnInitialUpdate()
{
	CIceEditView::OnInitialUpdate();


	// Set the printing margins (720 twips = 1/2 inch).
	SetMargins(CRect(720, 720, 720, 720));
}

/////////////////////////////////////////////////////////////////////////////
// CJsPadView printing

BOOL CJsPadView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}


void CJsPadView::OnDestroy()
{
	// Deactivate the item on destruction; this is important
	// when a splitter view is being used.
   CIceEditView::OnDestroy();
   COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
   if (pActiveItem != NULL && pActiveItem->GetActiveView() == this)
   {
      pActiveItem->Deactivate();
      ASSERT(GetDocument()->GetInPlaceActiveItem(this) == NULL);
   }
}


/////////////////////////////////////////////////////////////////////////////
// OLE Server support

// The following command handler provides the standard keyboard
//  user interface to cancel an in-place editing session.  Here,
//  the server (not the container) causes the deactivation.
void CJsPadView::OnCancelEditSrvr()
{
	GetDocument()->OnDeactivateUI(FALSE);
}

/////////////////////////////////////////////////////////////////////////////
// CJsPadView diagnostics

#ifdef _DEBUG
void CJsPadView::AssertValid() const
{
	CIceEditView::AssertValid();
}

void CJsPadView::Dump(CDumpContext& dc) const
{
	CIceEditView::Dump(dc);
}

CJsPadDoc* CJsPadView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CJsPadDoc)));
	return (CJsPadDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CJsPadView message handlers

void CJsPadView::Initialize() 
{
	PARAFORMAT pf;
	pf.cbSize = sizeof(PARAFORMAT);
	pf.dwMask = PFM_TABSTOPS ;
	pf.cTabCount = MAX_TAB_STOPS;
	for( int itab = 0 ; itab < pf.cTabCount  ; itab++ )
		pf.rgxTabs[itab] = (itab + 1) * 1440/5 ;

	SetParaFormat( pf );

	CHARFORMAT cfDefault;
	cfDefault.cbSize = sizeof(cfDefault);
	cfDefault.dwEffects = CFE_PROTECTED; 
	cfDefault.dwMask = CFM_BOLD | CFM_FACE | CFM_SIZE | CFM_CHARSET | CFM_PROTECTED;
	cfDefault.yHeight = 200;
	cfDefault.bCharSet = 0xEE; 
	strcpy(cfDefault.szFaceName, _T("Courier New")); 

	GetRichEditCtrl().SetDefaultCharFormat(cfDefault);
	GetRichEditCtrl().SetEventMask(ENM_CHANGE | ENM_SELCHANGE | ENM_PROTECTED);
}

static LPCTSTR szKeywords = " "
"break continue delete else false for function if "
"in new null return this true typeof var void while with "
"case catch class const debugger default do enum "
"export extends finally import super switch throw try ";
 
static LPCTSTR szConstants = " "
"Array Boolean Date Global Function Math Object String RegExp "; 
//"prototype toString ";


int CJsPadView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceEditView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
    Initialize();

    SetCaseSensitive(TRUE);
	SetStringQuotes(_T("\"\'"));
	SetSLComment(_T('/'),_T('/'));
	SetMLOpenComment(_T('/'),_T('*'));
	SetMLCloseComment(_T('*'),_T('/'));

    ClearKeywords();
	AddKeywords(szKeywords);
    ClearConstants();
	AddConstants(szConstants);
	
	return 0;
}

void CJsPadView::OnFinalRelease() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CIceEditView::OnFinalRelease();
}

void CJsPadView::OnBuildRun() 
{
    CString strFileName("");
    strFileName = GetDocument()->GetPathName();

    if(!strFileName.IsEmpty())
    {
        int retVal = 0;
        // If the file has been saved
        if(!GetDocument()->IsModified())
        {   
            retVal = (int) ShellExecute(this->GetSafeHwnd(),"open",strFileName,NULL,NULL,SW_SHOWNORMAL);
	        if(retVal > 32)
            {
                // call succeeded
            }
            else
            {
                DisplayError(retVal);
            }
        }
        else
        {
            // TODO:  ask to save it first and then run the program
            AfxMessageBox("Please Save Your Code First!", MB_OK);	
        }
    }
    else 
    {
        // Either the file is empty or it has never been saved since creation
        AfxMessageBox("Please Save Your Code First!", MB_OK);	
    }
}

void CJsPadView::OnUpdateBuildRun(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CJsPadView::DisplayError(int retVal)
{
    CString strMsg("");
    switch(retVal)
    {
    case 0:
        strMsg = "The operating system is out of memory or resources.";
        break;
    case ERROR_FILE_NOT_FOUND:
    //case SE_ERR_FNF:
        strMsg = "The specified file was not found.";
        break;
    case ERROR_PATH_NOT_FOUND :
    //case SE_ERR_PNF:
        strMsg = "The specified path was not found.";
        break;
    case ERROR_BAD_FORMAT :
        strMsg = "The .exe file is invalid (non-Win32 .exe or error in .exe image).";
        break;
    case SE_ERR_ACCESSDENIED:
        strMsg = "The operating system denied access to the specified file.";
        break;
    case SE_ERR_ASSOCINCOMPLETE:
        strMsg = "The file name association is incomplete or invalid.";
        break;
    case SE_ERR_DDEBUSY:
        strMsg = "The DDE transaction could not be completed because other DDE transactions were being processed.";
        break;
    case SE_ERR_DDEFAIL:
        strMsg = "The DDE transaction failed.";
        break;
    case SE_ERR_DDETIMEOUT:
        strMsg = "The DDE transaction could not be completed because the request timed out.";
        break;
    case SE_ERR_DLLNOTFOUND:
        strMsg = "The specified dynamic-link library was not found. ";
        break;
    case SE_ERR_NOASSOC:
        strMsg = "There is no application associated with the given file name extension.";
        break;
    case SE_ERR_OOM:
        strMsg = "There was not enough memory to complete the operation.";
        break;
    case SE_ERR_SHARE:
        strMsg = "A sharing violation occurred.";
        break;
    default:
        strMsg = "Unknown error occurred.";
        break;
    }
    AfxMessageBox(strMsg, MB_OK);
}

void CJsPadView::OnHelpTutorial() 
{
    /*
	if(NULL == HtmlHelp(this->GetSafeHwnd(),
		"help\\Wsh.chm::htm\\wsAboutWSH20.htm",
		HH_DISPLAY_TOPIC,SW_SHOWNORMAL))
	{
		CString  strFile = "help\\Wsh.chm";  // home.htm???
		ShellExecute(this->GetSafeHwnd(),"open",strFile,NULL,NULL,SW_SHOWNORMAL);
	}
    */
	if(NULL == HtmlHelp(NULL,
		"help\\Jscript5.chm::htm\\about.htm",
		HH_DISPLAY_TOPIC,0))
	{
		CString  strFile = "help\\Jcript5.chm";  // home.htm???
		ShellExecute(NULL,"open",strFile,NULL,NULL,0);
	}
}

void CJsPadView::OnUpdateHelpTutorial(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}
