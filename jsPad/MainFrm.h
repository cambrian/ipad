// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__FEF2B104_36FD_11D3_93E1_00207817FF60__INCLUDED_)
#define AFX_MAINFRM_H__FEF2B104_36FD_11D3_93E1_00207817FF60__INCLUDED_

#include "JsStatusBar.h"
#include "FindDlgBar.h"

#include "../icelib/IceFrameWnd.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMainFrame : public CIceFrameWnd
{
	
protected: // create from serialization only
	//CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

public:
	CMainFrame();


// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void ActivateFrame(int nCmdShow = -1);
	virtual void RecalcLayout(BOOL bNotify = TRUE);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CJsStatusBar  m_wndStatusBar;
	CToolBar      m_wndToolBar;
	CToolBar      m_wndToolsToolBar;
	CDialogBar    m_wndDlgBar;
	//CFindDlgBar   m_wndDlgBar;
	CReBar        m_wndReBar;

private:
    void  DisplayError(int retVal);


// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnInitMenu(CMenu* pMenu);
	afx_msg void OnToolsBeautify();
	afx_msg void OnUpdateToolsBeautify(CCmdUI* pCmdUI);
	afx_msg void OnToolsOptions();
	afx_msg void OnUpdateToolsOptions(CCmdUI* pCmdUI);
	afx_msg void OnToolsParse();
	afx_msg void OnUpdateToolsParse(CCmdUI* pCmdUI);
	afx_msg void OnToolsRun();
	afx_msg void OnUpdateToolsRun(CCmdUI* pCmdUI);
	afx_msg void OnHelp();
	afx_msg void OnUpdateHelp(CCmdUI* pCmdUI);
	afx_msg void OnHelpFinder();
	afx_msg void OnUpdateHelpFinder(CCmdUI* pCmdUI);
	afx_msg void OnContextHelp();
	afx_msg void OnUpdateContextHelp(CCmdUI* pCmdUI);
	afx_msg void OnHelpCodeSample();
	afx_msg void OnUpdateHelpCodeSample(CCmdUI* pCmdUI);
	afx_msg void OnHelpJsWeb();
	afx_msg void OnUpdateHelpJsWeb(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__FEF2B104_36FD_11D3_93E1_00207817FF60__INCLUDED_)
