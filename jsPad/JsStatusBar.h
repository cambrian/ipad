#if !defined(AFX_JSSTATUSBAR_H__BE152742_3768_11D3_93E5_00207817FF60__INCLUDED_)
#define AFX_JSSTATUSBAR_H__BE152742_3768_11D3_93E5_00207817FF60__INCLUDED_

#include "../icelib/IceStatusBar.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// JsStatusBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CJsStatusBar window

class CJsStatusBar : public CIceStatusBar
{
// Construction
public:
	CJsStatusBar();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJsStatusBar)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CJsStatusBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CJsStatusBar)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_JSSTATUSBAR_H__BE152742_3768_11D3_93E5_00207817FF60__INCLUDED_)
