// iPadDoc.cpp : implementation of the CIPadDoc class
//

#include "stdafx.h"
#include "iPad.h"

#include "iPadDoc.h"
#include "CntrItem.h"
#include "SrvrItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIPadDoc

IMPLEMENT_DYNCREATE(CIPadDoc, CRichEditDoc)

BEGIN_MESSAGE_MAP(CIPadDoc, CRichEditDoc)
	//{{AFX_MSG_MAP(CIPadDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Enable default OLE container implementation
	ON_UPDATE_COMMAND_UI(ID_OLE_EDIT_LINKS, CRichEditDoc::OnUpdateEditLinksMenu)
	ON_COMMAND(ID_OLE_EDIT_LINKS, CRichEditDoc::OnEditLinks)
	ON_UPDATE_COMMAND_UI(ID_OLE_VERB_FIRST, CRichEditDoc::OnUpdateObjectVerbMenu)
	ON_COMMAND(ID_FILE_SEND_MAIL, OnFileSendMail)
	ON_UPDATE_COMMAND_UI(ID_FILE_SEND_MAIL, OnUpdateFileSendMail)
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CIPadDoc, CRichEditDoc)
	//{{AFX_DISPATCH_MAP(CIPadDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//      DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IIPad to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {E01C5AC6-AF1E-11D2-8EFE-00A024E0E339}
static const IID IID_IIPad =
{ 0xe01c5ac6, 0xaf1e, 0x11d2, { 0x8e, 0xfe, 0x0, 0xa0, 0x24, 0xe0, 0xe3, 0x39 } };

BEGIN_INTERFACE_MAP(CIPadDoc, CRichEditDoc)
	INTERFACE_PART(CIPadDoc, IID_IIPad, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIPadDoc construction/destruction

CIPadDoc::CIPadDoc()
{
	// Use OLE compound files
	EnableCompoundFile();

	// TODO: add one-time construction code here

	EnableAutomation();

	AfxOleLockApp();
}

CIPadDoc::~CIPadDoc()
{
	AfxOleUnlockApp();
}

BOOL CIPadDoc::OnNewDocument()
{
	if (!CRichEditDoc::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

CRichEditCntrItem* CIPadDoc::CreateClientItem(REOBJECT* preo) const
{
	// cast away constness of this
	return new CIPadCntrItem(preo, (CIPadDoc*) this);
}

/////////////////////////////////////////////////////////////////////////////
// CIPadDoc server implementation

COleServerItem* CIPadDoc::OnGetEmbeddedItem()
{
	// OnGetEmbeddedItem is called by the framework to get the COleServerItem
	//  that is associated with the document.  It is only called when necessary.

	CIPadSrvrItem* pItem = new CIPadSrvrItem(this);
	ASSERT_VALID(pItem);
	return pItem;
}



/////////////////////////////////////////////////////////////////////////////
// CIPadDoc serialization

void CIPadDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}

	// Calling the base class CRichEditDoc enables serialization
	//  of the container document's COleClientItem objects.
	CRichEditDoc::Serialize(ar);
}

/////////////////////////////////////////////////////////////////////////////
// CIPadDoc diagnostics

#ifdef _DEBUG
void CIPadDoc::AssertValid() const
{
	CRichEditDoc::AssertValid();
}

void CIPadDoc::Dump(CDumpContext& dc) const
{
	CRichEditDoc::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIPadDoc commands
