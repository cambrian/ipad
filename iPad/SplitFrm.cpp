// SplitFrm.cpp : implementation file
//

#include "stdafx.h"
#include "iPad.h"
#include "SplitFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSplitFrame

IMPLEMENT_DYNCREATE(CSplitFrame, CIceMDIChildWnd)

CSplitFrame::CSplitFrame()
{
	//iceMenu.LoadToolBarResource(IDR_MAINFRAME);
	iceMenu.AddToolBarResource(IDR_SYSTEMBAR);
	iceMenu.AddToolBarResource(IDR_RESERVOIR);
	iceMenu.AddToolBarResource(IDR_MRUFILE);
}

CSplitFrame::~CSplitFrame()
{
}


BEGIN_MESSAGE_MAP(CSplitFrame, CIceMDIChildWnd)
	//{{AFX_MSG_MAP(CSplitFrame)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSplitFrame message handlers

BOOL CSplitFrame::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CIceMDIChildWnd::PreCreateWindow(cs);
}

void CSplitFrame::ActivateFrame(int nCmdShow) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	//nCmdShow = SW_SHOWMAXIMIZED;
	CIceMDIChildWnd::ActivateFrame(nCmdShow);
}

BOOL CSplitFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	// CG: The following block was added by the Split Bars component.

	{

		if (!m_wndSplitter.Create(this,
		                          2, 2,          // TODO: adjust the number of rows, columns
		                          CSize(10, 10), // TODO: adjust the minimum pane size
		                          pContext))
		{
			TRACE0("Failed to create split bar ");
			return FALSE;    // failed to create
		}

	}
	// TODO: Add your specialized code here and/or call the base class
	
	return TRUE;
	// Why can't I call this???????????????
	//return CIceMDIChildWnd::OnCreateClient(lpcs, pContext);
}
