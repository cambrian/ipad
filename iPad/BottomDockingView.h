#if !defined(AFX_BOTTOMDOCKINGVIEW_H__E01C5B13_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_BOTTOMDOCKINGVIEW_H__E01C5B13_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_

#include "../icelib/IceDockingTabbedView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// BottomDockingView.h : header file
//

#define  IDC_BOTTOMDOCKINGVIEW 6538


/////////////////////////////////////////////////////////////////////////////
// CBottomDockingView

class CBottomDockingView : public CIceDockingTabbedView
{
public:
	CBottomDockingView();          
	virtual ~CBottomDockingView();

// Attributes
public:

// Operations
public:


// Overridables
    virtual void OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBottomDockingView)
	protected:
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
protected:
	//{{AFX_MSG(CBottomDockingView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTabSelChange(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTabSelChanging(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BOTTOMDOCKINGVIEW_H__E01C5B13_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
