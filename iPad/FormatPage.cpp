// FormatPage.cpp : implementation file
//

#include "stdafx.h"
#include "ipad.h"
#include "FormatPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFormatPage property page

IMPLEMENT_DYNCREATE(CFormatPage, CPropertyPage)

CFormatPage::CFormatPage() : CPropertyPage(CFormatPage::IDD)
{
	//{{AFX_DATA_INIT(CFormatPage)
	m_strFontFace = _T("");
	m_nFontSize = 0;
	//}}AFX_DATA_INIT

}

CFormatPage::~CFormatPage()
{
}

void CFormatPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFormatPage)
	DDX_Control(pDX, IDC_FORMATPAGE_FILETYPE, m_cbFileType);
	DDX_Control(pDX, IDC_FORMATPAGE_SYNTAXCOLORS, m_lbSyntaxColors);
	DDX_Text(pDX, IDC_FORMATPAGE_FONTFACE, m_strFontFace);
	DDX_Text(pDX, IDC_FORMATPAGE_FONTSIZE, m_nFontSize);
	DDV_MinMaxInt(pDX, m_nFontSize, 0, 100);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFormatPage, CPropertyPage)
	//{{AFX_MSG_MAP(CFormatPage)
	ON_BN_CLICKED(IDC_FORMATPAGE_CHANGECOLOR, OnFormatpageChangecolor)
	ON_BN_CLICKED(IDC_FORMATPAGE_CHANGEFONT, OnFormatpageChangefont)
	ON_BN_CLICKED(IDC_FORMATPAGE_CLEAR, OnFormatpageClear)
	ON_BN_CLICKED(IDC_FORMATPAGE_EDITFILE, OnFormatpageEditfile)
	ON_CBN_SELCHANGE(IDC_FORMATPAGE_FILETYPE, OnSelchangeFormatpageFiletype)
	ON_LBN_SELCHANGE(IDC_FORMATPAGE_SYNTAXCOLORS, OnSelchangeFormatpageSyntaxcolors)
	ON_LBN_DBLCLK(IDC_FORMATPAGE_SYNTAXCOLORS, OnDblclkFormatpageSyntaxcolors)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFormatPage message handlers

BOOL CFormatPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
    m_cbFileType.AddString(_T("Default"));
    m_cbFileType.AddString(_T("Java"));
    m_cbFileType.AddString(_T("C++"));
    m_cbFileType.SelectString(1,_T("Default"));

    m_lbSyntaxColors.AddString(_T("Text"));
    m_lbSyntaxColors.AddString(_T("Keyword"));
    m_lbSyntaxColors.AddString(_T("Comment"));
    m_lbSyntaxColors.AddString(_T("String"));
    m_lbSyntaxColors.AddString(_T("Number"));
    
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFormatPage::OnFormatpageChangecolor() 
{
	// TODO: Add your control notification handler code here
	
}

void CFormatPage::OnFormatpageChangefont() 
{
	// TODO: Add your control notification handler code here
	
}

void CFormatPage::OnFormatpageClear() 
{
	// TODO: Add your control notification handler code here
	
}

void CFormatPage::OnFormatpageEditfile() 
{
	// TODO: Add your control notification handler code here
	
}

void CFormatPage::OnSelchangeFormatpageFiletype() 
{
	// TODO: Add your control notification handler code here
	
}

void CFormatPage::OnSelchangeFormatpageSyntaxcolors() 
{
	// TODO: Add your control notification handler code here
	
}

void CFormatPage::OnDblclkFormatpageSyntaxcolors() 
{
	// TODO: Add your control notification handler code here
	
}

