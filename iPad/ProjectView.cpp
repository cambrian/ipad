// ProjectView.cpp : implementation file
//

#include "stdafx.h"
#include "ipad.h"
#include "ProjectDoc.h"
#include "ProjectView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProjectView

IMPLEMENT_DYNCREATE(CProjectView, CEditView)

CProjectView::CProjectView()
{
}

CProjectView::~CProjectView()
{
}


BEGIN_MESSAGE_MAP(CProjectView, CEditView)
	//{{AFX_MSG_MAP(CProjectView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProjectView drawing

void CProjectView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CProjectView diagnostics

#ifdef _DEBUG
void CProjectView::AssertValid() const
{
	CEditView::AssertValid();
}

void CProjectView::Dump(CDumpContext& dc) const
{
	CEditView::Dump(dc);
}
#endif //_DEBUG


/////////////////////////////////////////////////////////////////////////////
// CProjectView message handlers

BOOL CProjectView::PreCreateWindow(CREATESTRUCT& cs) 
{
    // Invisible window....
	return CEditView::PreCreateWindow(cs);
}

void CProjectView::Serialize(CArchive& ar) 
{
	if (ar.IsStoring())
	{	// storing code
    }
	else
	{	// loading code

    	CProjectDoc* pDoc = (CProjectDoc*) GetDocument();
        SetWindowText(pDoc->strBuffer);   // temporary
    }
}
