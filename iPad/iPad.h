// iPad.h : main header file for the IPAD application
//

#if !defined(AFX_IPAD_H__E01C5AC9_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_IPAD_H__E01C5AC9_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////
////// strings for registry entry
static const TCHAR s_profileCompany[] = _T("BlueCraft");

/////////////////////////////////////////////////////////////////////////////
// CIPadApp:
// See iPad.cpp for the implementation of this class
//

class CIPadApp : public CWinApp
{
public:
	CIPadApp();

	BOOL NewProgramFile(const CString& strTarget);


public:
	CMultiDocTemplate* m_pProjectDocTemplate;
	CMultiDocTemplate* m_pJavaDocTemplate;
	CMultiDocTemplate* m_pCppDocTemplate;
	CMultiDocTemplate* m_pJsDocTemplate;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIPadApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
	COleTemplateServer m_server;
		// Server object for document creation

	//{{AFX_MSG(CIPadApp)
	afx_msg void OnAppAbout();
	afx_msg void OnFileNewCpp();
	afx_msg void OnUpdateFileNewCpp(CCmdUI* pCmdUI);
	afx_msg void OnFileNewJava();
	afx_msg void OnUpdateFileNewJava(CCmdUI* pCmdUI);
	afx_msg void OnFileOpenCpp();
	afx_msg void OnUpdateFileOpenCpp(CCmdUI* pCmdUI);
	afx_msg void OnFileOpenJava();
	afx_msg void OnUpdateFileOpenJava(CCmdUI* pCmdUI);
	afx_msg void OnToolsOptions();
	afx_msg void OnUpdateToolsOptions(CCmdUI* pCmdUI);
	afx_msg void OnProjectNew();
	afx_msg void OnUpdateProjectNew(CCmdUI* pCmdUI);
	afx_msg void OnProjectOpen();
	afx_msg void OnUpdateProjectOpen(CCmdUI* pCmdUI);
	afx_msg void OnProjectClose();
	afx_msg void OnUpdateProjectClose(CCmdUI* pCmdUI);
	afx_msg void OnProjectSave();
	afx_msg void OnUpdateProjectSave(CCmdUI* pCmdUI);
	afx_msg void OnFileNewJscript();
	afx_msg void OnUpdateFileNewJscript(CCmdUI* pCmdUI);
	afx_msg void OnFileOpenJscript();
	afx_msg void OnUpdateFileOpenJscript(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void ShowTipAtStartup(void);
private:
	void ShowTipOfTheDay(void);
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IPAD_H__E01C5AC9_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
