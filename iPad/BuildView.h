#if !defined(AFX_BUILDVIEW_H__E01C5B02_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_BUILDVIEW_H__E01C5B02_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// BuildView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBuildView view

class CBuildView : public CRichEditView
{
protected: // create from serialization only
	CBuildView();
	DECLARE_DYNCREATE(CBuildView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBuildView)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBuildView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	// Generated message map functions
	//{{AFX_MSG(CBuildView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CBuildView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BUILDVIEW_H__E01C5B02_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
