#if !defined(AFX_LEFTDOCKINGVIEW_H__56D8A944_521B_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_LEFTDOCKINGVIEW_H__56D8A944_521B_11D2_852E_00A024E0E339__INCLUDED_

#include "../icelib/IceDockingTabbedView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// LeftDockingView.h : header file
//

#define  IDC_LEFTDOCKINGVIEW 6539


/////////////////////////////////////////////////////////////////////////////
// CLeftDockingView

class CLeftDockingView : public CIceDockingTabbedView
{
public:
	CLeftDockingView();          
	virtual ~CLeftDockingView();

// Attributes
public:

// Operations
public:


// Overridables
    virtual void OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLeftDockingView)
	protected:
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
protected:
	//{{AFX_MSG(CLeftDockingView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTabSelChange(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTabSelChanging(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LEFTDOCKINGVIEW_H__56D8A944_521B_11D2_852E_00A024E0E339__INCLUDED_)
