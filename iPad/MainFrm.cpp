// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "iPad.h"

#include "MainFrm.h"
#include "BuildView.h"
#include "DebugView.h"
#include "ScriptView.h"
#include "FindView.h"
#include "LogView.h"
#include "ClassView.h"
#include "FileView.h"
#include "InfoView.h"

#include "TipDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CIceMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CIceMDIFrameWnd)
	ON_COMMAND_EX(ID_VIEW_WORKSPACE_PANE, OnBarCheck)
	ON_UPDATE_COMMAND_UI(ID_VIEW_WORKSPACE_PANE, OnUpdateControlBarMenu)
	ON_COMMAND_EX(ID_VIEW_OUTPUT_PANE, OnBarCheck)
	ON_UPDATE_COMMAND_UI(ID_VIEW_OUTPUT_PANE, OnUpdateControlBarMenu)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_INITMENU()
	ON_COMMAND(CG_IDS_TIPOFTHEDAYMENU, OnTipofthedaymenu)
	ON_UPDATE_COMMAND_UI(CG_IDS_TIPOFTHEDAYMENU, OnUpdateTipofthedaymenu)
	//}}AFX_MSG_MAP
	// Global help commands
	ON_COMMAND(ID_HELP_FINDER, CIceMDIFrameWnd::OnHelpFinder)
	ON_COMMAND(ID_HELP, CIceMDIFrameWnd::OnHelp)
	ON_COMMAND(ID_CONTEXT_HELP, CIceMDIFrameWnd::OnContextHelp)
	ON_COMMAND(ID_DEFAULT_HELP, CIceMDIFrameWnd::OnHelpFinder)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here

	//iceMenu.AddToolBarResource(IDR_MRUFILE);
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CIceMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndMainToolBar.Create(this,
        WS_CHILD | WS_VISIBLE | CBRS_TOP, ID_VIEW_TOOLBAR) ||
		!m_wndMainToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
	//m_wndMainToolBar.ModifyStyle(0, TBSTYLE_FLAT); 

	/*
	if (!m_wndDefaultStatusBar.Create(this) ||
		!m_wndDefaultStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	*/
	if (!m_wndDefaultStatusBar.CreateStatusBar(this, indicators, sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}


	// TODO: Remove this if you don't want tool tips or a resizeable toolbar
	m_wndMainToolBar.SetBarStyle(m_wndMainToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndMainToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndMainToolBar);
    m_wndMainToolBar.SetWindowText(_T("Standard Tool Bar"));


	/////  Docking views
	// [1] Workspace tab windows
	if (!m_wndLeftDockBar.Create(_T("Workspace Window"), this, CSize(200, 400),
		TRUE, //bHasGripper 
		ID_VIEW_WORKSPACE_PANE))
	{
		TRACE0("Failed to create workspace pane\n");
		return -1;      // fail to create
	}
	m_wndLeftDockBar.SetBarStyle(m_wndLeftDockBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndLeftDockBar.EnableDocking(CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndLeftDockBar, AFX_IDW_DOCKBAR_LEFT);
	// Add view windows in the docking bar
	m_wndLeftDockBar.AddView(_T("Class"), RUNTIME_CLASS(CClassView));
	m_wndLeftDockBar.AddView(_T("File"), RUNTIME_CLASS(CFileView)); 
	m_wndLeftDockBar.AddView(_T("Info"), RUNTIME_CLASS(CInfoView));

	// [2] Output tab windows
	if (!m_wndBottomDockBar.Create(_T("Output Window"), this, CSize(550, 100),
		FALSE, //bHasGripper 
		ID_VIEW_OUTPUT_PANE))
	{
		TRACE0("Failed to create output pane\n");
		return -1;      // fail to create
	}
	m_wndBottomDockBar.SetBarStyle(m_wndBottomDockBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndBottomDockBar.EnableDocking(CBRS_ALIGN_TOP | CBRS_ALIGN_BOTTOM);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndBottomDockBar, AFX_IDW_DOCKBAR_BOTTOM);
	// Add view windows in the docking bar
	m_wndBottomDockBar.AddView(_T("Build"), RUNTIME_CLASS(CBuildView));
	m_wndBottomDockBar.AddView(_T("Debug"), RUNTIME_CLASS(CDebugView));
	m_wndBottomDockBar.AddView(_T("Script"), RUNTIME_CLASS(CScriptView));
	m_wndBottomDockBar.AddView(_T("Find"), RUNTIME_CLASS(CFindView));
	m_wndBottomDockBar.AddView(_T("Log"), RUNTIME_CLASS(CLogView));

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CIceMDIFrameWnd::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CIceMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CIceMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::ActivateFrame(int nCmdShow) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CIceMDIFrameWnd::ActivateFrame(nCmdShow);
}

void CMainFrame::RecalcLayout(BOOL bNotify) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CIceMDIFrameWnd::RecalcLayout(bNotify);
}

void CMainFrame::OnInitMenu(CMenu* pMenu) 
{
	CIceMDIFrameWnd::OnInitMenu(pMenu);
}


void CMainFrame::OnTipofthedaymenu() 
{
    CTipDlg tipDlg;
    
    if(tipDlg.DoModal() == IDOK)
        return;
}

void CMainFrame::OnUpdateTipofthedaymenu(CCmdUI* pCmdUI) 
{
    pCmdUI->Enable(TRUE);
}
