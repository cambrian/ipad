// SrvrItem.h : interface of the CIPadSrvrItem class
//

#if !defined(AFX_SRVRITEM_H__E01C5AD7_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_SRVRITEM_H__E01C5AD7_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CIPadSrvrItem : public COleServerItem
{
	DECLARE_DYNAMIC(CIPadSrvrItem)

// Constructors
public:
	CIPadSrvrItem(CIPadDoc* pContainerDoc);

// Attributes
	CIPadDoc* GetDocument() const
		{ return (CIPadDoc*)COleServerItem::GetDocument(); }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIPadSrvrItem)
	public:
	virtual BOOL OnDraw(CDC* pDC, CSize& rSize);
	virtual BOOL OnGetExtent(DVASPECT dwDrawAspect, CSize& rSize);
	//}}AFX_VIRTUAL

// Implementation
public:
	~CIPadSrvrItem();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SRVRITEM_H__E01C5AD7_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
