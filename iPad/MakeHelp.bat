@echo off
REM -- First make map file from Microsoft Visual C++ generated resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by IPAD.HPJ. >"hlp\iPad.hm"
echo. >>"hlp\iPad.hm"
echo // Commands (ID_* and IDM_*) >>"hlp\iPad.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\iPad.hm"
echo. >>"hlp\iPad.hm"
echo // Prompts (IDP_*) >>"hlp\iPad.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\iPad.hm"
echo. >>"hlp\iPad.hm"
echo // Resources (IDR_*) >>"hlp\iPad.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\iPad.hm"
echo. >>"hlp\iPad.hm"
echo // Dialogs (IDD_*) >>"hlp\iPad.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\iPad.hm"
echo. >>"hlp\iPad.hm"
echo // Frame Controls (IDW_*) >>"hlp\iPad.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\iPad.hm"
REM -- Make help for Project IPAD


echo Building Win32 Help files
start /wait hcw /C /E /M "hlp\iPad.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\iPad.hlp" goto :Error
if not exist "hlp\iPad.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\iPad.hlp" Debug
if exist Debug\nul copy "hlp\iPad.cnt" Debug
if exist Release\nul copy "hlp\iPad.hlp" Release
if exist Release\nul copy "hlp\iPad.cnt" Release
echo.
goto :done

:Error
echo hlp\iPad.hpj(1) : error: Problem encountered creating help file

:done
echo.
