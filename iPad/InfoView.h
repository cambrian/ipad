#if !defined(AFX_INFOVIEW_H__E01C5B12_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_INFOVIEW_H__E01C5B12_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_

#include <afxcview.h>

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// InfoView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInfoView view

class CInfoView : public CTreeView
{
protected:
	CInfoView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CInfoView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInfoView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CInfoView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CInfoView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INFOVIEW_H__E01C5B12_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
