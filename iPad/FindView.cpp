// FindView.cpp : implementation file
//

#include "stdafx.h"
#include "iPad.h"
#include "FindView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFindView

IMPLEMENT_DYNCREATE(CFindView, CRichEditView)

CFindView::CFindView()
{
	EnableAutomation();
}

CFindView::~CFindView()
{
}

void CFindView::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CRichEditView::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CFindView, CRichEditView)
	//{{AFX_MSG_MAP(CFindView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CFindView, CRichEditView)
	//{{AFX_DISPATCH_MAP(CFindView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFindView to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {E01C5B0D-AF1E-11D2-8EFE-00A024E0E339}
static const IID IID_IFindView =
{ 0xe01c5b0d, 0xaf1e, 0x11d2, { 0x8e, 0xfe, 0x0, 0xa0, 0x24, 0xe0, 0xe3, 0x39 } };

BEGIN_INTERFACE_MAP(CFindView, CRichEditView)
	INTERFACE_PART(CFindView, IID_IFindView, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFindView diagnostics

#ifdef _DEBUG
void CFindView::AssertValid() const
{
	CRichEditView::AssertValid();
}

void CFindView::Dump(CDumpContext& dc) const
{
	CRichEditView::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFindView message handlers
