// MainToolBar.cpp : implementation file
//

#include "stdafx.h"
#include "iPad.h"
#include "MainToolBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainToolBar

CMainToolBar::CMainToolBar()
{
	//m_nBarGripper = 1;  // number of bars in the gripper (0, 1, or 2; default: 1)
}

CMainToolBar::~CMainToolBar()
{
}


//BEGIN_MESSAGE_MAP(CMainToolBar, CToolBar)
BEGIN_MESSAGE_MAP(CMainToolBar, CIceToolBar)
	//{{AFX_MSG_MAP(CMainToolBar)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainToolBar message handlers
