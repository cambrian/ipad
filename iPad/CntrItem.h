// CntrItem.h : interface of the CIPadCntrItem class
//

#if !defined(AFX_CNTRITEM_H__E01C5ADB_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_CNTRITEM_H__E01C5ADB_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CIPadDoc;
class CIPadView;

class CIPadCntrItem : public CRichEditCntrItem
{
	DECLARE_SERIAL(CIPadCntrItem)

// Constructors
public:
	CIPadCntrItem(REOBJECT* preo = NULL, CIPadDoc* pContainer = NULL);
		// Note: pContainer is allowed to be NULL to enable IMPLEMENT_SERIALIZE.
		//  IMPLEMENT_SERIALIZE requires the class have a constructor with
		//  zero arguments.  Normally, OLE items are constructed with a
		//  non-NULL document pointer.

// Attributes
public:
	CIPadDoc* GetDocument()
		{ return (CIPadDoc*)CRichEditCntrItem::GetDocument(); }
	CIPadView* GetActiveView()
		{ return (CIPadView*)CRichEditCntrItem::GetActiveView(); }

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIPadCntrItem)
	public:
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	~CIPadCntrItem();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CNTRITEM_H__E01C5ADB_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
