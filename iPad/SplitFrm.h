#if !defined(AFX_SPLITFRM_H__C7AA061F_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_SPLITFRM_H__C7AA061F_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_

#include "../icelib/IceMDIChildWnd.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// SplitFrm.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSplitFrame frame

class CSplitFrame : public CIceMDIChildWnd
{
	DECLARE_DYNCREATE(CSplitFrame)
protected:
	CSplitFrame();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSplitFrame)
	public:
	virtual void ActivateFrame(int nCmdShow = -1);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	//}}AFX_VIRTUAL

// Implementation
protected:
	CSplitterWnd m_wndSplitter;
	virtual ~CSplitFrame();

	// Generated message map functions
	//{{AFX_MSG(CSplitFrame)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SPLITFRM_H__C7AA061F_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_)
