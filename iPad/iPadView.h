// iPadView.h : interface of the CIPadView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_IPADVIEW_H__E01C5AD4_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_IPADVIEW_H__E01C5AD4_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CIPadCntrItem;

class CIPadView : public CRichEditView
{
protected: // create from serialization only
	CIPadView();
	DECLARE_DYNCREATE(CIPadView)

// Attributes
public:
	CIPadDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIPadView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIPadView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CIPadView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	afx_msg void OnDestroy();
	afx_msg void OnCancelEditSrvr();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in iPadView.cpp
inline CIPadDoc* CIPadView::GetDocument()
   { return (CIPadDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IPADVIEW_H__E01C5AD4_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
