# Microsoft Developer Studio Generated NMAKE File, Based on iPad.dsp
!IF "$(CFG)" == ""
CFG=iPad - Win32 Debug
!MESSAGE No configuration specified. Defaulting to iPad - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "iPad - Win32 Release" && "$(CFG)" != "iPad - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "iPad.mak" CFG="iPad - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "iPad - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "iPad - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "iPad - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
TargetName=iPad
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OutDir)\$(TargetName).hlp" "$(OUTDIR)\iPad.exe" "$(OUTDIR)\iPad.tlb"

!ELSE 

ALL : "$(OutDir)\$(TargetName).hlp" "$(OUTDIR)\iPad.exe" "$(OUTDIR)\iPad.tlb"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\BottomDockingView.obj"
	-@erase "$(INTDIR)\BuildView.obj"
	-@erase "$(INTDIR)\ChildFrm.obj"
	-@erase "$(INTDIR)\ClassView.obj"
	-@erase "$(INTDIR)\CntrItem.obj"
	-@erase "$(INTDIR)\CppCntrItem.obj"
	-@erase "$(INTDIR)\CppDoc.obj"
	-@erase "$(INTDIR)\CppSrvrItem.obj"
	-@erase "$(INTDIR)\CppView.obj"
	-@erase "$(INTDIR)\DebugView.obj"
	-@erase "$(INTDIR)\DefaultStatusBar.obj"
	-@erase "$(INTDIR)\FileView.obj"
	-@erase "$(INTDIR)\FindView.obj"
	-@erase "$(INTDIR)\FormatPage.obj"
	-@erase "$(INTDIR)\IceButton.obj"
	-@erase "$(INTDIR)\IceDockingControlBar.obj"
	-@erase "$(INTDIR)\IceDockingTabbedView.obj"
	-@erase "$(INTDIR)\IceEditCntrItem.obj"
	-@erase "$(INTDIR)\IceEditDoc.obj"
	-@erase "$(INTDIR)\IceEditSrvrItem.obj"
	-@erase "$(INTDIR)\IceEditView.obj"
	-@erase "$(INTDIR)\IceMDIChildWnd.obj"
	-@erase "$(INTDIR)\IceMDIFrameWnd.obj"
	-@erase "$(INTDIR)\IceMenu.obj"
	-@erase "$(INTDIR)\IceStatic.obj"
	-@erase "$(INTDIR)\IceStatusBar.obj"
	-@erase "$(INTDIR)\IceToolBar.obj"
	-@erase "$(INTDIR)\InfoView.obj"
	-@erase "$(INTDIR)\InvisibleFrm.obj"
	-@erase "$(INTDIR)\iPad.obj"
	-@erase "$(INTDIR)\iPad.pch"
	-@erase "$(INTDIR)\iPad.res"
	-@erase "$(INTDIR)\iPad.tlb"
	-@erase "$(INTDIR)\iPadDoc.obj"
	-@erase "$(INTDIR)\iPadView.obj"
	-@erase "$(INTDIR)\IpFrame.obj"
	-@erase "$(INTDIR)\ISBPaneInfo.obj"
	-@erase "$(INTDIR)\ISBPaneText.obj"
	-@erase "$(INTDIR)\JavaCntrItem.obj"
	-@erase "$(INTDIR)\JavaDoc.obj"
	-@erase "$(INTDIR)\JavaSrvrItem.obj"
	-@erase "$(INTDIR)\JavaView.obj"
	-@erase "$(INTDIR)\LeftDockingView.obj"
	-@erase "$(INTDIR)\LogView.obj"
	-@erase "$(INTDIR)\MainFrm.obj"
	-@erase "$(INTDIR)\MainToolBar.obj"
	-@erase "$(INTDIR)\ProjectDoc.obj"
	-@erase "$(INTDIR)\ProjectView.obj"
	-@erase "$(INTDIR)\ScriptView.obj"
	-@erase "$(INTDIR)\SplitFrm.obj"
	-@erase "$(INTDIR)\SrvrItem.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\TabsPage.obj"
	-@erase "$(INTDIR)\TipDlg.obj"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(OUTDIR)\iPad.exe"
	-@erase "$(OutDir)\$(TargetName).hlp"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D\
 "_AFXDLL" /Fp"$(INTDIR)\iPad.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\"\
 /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Release/
CPP_SBRS=.
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o NUL /win32 
RSC_PROJ=/l 0x412 /fo"$(INTDIR)\iPad.res" /d "NDEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\iPad.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=.\expat\xmltok.lib .\expat\xmlparse.lib /nologo /subsystem:windows\
 /incremental:no /pdb:"$(OUTDIR)\iPad.pdb" /machine:I386\
 /out:"$(OUTDIR)\iPad.exe" 
LINK32_OBJS= \
	"$(INTDIR)\BottomDockingView.obj" \
	"$(INTDIR)\BuildView.obj" \
	"$(INTDIR)\ChildFrm.obj" \
	"$(INTDIR)\ClassView.obj" \
	"$(INTDIR)\CntrItem.obj" \
	"$(INTDIR)\CppCntrItem.obj" \
	"$(INTDIR)\CppDoc.obj" \
	"$(INTDIR)\CppSrvrItem.obj" \
	"$(INTDIR)\CppView.obj" \
	"$(INTDIR)\DebugView.obj" \
	"$(INTDIR)\DefaultStatusBar.obj" \
	"$(INTDIR)\FileView.obj" \
	"$(INTDIR)\FindView.obj" \
	"$(INTDIR)\FormatPage.obj" \
	"$(INTDIR)\IceButton.obj" \
	"$(INTDIR)\IceDockingControlBar.obj" \
	"$(INTDIR)\IceDockingTabbedView.obj" \
	"$(INTDIR)\IceEditCntrItem.obj" \
	"$(INTDIR)\IceEditDoc.obj" \
	"$(INTDIR)\IceEditSrvrItem.obj" \
	"$(INTDIR)\IceEditView.obj" \
	"$(INTDIR)\IceMDIChildWnd.obj" \
	"$(INTDIR)\IceMDIFrameWnd.obj" \
	"$(INTDIR)\IceMenu.obj" \
	"$(INTDIR)\IceStatic.obj" \
	"$(INTDIR)\IceStatusBar.obj" \
	"$(INTDIR)\IceToolBar.obj" \
	"$(INTDIR)\InfoView.obj" \
	"$(INTDIR)\InvisibleFrm.obj" \
	"$(INTDIR)\iPad.obj" \
	"$(INTDIR)\iPad.res" \
	"$(INTDIR)\iPadDoc.obj" \
	"$(INTDIR)\iPadView.obj" \
	"$(INTDIR)\IpFrame.obj" \
	"$(INTDIR)\ISBPaneInfo.obj" \
	"$(INTDIR)\ISBPaneText.obj" \
	"$(INTDIR)\JavaCntrItem.obj" \
	"$(INTDIR)\JavaDoc.obj" \
	"$(INTDIR)\JavaSrvrItem.obj" \
	"$(INTDIR)\JavaView.obj" \
	"$(INTDIR)\LeftDockingView.obj" \
	"$(INTDIR)\LogView.obj" \
	"$(INTDIR)\MainFrm.obj" \
	"$(INTDIR)\MainToolBar.obj" \
	"$(INTDIR)\ProjectDoc.obj" \
	"$(INTDIR)\ProjectView.obj" \
	"$(INTDIR)\ScriptView.obj" \
	"$(INTDIR)\SplitFrm.obj" \
	"$(INTDIR)\SrvrItem.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\TabsPage.obj" \
	"$(INTDIR)\TipDlg.obj"

"$(OUTDIR)\iPad.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
TargetName=iPad
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OutDir)\$(TargetName).hlp" "$(OUTDIR)\iPad.exe" "$(OUTDIR)\iPad.tlb"\
 "$(OUTDIR)\iPad.bsc"

!ELSE 

ALL : "$(OutDir)\$(TargetName).hlp" "$(OUTDIR)\iPad.exe" "$(OUTDIR)\iPad.tlb"\
 "$(OUTDIR)\iPad.bsc"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\BottomDockingView.obj"
	-@erase "$(INTDIR)\BottomDockingView.sbr"
	-@erase "$(INTDIR)\BuildView.obj"
	-@erase "$(INTDIR)\BuildView.sbr"
	-@erase "$(INTDIR)\ChildFrm.obj"
	-@erase "$(INTDIR)\ChildFrm.sbr"
	-@erase "$(INTDIR)\ClassView.obj"
	-@erase "$(INTDIR)\ClassView.sbr"
	-@erase "$(INTDIR)\CntrItem.obj"
	-@erase "$(INTDIR)\CntrItem.sbr"
	-@erase "$(INTDIR)\CppCntrItem.obj"
	-@erase "$(INTDIR)\CppCntrItem.sbr"
	-@erase "$(INTDIR)\CppDoc.obj"
	-@erase "$(INTDIR)\CppDoc.sbr"
	-@erase "$(INTDIR)\CppSrvrItem.obj"
	-@erase "$(INTDIR)\CppSrvrItem.sbr"
	-@erase "$(INTDIR)\CppView.obj"
	-@erase "$(INTDIR)\CppView.sbr"
	-@erase "$(INTDIR)\DebugView.obj"
	-@erase "$(INTDIR)\DebugView.sbr"
	-@erase "$(INTDIR)\DefaultStatusBar.obj"
	-@erase "$(INTDIR)\DefaultStatusBar.sbr"
	-@erase "$(INTDIR)\FileView.obj"
	-@erase "$(INTDIR)\FileView.sbr"
	-@erase "$(INTDIR)\FindView.obj"
	-@erase "$(INTDIR)\FindView.sbr"
	-@erase "$(INTDIR)\FormatPage.obj"
	-@erase "$(INTDIR)\FormatPage.sbr"
	-@erase "$(INTDIR)\IceButton.obj"
	-@erase "$(INTDIR)\IceButton.sbr"
	-@erase "$(INTDIR)\IceDockingControlBar.obj"
	-@erase "$(INTDIR)\IceDockingControlBar.sbr"
	-@erase "$(INTDIR)\IceDockingTabbedView.obj"
	-@erase "$(INTDIR)\IceDockingTabbedView.sbr"
	-@erase "$(INTDIR)\IceEditCntrItem.obj"
	-@erase "$(INTDIR)\IceEditCntrItem.sbr"
	-@erase "$(INTDIR)\IceEditDoc.obj"
	-@erase "$(INTDIR)\IceEditDoc.sbr"
	-@erase "$(INTDIR)\IceEditSrvrItem.obj"
	-@erase "$(INTDIR)\IceEditSrvrItem.sbr"
	-@erase "$(INTDIR)\IceEditView.obj"
	-@erase "$(INTDIR)\IceEditView.sbr"
	-@erase "$(INTDIR)\IceMDIChildWnd.obj"
	-@erase "$(INTDIR)\IceMDIChildWnd.sbr"
	-@erase "$(INTDIR)\IceMDIFrameWnd.obj"
	-@erase "$(INTDIR)\IceMDIFrameWnd.sbr"
	-@erase "$(INTDIR)\IceMenu.obj"
	-@erase "$(INTDIR)\IceMenu.sbr"
	-@erase "$(INTDIR)\IceStatic.obj"
	-@erase "$(INTDIR)\IceStatic.sbr"
	-@erase "$(INTDIR)\IceStatusBar.obj"
	-@erase "$(INTDIR)\IceStatusBar.sbr"
	-@erase "$(INTDIR)\IceToolBar.obj"
	-@erase "$(INTDIR)\IceToolBar.sbr"
	-@erase "$(INTDIR)\InfoView.obj"
	-@erase "$(INTDIR)\InfoView.sbr"
	-@erase "$(INTDIR)\InvisibleFrm.obj"
	-@erase "$(INTDIR)\InvisibleFrm.sbr"
	-@erase "$(INTDIR)\iPad.obj"
	-@erase "$(INTDIR)\iPad.pch"
	-@erase "$(INTDIR)\iPad.res"
	-@erase "$(INTDIR)\iPad.sbr"
	-@erase "$(INTDIR)\iPad.tlb"
	-@erase "$(INTDIR)\iPadDoc.obj"
	-@erase "$(INTDIR)\iPadDoc.sbr"
	-@erase "$(INTDIR)\iPadView.obj"
	-@erase "$(INTDIR)\iPadView.sbr"
	-@erase "$(INTDIR)\IpFrame.obj"
	-@erase "$(INTDIR)\IpFrame.sbr"
	-@erase "$(INTDIR)\ISBPaneInfo.obj"
	-@erase "$(INTDIR)\ISBPaneInfo.sbr"
	-@erase "$(INTDIR)\ISBPaneText.obj"
	-@erase "$(INTDIR)\ISBPaneText.sbr"
	-@erase "$(INTDIR)\JavaCntrItem.obj"
	-@erase "$(INTDIR)\JavaCntrItem.sbr"
	-@erase "$(INTDIR)\JavaDoc.obj"
	-@erase "$(INTDIR)\JavaDoc.sbr"
	-@erase "$(INTDIR)\JavaSrvrItem.obj"
	-@erase "$(INTDIR)\JavaSrvrItem.sbr"
	-@erase "$(INTDIR)\JavaView.obj"
	-@erase "$(INTDIR)\JavaView.sbr"
	-@erase "$(INTDIR)\LeftDockingView.obj"
	-@erase "$(INTDIR)\LeftDockingView.sbr"
	-@erase "$(INTDIR)\LogView.obj"
	-@erase "$(INTDIR)\LogView.sbr"
	-@erase "$(INTDIR)\MainFrm.obj"
	-@erase "$(INTDIR)\MainFrm.sbr"
	-@erase "$(INTDIR)\MainToolBar.obj"
	-@erase "$(INTDIR)\MainToolBar.sbr"
	-@erase "$(INTDIR)\ProjectDoc.obj"
	-@erase "$(INTDIR)\ProjectDoc.sbr"
	-@erase "$(INTDIR)\ProjectView.obj"
	-@erase "$(INTDIR)\ProjectView.sbr"
	-@erase "$(INTDIR)\ScriptView.obj"
	-@erase "$(INTDIR)\ScriptView.sbr"
	-@erase "$(INTDIR)\SplitFrm.obj"
	-@erase "$(INTDIR)\SplitFrm.sbr"
	-@erase "$(INTDIR)\SrvrItem.obj"
	-@erase "$(INTDIR)\SrvrItem.sbr"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\StdAfx.sbr"
	-@erase "$(INTDIR)\TabsPage.obj"
	-@erase "$(INTDIR)\TabsPage.sbr"
	-@erase "$(INTDIR)\TipDlg.obj"
	-@erase "$(INTDIR)\TipDlg.sbr"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\vc50.pdb"
	-@erase "$(OUTDIR)\iPad.bsc"
	-@erase "$(OUTDIR)\iPad.exe"
	-@erase "$(OUTDIR)\iPad.ilk"
	-@erase "$(OUTDIR)\iPad.pdb"
	-@erase "$(OutDir)\$(TargetName).hlp"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS"\
 /D "_AFXDLL" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\iPad.pch" /Yu"stdafx.h"\
 /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.\Debug/
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o NUL /win32 
RSC_PROJ=/l 0x412 /fo"$(INTDIR)\iPad.res" /d "_DEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\iPad.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\BottomDockingView.sbr" \
	"$(INTDIR)\BuildView.sbr" \
	"$(INTDIR)\ChildFrm.sbr" \
	"$(INTDIR)\ClassView.sbr" \
	"$(INTDIR)\CntrItem.sbr" \
	"$(INTDIR)\CppCntrItem.sbr" \
	"$(INTDIR)\CppDoc.sbr" \
	"$(INTDIR)\CppSrvrItem.sbr" \
	"$(INTDIR)\CppView.sbr" \
	"$(INTDIR)\DebugView.sbr" \
	"$(INTDIR)\DefaultStatusBar.sbr" \
	"$(INTDIR)\FileView.sbr" \
	"$(INTDIR)\FindView.sbr" \
	"$(INTDIR)\FormatPage.sbr" \
	"$(INTDIR)\IceButton.sbr" \
	"$(INTDIR)\IceDockingControlBar.sbr" \
	"$(INTDIR)\IceDockingTabbedView.sbr" \
	"$(INTDIR)\IceEditCntrItem.sbr" \
	"$(INTDIR)\IceEditDoc.sbr" \
	"$(INTDIR)\IceEditSrvrItem.sbr" \
	"$(INTDIR)\IceEditView.sbr" \
	"$(INTDIR)\IceMDIChildWnd.sbr" \
	"$(INTDIR)\IceMDIFrameWnd.sbr" \
	"$(INTDIR)\IceMenu.sbr" \
	"$(INTDIR)\IceStatic.sbr" \
	"$(INTDIR)\IceStatusBar.sbr" \
	"$(INTDIR)\IceToolBar.sbr" \
	"$(INTDIR)\InfoView.sbr" \
	"$(INTDIR)\InvisibleFrm.sbr" \
	"$(INTDIR)\iPad.sbr" \
	"$(INTDIR)\iPadDoc.sbr" \
	"$(INTDIR)\iPadView.sbr" \
	"$(INTDIR)\IpFrame.sbr" \
	"$(INTDIR)\ISBPaneInfo.sbr" \
	"$(INTDIR)\ISBPaneText.sbr" \
	"$(INTDIR)\JavaCntrItem.sbr" \
	"$(INTDIR)\JavaDoc.sbr" \
	"$(INTDIR)\JavaSrvrItem.sbr" \
	"$(INTDIR)\JavaView.sbr" \
	"$(INTDIR)\LeftDockingView.sbr" \
	"$(INTDIR)\LogView.sbr" \
	"$(INTDIR)\MainFrm.sbr" \
	"$(INTDIR)\MainToolBar.sbr" \
	"$(INTDIR)\ProjectDoc.sbr" \
	"$(INTDIR)\ProjectView.sbr" \
	"$(INTDIR)\ScriptView.sbr" \
	"$(INTDIR)\SplitFrm.sbr" \
	"$(INTDIR)\SrvrItem.sbr" \
	"$(INTDIR)\StdAfx.sbr" \
	"$(INTDIR)\TabsPage.sbr" \
	"$(INTDIR)\TipDlg.sbr"

"$(OUTDIR)\iPad.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=.\expat\xmltok.lib .\expat\xmlparse.lib /nologo /subsystem:windows\
 /incremental:yes /pdb:"$(OUTDIR)\iPad.pdb" /debug /machine:I386\
 /out:"$(OUTDIR)\iPad.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\BottomDockingView.obj" \
	"$(INTDIR)\BuildView.obj" \
	"$(INTDIR)\ChildFrm.obj" \
	"$(INTDIR)\ClassView.obj" \
	"$(INTDIR)\CntrItem.obj" \
	"$(INTDIR)\CppCntrItem.obj" \
	"$(INTDIR)\CppDoc.obj" \
	"$(INTDIR)\CppSrvrItem.obj" \
	"$(INTDIR)\CppView.obj" \
	"$(INTDIR)\DebugView.obj" \
	"$(INTDIR)\DefaultStatusBar.obj" \
	"$(INTDIR)\FileView.obj" \
	"$(INTDIR)\FindView.obj" \
	"$(INTDIR)\FormatPage.obj" \
	"$(INTDIR)\IceButton.obj" \
	"$(INTDIR)\IceDockingControlBar.obj" \
	"$(INTDIR)\IceDockingTabbedView.obj" \
	"$(INTDIR)\IceEditCntrItem.obj" \
	"$(INTDIR)\IceEditDoc.obj" \
	"$(INTDIR)\IceEditSrvrItem.obj" \
	"$(INTDIR)\IceEditView.obj" \
	"$(INTDIR)\IceMDIChildWnd.obj" \
	"$(INTDIR)\IceMDIFrameWnd.obj" \
	"$(INTDIR)\IceMenu.obj" \
	"$(INTDIR)\IceStatic.obj" \
	"$(INTDIR)\IceStatusBar.obj" \
	"$(INTDIR)\IceToolBar.obj" \
	"$(INTDIR)\InfoView.obj" \
	"$(INTDIR)\InvisibleFrm.obj" \
	"$(INTDIR)\iPad.obj" \
	"$(INTDIR)\iPad.res" \
	"$(INTDIR)\iPadDoc.obj" \
	"$(INTDIR)\iPadView.obj" \
	"$(INTDIR)\IpFrame.obj" \
	"$(INTDIR)\ISBPaneInfo.obj" \
	"$(INTDIR)\ISBPaneText.obj" \
	"$(INTDIR)\JavaCntrItem.obj" \
	"$(INTDIR)\JavaDoc.obj" \
	"$(INTDIR)\JavaSrvrItem.obj" \
	"$(INTDIR)\JavaView.obj" \
	"$(INTDIR)\LeftDockingView.obj" \
	"$(INTDIR)\LogView.obj" \
	"$(INTDIR)\MainFrm.obj" \
	"$(INTDIR)\MainToolBar.obj" \
	"$(INTDIR)\ProjectDoc.obj" \
	"$(INTDIR)\ProjectView.obj" \
	"$(INTDIR)\ScriptView.obj" \
	"$(INTDIR)\SplitFrm.obj" \
	"$(INTDIR)\SrvrItem.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\TabsPage.obj" \
	"$(INTDIR)\TipDlg.obj"

"$(OUTDIR)\iPad.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(CFG)" == "iPad - Win32 Release" || "$(CFG)" == "iPad - Win32 Debug"
SOURCE=.\BottomDockingView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_BOTTO=\
	".\BottomDockingView.h"\
	".\IceDockingControlBar.h"\
	".\IceDockingTabbedView.h"\
	".\iPad.h"\
	

"$(INTDIR)\BottomDockingView.obj" : $(SOURCE) $(DEP_CPP_BOTTO) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_BOTTO=\
	".\BottomDockingView.h"\
	".\IceDockingControlBar.h"\
	".\IceDockingTabbedView.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\BottomDockingView.obj"	"$(INTDIR)\BottomDockingView.sbr" : $(SOURCE)\
 $(DEP_CPP_BOTTO) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\BuildView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_BUILD=\
	".\BuildView.h"\
	".\iPad.h"\
	

"$(INTDIR)\BuildView.obj" : $(SOURCE) $(DEP_CPP_BUILD) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_BUILD=\
	".\BuildView.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\BuildView.obj"	"$(INTDIR)\BuildView.sbr" : $(SOURCE)\
 $(DEP_CPP_BUILD) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\ChildFrm.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_CHILD=\
	".\ChildFrm.h"\
	".\IceMDIChildWnd.h"\
	".\IceMenu.h"\
	".\iPad.h"\
	

"$(INTDIR)\ChildFrm.obj" : $(SOURCE) $(DEP_CPP_CHILD) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_CHILD=\
	".\ChildFrm.h"\
	".\IceMDIChildWnd.h"\
	".\IceMenu.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\ChildFrm.obj"	"$(INTDIR)\ChildFrm.sbr" : $(SOURCE) $(DEP_CPP_CHILD)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\ClassView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_CLASS=\
	".\ClassView.h"\
	".\iPad.h"\
	

"$(INTDIR)\ClassView.obj" : $(SOURCE) $(DEP_CPP_CLASS) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_CLASS=\
	".\ClassView.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\ClassView.obj"	"$(INTDIR)\ClassView.sbr" : $(SOURCE)\
 $(DEP_CPP_CLASS) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\CntrItem.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_CNTRI=\
	".\CntrItem.h"\
	".\iPad.h"\
	".\iPadDoc.h"\
	".\iPadView.h"\
	

"$(INTDIR)\CntrItem.obj" : $(SOURCE) $(DEP_CPP_CNTRI) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_CNTRI=\
	".\CntrItem.h"\
	".\iPad.h"\
	".\iPadDoc.h"\
	".\iPadView.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\CntrItem.obj"	"$(INTDIR)\CntrItem.sbr" : $(SOURCE) $(DEP_CPP_CNTRI)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\CppCntrItem.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_CPPCN=\
	".\CppCntrItem.h"\
	".\CppDoc.h"\
	".\CppView.h"\
	".\IceEditDoc.h"\
	".\IceEditView.h"\
	".\iPad.h"\
	

"$(INTDIR)\CppCntrItem.obj" : $(SOURCE) $(DEP_CPP_CPPCN) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_CPPCN=\
	".\CppCntrItem.h"\
	".\CppDoc.h"\
	".\CppView.h"\
	".\IceEditDoc.h"\
	".\IceEditView.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\CppCntrItem.obj"	"$(INTDIR)\CppCntrItem.sbr" : $(SOURCE)\
 $(DEP_CPP_CPPCN) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\CppDoc.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_CPPDO=\
	".\CppCntrItem.h"\
	".\CppDoc.h"\
	".\CppSrvrItem.h"\
	".\IceEditDoc.h"\
	".\iPad.h"\
	

"$(INTDIR)\CppDoc.obj" : $(SOURCE) $(DEP_CPP_CPPDO) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_CPPDO=\
	".\CppCntrItem.h"\
	".\CppDoc.h"\
	".\CppSrvrItem.h"\
	".\IceEditDoc.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\CppDoc.obj"	"$(INTDIR)\CppDoc.sbr" : $(SOURCE) $(DEP_CPP_CPPDO)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\CppSrvrItem.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_CPPSR=\
	".\CppCntrItem.h"\
	".\CppDoc.h"\
	".\CppSrvrItem.h"\
	".\IceEditDoc.h"\
	".\iPad.h"\
	

"$(INTDIR)\CppSrvrItem.obj" : $(SOURCE) $(DEP_CPP_CPPSR) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_CPPSR=\
	".\CppCntrItem.h"\
	".\CppDoc.h"\
	".\CppSrvrItem.h"\
	".\IceEditDoc.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\CppSrvrItem.obj"	"$(INTDIR)\CppSrvrItem.sbr" : $(SOURCE)\
 $(DEP_CPP_CPPSR) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\CppView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_CPPVI=\
	".\CppCntrItem.h"\
	".\CppDoc.h"\
	".\CppView.h"\
	".\IceEditDoc.h"\
	".\IceEditView.h"\
	".\iPad.h"\
	

"$(INTDIR)\CppView.obj" : $(SOURCE) $(DEP_CPP_CPPVI) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_CPPVI=\
	".\CppCntrItem.h"\
	".\CppDoc.h"\
	".\CppView.h"\
	".\IceEditDoc.h"\
	".\IceEditView.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\CppView.obj"	"$(INTDIR)\CppView.sbr" : $(SOURCE) $(DEP_CPP_CPPVI)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\DebugView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_DEBUG=\
	".\DebugView.h"\
	".\iPad.h"\
	

"$(INTDIR)\DebugView.obj" : $(SOURCE) $(DEP_CPP_DEBUG) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_DEBUG=\
	".\DebugView.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\DebugView.obj"	"$(INTDIR)\DebugView.sbr" : $(SOURCE)\
 $(DEP_CPP_DEBUG) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\DefaultStatusBar.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_DEFAU=\
	".\DefaultStatusBar.h"\
	".\IceStatusBar.h"\
	".\iPad.h"\
	".\ISBPaneInfo.h"\
	

"$(INTDIR)\DefaultStatusBar.obj" : $(SOURCE) $(DEP_CPP_DEFAU) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_DEFAU=\
	".\DefaultStatusBar.h"\
	".\IceStatusBar.h"\
	".\iPad.h"\
	".\ISBPaneInfo.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\DefaultStatusBar.obj"	"$(INTDIR)\DefaultStatusBar.sbr" : $(SOURCE)\
 $(DEP_CPP_DEFAU) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\FileView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_FILEV=\
	".\FileView.h"\
	".\iPad.h"\
	

"$(INTDIR)\FileView.obj" : $(SOURCE) $(DEP_CPP_FILEV) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_FILEV=\
	".\expat\xmlparse.h"\
	".\FileView.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\FileView.obj"	"$(INTDIR)\FileView.sbr" : $(SOURCE) $(DEP_CPP_FILEV)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\FindView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_FINDV=\
	".\FindView.h"\
	".\iPad.h"\
	

"$(INTDIR)\FindView.obj" : $(SOURCE) $(DEP_CPP_FINDV) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_FINDV=\
	".\FindView.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\FindView.obj"	"$(INTDIR)\FindView.sbr" : $(SOURCE) $(DEP_CPP_FINDV)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\FormatPage.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_FORMA=\
	".\FormatPage.h"\
	".\iPad.h"\
	

"$(INTDIR)\FormatPage.obj" : $(SOURCE) $(DEP_CPP_FORMA) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_FORMA=\
	".\FormatPage.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\FormatPage.obj"	"$(INTDIR)\FormatPage.sbr" : $(SOURCE)\
 $(DEP_CPP_FORMA) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\IceButton.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ICEBU=\
	".\IceButton.h"\
	".\iPad.h"\
	

"$(INTDIR)\IceButton.obj" : $(SOURCE) $(DEP_CPP_ICEBU) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ICEBU=\
	".\IceButton.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\IceButton.obj"	"$(INTDIR)\IceButton.sbr" : $(SOURCE)\
 $(DEP_CPP_ICEBU) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\IceDockingControlBar.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ICEDO=\
	".\IceDockingControlBar.h"\
	".\iPad.h"\
	

"$(INTDIR)\IceDockingControlBar.obj" : $(SOURCE) $(DEP_CPP_ICEDO) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ICEDO=\
	".\IceDockingControlBar.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\IceDockingControlBar.obj"	"$(INTDIR)\IceDockingControlBar.sbr" : \
$(SOURCE) $(DEP_CPP_ICEDO) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\IceDockingTabbedView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ICEDOC=\
	".\IceDockingControlBar.h"\
	".\IceDockingTabbedView.h"\
	".\iPad.h"\
	

"$(INTDIR)\IceDockingTabbedView.obj" : $(SOURCE) $(DEP_CPP_ICEDOC) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ICEDOC=\
	".\IceDockingControlBar.h"\
	".\IceDockingTabbedView.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\IceDockingTabbedView.obj"	"$(INTDIR)\IceDockingTabbedView.sbr" : \
$(SOURCE) $(DEP_CPP_ICEDOC) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\IceEditCntrItem.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ICEED=\
	".\IceEditCntrItem.h"\
	".\IceEditDoc.h"\
	".\IceEditView.h"\
	".\iPad.h"\
	

"$(INTDIR)\IceEditCntrItem.obj" : $(SOURCE) $(DEP_CPP_ICEED) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ICEED=\
	".\IceEditCntrItem.h"\
	".\IceEditDoc.h"\
	".\IceEditView.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\IceEditCntrItem.obj"	"$(INTDIR)\IceEditCntrItem.sbr" : $(SOURCE)\
 $(DEP_CPP_ICEED) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\IceEditDoc.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ICEEDI=\
	".\IceEditCntrItem.h"\
	".\IceEditDoc.h"\
	".\IceEditSrvrItem.h"\
	".\IceEditView.h"\
	".\iPad.h"\
	

"$(INTDIR)\IceEditDoc.obj" : $(SOURCE) $(DEP_CPP_ICEEDI) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ICEEDI=\
	".\IceEditCntrItem.h"\
	".\IceEditDoc.h"\
	".\IceEditSrvrItem.h"\
	".\IceEditView.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\IceEditDoc.obj"	"$(INTDIR)\IceEditDoc.sbr" : $(SOURCE)\
 $(DEP_CPP_ICEEDI) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\IceEditSrvrItem.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ICEEDIT=\
	".\IceEditCntrItem.h"\
	".\IceEditDoc.h"\
	".\IceEditSrvrItem.h"\
	".\iPad.h"\
	

"$(INTDIR)\IceEditSrvrItem.obj" : $(SOURCE) $(DEP_CPP_ICEEDIT) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ICEEDIT=\
	".\IceEditCntrItem.h"\
	".\IceEditDoc.h"\
	".\IceEditSrvrItem.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\IceEditSrvrItem.obj"	"$(INTDIR)\IceEditSrvrItem.sbr" : $(SOURCE)\
 $(DEP_CPP_ICEEDIT) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\IceEditView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ICEEDITV=\
	".\IceEditCntrItem.h"\
	".\IceEditDoc.h"\
	".\IceEditView.h"\
	".\iPad.h"\
	

"$(INTDIR)\IceEditView.obj" : $(SOURCE) $(DEP_CPP_ICEEDITV) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ICEEDITV=\
	".\IceEditCntrItem.h"\
	".\IceEditDoc.h"\
	".\IceEditView.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\IceEditView.obj"	"$(INTDIR)\IceEditView.sbr" : $(SOURCE)\
 $(DEP_CPP_ICEEDITV) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\IceMDIChildWnd.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ICEMD=\
	".\IceMDIChildWnd.h"\
	".\IceMenu.h"\
	".\iPad.h"\
	

"$(INTDIR)\IceMDIChildWnd.obj" : $(SOURCE) $(DEP_CPP_ICEMD) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ICEMD=\
	".\IceMDIChildWnd.h"\
	".\IceMenu.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\IceMDIChildWnd.obj"	"$(INTDIR)\IceMDIChildWnd.sbr" : $(SOURCE)\
 $(DEP_CPP_ICEMD) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\IceMDIFrameWnd.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ICEMDI=\
	".\IceMDIFrameWnd.h"\
	".\IceMenu.h"\
	".\iPad.h"\
	

"$(INTDIR)\IceMDIFrameWnd.obj" : $(SOURCE) $(DEP_CPP_ICEMDI) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ICEMDI=\
	".\IceMDIFrameWnd.h"\
	".\IceMenu.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\IceMDIFrameWnd.obj"	"$(INTDIR)\IceMDIFrameWnd.sbr" : $(SOURCE)\
 $(DEP_CPP_ICEMDI) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\IceMenu.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ICEME=\
	".\IceMenu.h"\
	".\iPad.h"\
	

"$(INTDIR)\IceMenu.obj" : $(SOURCE) $(DEP_CPP_ICEME) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ICEME=\
	".\IceMenu.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\IceMenu.obj"	"$(INTDIR)\IceMenu.sbr" : $(SOURCE) $(DEP_CPP_ICEME)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\IceStatic.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ICEST=\
	".\IceStatic.h"\
	".\iPad.h"\
	

"$(INTDIR)\IceStatic.obj" : $(SOURCE) $(DEP_CPP_ICEST) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ICEST=\
	".\IceStatic.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\IceStatic.obj"	"$(INTDIR)\IceStatic.sbr" : $(SOURCE)\
 $(DEP_CPP_ICEST) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\IceStatusBar.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ICESTA=\
	".\IceStatusBar.h"\
	".\iPad.h"\
	".\ISBPaneInfo.h"\
	

"$(INTDIR)\IceStatusBar.obj" : $(SOURCE) $(DEP_CPP_ICESTA) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ICESTA=\
	".\IceStatusBar.h"\
	".\iPad.h"\
	".\ISBPaneInfo.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\IceStatusBar.obj"	"$(INTDIR)\IceStatusBar.sbr" : $(SOURCE)\
 $(DEP_CPP_ICESTA) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\IceToolBar.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ICETO=\
	".\IceToolBar.h"\
	".\iPad.h"\
	

"$(INTDIR)\IceToolBar.obj" : $(SOURCE) $(DEP_CPP_ICETO) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ICETO=\
	".\IceToolBar.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\IceToolBar.obj"	"$(INTDIR)\IceToolBar.sbr" : $(SOURCE)\
 $(DEP_CPP_ICETO) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\InfoView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_INFOV=\
	".\InfoView.h"\
	".\iPad.h"\
	

"$(INTDIR)\InfoView.obj" : $(SOURCE) $(DEP_CPP_INFOV) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_INFOV=\
	".\InfoView.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\InfoView.obj"	"$(INTDIR)\InfoView.sbr" : $(SOURCE) $(DEP_CPP_INFOV)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\InvisibleFrm.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_INVIS=\
	".\InvisibleFrm.h"\
	".\iPad.h"\
	".\StdAfx.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\InvisibleFrm.obj" : $(SOURCE) $(DEP_CPP_INVIS) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_INVIS=\
	".\InvisibleFrm.h"\
	".\iPad.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\InvisibleFrm.obj"	"$(INTDIR)\InvisibleFrm.sbr" : $(SOURCE)\
 $(DEP_CPP_INVIS) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\iPad.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_IPAD_=\
	".\BottomDockingView.h"\
	".\ChildFrm.h"\
	".\CppDoc.h"\
	".\CppView.h"\
	".\DefaultStatusBar.h"\
	".\FormatPage.h"\
	".\IceDockingControlBar.h"\
	".\IceDockingTabbedView.h"\
	".\IceEditDoc.h"\
	".\IceEditView.h"\
	".\IceMDIChildWnd.h"\
	".\IceMDIFrameWnd.h"\
	".\IceMenu.h"\
	".\IceStatusBar.h"\
	".\IceToolBar.h"\
	".\iPad.h"\
	".\iPadDoc.h"\
	".\iPadView.h"\
	".\IpFrame.h"\
	".\ISBPaneInfo.h"\
	".\JavaDoc.h"\
	".\JavaView.h"\
	".\LeftDockingView.h"\
	".\MainFrm.h"\
	".\MainToolBar.h"\
	".\SplitFrm.h"\
	".\TabsPage.h"\
	

"$(INTDIR)\iPad.obj" : $(SOURCE) $(DEP_CPP_IPAD_) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_IPAD_=\
	".\BottomDockingView.h"\
	".\ChildFrm.h"\
	".\CppDoc.h"\
	".\CppView.h"\
	".\DefaultStatusBar.h"\
	".\FormatPage.h"\
	".\IceDockingControlBar.h"\
	".\IceDockingTabbedView.h"\
	".\IceEditDoc.h"\
	".\IceEditView.h"\
	".\IceMDIChildWnd.h"\
	".\IceMDIFrameWnd.h"\
	".\IceMenu.h"\
	".\IceStatusBar.h"\
	".\IceToolBar.h"\
	".\InvisibleFrm.h"\
	".\iPad.h"\
	".\iPadDoc.h"\
	".\iPadView.h"\
	".\IpFrame.h"\
	".\ISBPaneInfo.h"\
	".\JavaDoc.h"\
	".\JavaView.h"\
	".\LeftDockingView.h"\
	".\MainFrm.h"\
	".\MainToolBar.h"\
	".\ProjectDoc.h"\
	".\ProjectView.h"\
	".\SplitFrm.h"\
	".\TabsPage.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\iPad.obj"	"$(INTDIR)\iPad.sbr" : $(SOURCE) $(DEP_CPP_IPAD_)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\hlp\iPad.hpj
USERDEP__IPAD_H="$(ProjDir)\hlp\AfxCore.rtf"	"$(ProjDir)\hlp\AfxPrint.rtf"	

!IF  "$(CFG)" == "iPad - Win32 Release"

OutDir=.\Release
ProjDir=.
TargetName=iPad
InputPath=.\hlp\iPad.hpj

"$(OutDir)\$(TargetName).hlp"	 : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"\
 $(USERDEP__IPAD_H)
	call "$(ProjDir)\makehelp.bat"

!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

OutDir=.\Debug
ProjDir=.
TargetName=iPad
InputPath=.\hlp\iPad.hpj

"$(OutDir)\$(TargetName).hlp"	 : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"\
 $(USERDEP__IPAD_H)
	call "$(ProjDir)\makehelp.bat"

!ENDIF 

SOURCE=.\iPad.odl

!IF  "$(CFG)" == "iPad - Win32 Release"

MTL_SWITCHES=/nologo /D "NDEBUG" /tlb "$(OUTDIR)\iPad.tlb" /mktyplib203 /o NUL\
 /win32 

"$(OUTDIR)\iPad.tlb" : $(SOURCE) "$(OUTDIR)"
	$(MTL) @<<
  $(MTL_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

MTL_SWITCHES=/nologo /D "_DEBUG" /tlb "$(OUTDIR)\iPad.tlb" /mktyplib203 /o NUL\
 /win32 

"$(OUTDIR)\iPad.tlb" : $(SOURCE) "$(OUTDIR)"
	$(MTL) @<<
  $(MTL_SWITCHES) $(SOURCE)
<<


!ENDIF 

SOURCE=.\iPad.rc
DEP_RSC_IPAD_R=\
	".\res\botombar.bmp"\
	".\res\idr_cppt.ico"\
	".\res\idr_java.ico"\
	".\res\idr_proj.ico"\
	".\res\iPad.ico"\
	".\res\iPad.rc2"\
	".\res\iPadDoc.ico"\
	".\res\IToolbar.bmp"\
	".\res\leftbar.bmp"\
	".\res\ListItem.bmp"\
	".\res\litebulb.bmp"\
	".\res\mainfram.bmp"\
	".\res\MenuChk.bmp"\
	".\res\mrufile.bmp"\
	".\res\reservoi.bmp"\
	".\res\sysbar.bmp"\
	".\res\WrkSpace.bmp"\
	

!IF  "$(CFG)" == "iPad - Win32 Release"


"$(INTDIR)\iPad.res" : $(SOURCE) $(DEP_RSC_IPAD_R) "$(INTDIR)"
	$(RSC) /l 0x412 /fo"$(INTDIR)\iPad.res" /i "Release" /d "NDEBUG" /d "_AFXDLL"\
 $(SOURCE)


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"


"$(INTDIR)\iPad.res" : $(SOURCE) $(DEP_RSC_IPAD_R) "$(INTDIR)"
	$(RSC) /l 0x412 /fo"$(INTDIR)\iPad.res" /i "Debug" /d "_DEBUG" /d "_AFXDLL"\
 $(SOURCE)


!ENDIF 

SOURCE=.\iPadDoc.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_IPADD=\
	".\CntrItem.h"\
	".\iPad.h"\
	".\iPadDoc.h"\
	".\SrvrItem.h"\
	

"$(INTDIR)\iPadDoc.obj" : $(SOURCE) $(DEP_CPP_IPADD) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_IPADD=\
	".\CntrItem.h"\
	".\iPad.h"\
	".\iPadDoc.h"\
	".\SrvrItem.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\iPadDoc.obj"	"$(INTDIR)\iPadDoc.sbr" : $(SOURCE) $(DEP_CPP_IPADD)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\iPadView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_IPADV=\
	".\CntrItem.h"\
	".\iPad.h"\
	".\iPadDoc.h"\
	".\iPadView.h"\
	

"$(INTDIR)\iPadView.obj" : $(SOURCE) $(DEP_CPP_IPADV) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_IPADV=\
	".\CntrItem.h"\
	".\iPad.h"\
	".\iPadDoc.h"\
	".\iPadView.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\iPadView.obj"	"$(INTDIR)\iPadView.sbr" : $(SOURCE) $(DEP_CPP_IPADV)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\IpFrame.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_IPFRA=\
	".\iPad.h"\
	".\IpFrame.h"\
	

"$(INTDIR)\IpFrame.obj" : $(SOURCE) $(DEP_CPP_IPFRA) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_IPFRA=\
	".\iPad.h"\
	".\IpFrame.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\IpFrame.obj"	"$(INTDIR)\IpFrame.sbr" : $(SOURCE) $(DEP_CPP_IPFRA)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\ISBPaneInfo.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ISBPA=\
	".\IceStatusBar.h"\
	".\iPad.h"\
	".\ISBPaneInfo.h"\
	

"$(INTDIR)\ISBPaneInfo.obj" : $(SOURCE) $(DEP_CPP_ISBPA) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ISBPA=\
	".\IceStatusBar.h"\
	".\iPad.h"\
	".\ISBPaneInfo.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\ISBPaneInfo.obj"	"$(INTDIR)\ISBPaneInfo.sbr" : $(SOURCE)\
 $(DEP_CPP_ISBPA) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\ISBPaneText.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_ISBPAN=\
	".\IceStatusBar.h"\
	".\iPad.h"\
	".\ISBPaneInfo.h"\
	".\ISBPaneText.h"\
	

"$(INTDIR)\ISBPaneText.obj" : $(SOURCE) $(DEP_CPP_ISBPAN) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_ISBPAN=\
	".\IceStatusBar.h"\
	".\iPad.h"\
	".\ISBPaneInfo.h"\
	".\ISBPaneText.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\ISBPaneText.obj"	"$(INTDIR)\ISBPaneText.sbr" : $(SOURCE)\
 $(DEP_CPP_ISBPAN) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\JavaCntrItem.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_JAVAC=\
	".\IceEditDoc.h"\
	".\IceEditView.h"\
	".\iPad.h"\
	".\JavaCntrItem.h"\
	".\JavaDoc.h"\
	".\JavaView.h"\
	

"$(INTDIR)\JavaCntrItem.obj" : $(SOURCE) $(DEP_CPP_JAVAC) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_JAVAC=\
	".\IceEditDoc.h"\
	".\IceEditView.h"\
	".\iPad.h"\
	".\JavaCntrItem.h"\
	".\JavaDoc.h"\
	".\JavaView.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\JavaCntrItem.obj"	"$(INTDIR)\JavaCntrItem.sbr" : $(SOURCE)\
 $(DEP_CPP_JAVAC) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\JavaDoc.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_JAVAD=\
	".\IceEditDoc.h"\
	".\iPad.h"\
	".\JavaCntrItem.h"\
	".\JavaDoc.h"\
	".\JavaSrvrItem.h"\
	

"$(INTDIR)\JavaDoc.obj" : $(SOURCE) $(DEP_CPP_JAVAD) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_JAVAD=\
	".\IceEditDoc.h"\
	".\iPad.h"\
	".\JavaCntrItem.h"\
	".\JavaDoc.h"\
	".\JavaSrvrItem.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\JavaDoc.obj"	"$(INTDIR)\JavaDoc.sbr" : $(SOURCE) $(DEP_CPP_JAVAD)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\JavaSrvrItem.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_JAVAS=\
	".\IceEditDoc.h"\
	".\iPad.h"\
	".\JavaCntrItem.h"\
	".\JavaDoc.h"\
	".\JavaSrvrItem.h"\
	

"$(INTDIR)\JavaSrvrItem.obj" : $(SOURCE) $(DEP_CPP_JAVAS) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_JAVAS=\
	".\IceEditDoc.h"\
	".\iPad.h"\
	".\JavaCntrItem.h"\
	".\JavaDoc.h"\
	".\JavaSrvrItem.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\JavaSrvrItem.obj"	"$(INTDIR)\JavaSrvrItem.sbr" : $(SOURCE)\
 $(DEP_CPP_JAVAS) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\JavaView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_JAVAV=\
	".\IceEditDoc.h"\
	".\IceEditView.h"\
	".\iPad.h"\
	".\JavaCntrItem.h"\
	".\JavaDoc.h"\
	".\JavaView.h"\
	

"$(INTDIR)\JavaView.obj" : $(SOURCE) $(DEP_CPP_JAVAV) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_JAVAV=\
	".\IceEditDoc.h"\
	".\IceEditView.h"\
	".\iPad.h"\
	".\JavaCntrItem.h"\
	".\JavaDoc.h"\
	".\JavaView.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\JavaView.obj"	"$(INTDIR)\JavaView.sbr" : $(SOURCE) $(DEP_CPP_JAVAV)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\LeftDockingView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_LEFTD=\
	".\IceDockingControlBar.h"\
	".\IceDockingTabbedView.h"\
	".\iPad.h"\
	".\LeftDockingView.h"\
	

"$(INTDIR)\LeftDockingView.obj" : $(SOURCE) $(DEP_CPP_LEFTD) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_LEFTD=\
	".\IceDockingControlBar.h"\
	".\IceDockingTabbedView.h"\
	".\iPad.h"\
	".\LeftDockingView.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\LeftDockingView.obj"	"$(INTDIR)\LeftDockingView.sbr" : $(SOURCE)\
 $(DEP_CPP_LEFTD) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\LogView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_LOGVI=\
	".\iPad.h"\
	".\LogView.h"\
	

"$(INTDIR)\LogView.obj" : $(SOURCE) $(DEP_CPP_LOGVI) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_LOGVI=\
	".\iPad.h"\
	".\LogView.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\LogView.obj"	"$(INTDIR)\LogView.sbr" : $(SOURCE) $(DEP_CPP_LOGVI)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\MainFrm.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_MAINF=\
	".\BottomDockingView.h"\
	".\BuildView.h"\
	".\ClassView.h"\
	".\DebugView.h"\
	".\DefaultStatusBar.h"\
	".\FileView.h"\
	".\FindView.h"\
	".\IceDockingControlBar.h"\
	".\IceDockingTabbedView.h"\
	".\IceMDIFrameWnd.h"\
	".\IceMenu.h"\
	".\IceStatusBar.h"\
	".\IceToolBar.h"\
	".\InfoView.h"\
	".\iPad.h"\
	".\ISBPaneInfo.h"\
	".\LeftDockingView.h"\
	".\LogView.h"\
	".\MainFrm.h"\
	".\MainToolBar.h"\
	".\ScriptView.h"\
	

"$(INTDIR)\MainFrm.obj" : $(SOURCE) $(DEP_CPP_MAINF) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_MAINF=\
	".\BottomDockingView.h"\
	".\BuildView.h"\
	".\ClassView.h"\
	".\DebugView.h"\
	".\DefaultStatusBar.h"\
	".\FileView.h"\
	".\FindView.h"\
	".\IceDockingControlBar.h"\
	".\IceDockingTabbedView.h"\
	".\IceMDIFrameWnd.h"\
	".\IceMenu.h"\
	".\IceStatusBar.h"\
	".\IceToolBar.h"\
	".\InfoView.h"\
	".\iPad.h"\
	".\ISBPaneInfo.h"\
	".\LeftDockingView.h"\
	".\LogView.h"\
	".\MainFrm.h"\
	".\MainToolBar.h"\
	".\ScriptView.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\MainFrm.obj"	"$(INTDIR)\MainFrm.sbr" : $(SOURCE) $(DEP_CPP_MAINF)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\MainToolBar.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_MAINT=\
	".\IceToolBar.h"\
	".\iPad.h"\
	".\MainToolBar.h"\
	

"$(INTDIR)\MainToolBar.obj" : $(SOURCE) $(DEP_CPP_MAINT) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_MAINT=\
	".\IceToolBar.h"\
	".\iPad.h"\
	".\MainToolBar.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\MainToolBar.obj"	"$(INTDIR)\MainToolBar.sbr" : $(SOURCE)\
 $(DEP_CPP_MAINT) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\ProjectDoc.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_PROJE=\
	".\iPad.h"\
	".\ProjectDoc.h"\
	".\ProjectView.h"\
	".\StdAfx.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\ProjectDoc.obj" : $(SOURCE) $(DEP_CPP_PROJE) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_PROJE=\
	".\iPad.h"\
	".\ProjectDoc.h"\
	".\ProjectView.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\ProjectDoc.obj"	"$(INTDIR)\ProjectDoc.sbr" : $(SOURCE)\
 $(DEP_CPP_PROJE) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\ProjectView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_PROJEC=\
	".\BottomDockingView.h"\
	".\ClassView.h"\
	".\DefaultStatusBar.h"\
	".\FileView.h"\
	".\IceDockingControlBar.h"\
	".\IceDockingTabbedView.h"\
	".\IceMDIFrameWnd.h"\
	".\IceMenu.h"\
	".\IceStatusBar.h"\
	".\IceToolBar.h"\
	".\iPad.h"\
	".\ISBPaneInfo.h"\
	".\LeftDockingView.h"\
	".\MainFrm.h"\
	".\MainToolBar.h"\
	".\ProjectView.h"\
	".\StdAfx.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\ProjectView.obj" : $(SOURCE) $(DEP_CPP_PROJEC) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_PROJEC=\
	".\BottomDockingView.h"\
	".\ClassView.h"\
	".\DefaultStatusBar.h"\
	".\FileView.h"\
	".\IceDockingControlBar.h"\
	".\IceDockingTabbedView.h"\
	".\IceMDIFrameWnd.h"\
	".\IceMenu.h"\
	".\IceStatusBar.h"\
	".\IceToolBar.h"\
	".\iPad.h"\
	".\ISBPaneInfo.h"\
	".\LeftDockingView.h"\
	".\MainFrm.h"\
	".\MainToolBar.h"\
	".\ProjectView.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\ProjectView.obj"	"$(INTDIR)\ProjectView.sbr" : $(SOURCE)\
 $(DEP_CPP_PROJEC) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\ScriptView.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_SCRIP=\
	".\iPad.h"\
	".\ScriptView.h"\
	

"$(INTDIR)\ScriptView.obj" : $(SOURCE) $(DEP_CPP_SCRIP) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_SCRIP=\
	".\iPad.h"\
	".\ScriptView.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\ScriptView.obj"	"$(INTDIR)\ScriptView.sbr" : $(SOURCE)\
 $(DEP_CPP_SCRIP) "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\SplitFrm.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_SPLIT=\
	".\IceMDIChildWnd.h"\
	".\IceMenu.h"\
	".\iPad.h"\
	".\SplitFrm.h"\
	

"$(INTDIR)\SplitFrm.obj" : $(SOURCE) $(DEP_CPP_SPLIT) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_SPLIT=\
	".\IceMDIChildWnd.h"\
	".\IceMenu.h"\
	".\iPad.h"\
	".\SplitFrm.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\SplitFrm.obj"	"$(INTDIR)\SplitFrm.sbr" : $(SOURCE) $(DEP_CPP_SPLIT)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\SrvrItem.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_SRVRI=\
	".\CntrItem.h"\
	".\iPad.h"\
	".\iPadDoc.h"\
	".\SrvrItem.h"\
	

"$(INTDIR)\SrvrItem.obj" : $(SOURCE) $(DEP_CPP_SRVRI) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_SRVRI=\
	".\CntrItem.h"\
	".\iPad.h"\
	".\iPadDoc.h"\
	".\SrvrItem.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\SrvrItem.obj"	"$(INTDIR)\SrvrItem.sbr" : $(SOURCE) $(DEP_CPP_SRVRI)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_STDAF=\
	".\StdAfx.h"\
	
CPP_SWITCHES=/nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D\
 "_AFXDLL" /Fp"$(INTDIR)\iPad.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\"\
 /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\iPad.pch" : $(SOURCE) $(DEP_CPP_STDAF)\
 "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_STDAF=\
	".\StdAfx.h"\
	".\TipDlg.h"\
	
CPP_SWITCHES=/nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D\
 "_WINDOWS" /D "_AFXDLL" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\iPad.pch" /Yc"stdafx.h"\
 /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\StdAfx.sbr"	"$(INTDIR)\iPad.pch" : $(SOURCE)\
 $(DEP_CPP_STDAF) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ENDIF 

SOURCE=.\TabsPage.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_TABSP=\
	".\iPad.h"\
	".\TabsPage.h"\
	

"$(INTDIR)\TabsPage.obj" : $(SOURCE) $(DEP_CPP_TABSP) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_TABSP=\
	".\iPad.h"\
	".\TabsPage.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\TabsPage.obj"	"$(INTDIR)\TabsPage.sbr" : $(SOURCE) $(DEP_CPP_TABSP)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 

SOURCE=.\TipDlg.cpp

!IF  "$(CFG)" == "iPad - Win32 Release"

DEP_CPP_TIPDL=\
	".\StdAfx.h"\
	".\TipDlg.h"\
	

"$(INTDIR)\TipDlg.obj" : $(SOURCE) $(DEP_CPP_TIPDL) "$(INTDIR)"\
 "$(INTDIR)\iPad.pch"


!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

DEP_CPP_TIPDL=\
	".\TipDlg.h"\
	

"$(INTDIR)\TipDlg.obj"	"$(INTDIR)\TipDlg.sbr" : $(SOURCE) $(DEP_CPP_TIPDL)\
 "$(INTDIR)" "$(INTDIR)\iPad.pch"


!ENDIF 


!ENDIF 

