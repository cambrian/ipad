// DefaultStatusBar.cpp : implementation file
//

#include "stdafx.h"
#include "iPad.h"
#include "DefaultStatusBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDefaultStatusBar

CDefaultStatusBar::CDefaultStatusBar()
{
}

CDefaultStatusBar::~CDefaultStatusBar()
{
}


//BEGIN_MESSAGE_MAP(CDefaultStatusBar, XStatusBar)
BEGIN_MESSAGE_MAP(CDefaultStatusBar, CIceStatusBar)
	//{{AFX_MSG_MAP(CDefaultStatusBar)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDefaultStatusBar message handlers
