//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by iPad.rc
//
#define IDR_IPADTYPE_SRVR_IP            4
#define IDR_IPADTYPE_SRVR_EMB           5
#define IDR_IPADTYPE_CNTR_IP            6
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDP_FAILED_TO_CREATE            102
#define CG_IDS_PHYSICAL_MEM             103
#define IDP_SOCKETS_INIT_FAILED         104
#define CG_IDS_DISK_SPACE               105
#define CG_IDS_DISK_SPACE_UNAVAIL       106
#define IDB_LIGHTBULB                   115
#define IDD_TIP                         116
#define CG_IDS_TIPOFTHEDAY              117
#define CG_IDS_TIPOFTHEDAYMENU          118
#define CG_IDS_DIDYOUKNOW               119
#define CG_IDS_FILE_ABSENT              120
#define CG_IDP_FILE_CORRUPT             121
#define CG_IDS_TIPOFTHEDAYHELP          122
#define IDR_MAINFRAME                   128
#define IDR_IPADTYPE                    129
#define IDB_LISTITEM                    130
#define IDR_JAVATYPE                    130
#define IDB_MENUCHECK                   131
#define IDR_CPPTYPE                     131
#define IDB_BOTTOMTHUMBTACK             132
#define IDR_PROJECTTYPE                 132
#define IDB_WORKSPACE                   133
#define IDB_LEFTTHUMBTACK               135
#define IDD_TABSPAGE                    136
#define IDD_FORMATPAGE                  138
#define IDB_FILEVIEW                    140
#define IDB_CLASSVIEW                   141
#define IDR_JSTYPE                      142
#define IDR_MRUFILE                     149
#define IDR_RESERVOIR                   150
#define IDR_SYSTEMBAR                   151
#define IDC_PHYSICAL_MEM                1000
#define IDC_BULB                        1000
#define IDC_DISK_SPACE                  1001
#define IDC_STARTUP                     1001
#define IDC_TABSPAGE_FILETYPE           1002
#define IDC_NEXTTIP                     1002
#define IDC_TABSPAGE_TABSIZE            1003
#define IDC_TABSPAGE_INDENTSIZE         1004
#define IDC_TIPSTRING                   1004
#define IDC_TABSPAGE_INSERTSPACE        1005
#define IDC_TABSPAGE_KEEPTABS           1006
#define IDC_TABSPAGE_AUTOINDENT_NONE    1007
#define IDC_TABSPAGE_AUTOINDENT_DEFAULT 1008
#define IDC_TABSPAGE_AUTOINDENT_SMART   1009
#define IDC_TABSPAGE_SMARTINDENT_OPENBRACE 1010
#define IDC_TABSPAGE_SMARTINDENT_CLOSEBRACE 1011
#define IDC_TABSPAGE_SMARTINDENT_TABS   1012
#define IDC_FORMATPAGE_FILETYPE         1013
#define IDC_FORMATPAGE_FONTFACE         1014
#define IDC_FORMATPAGE_FONTSIZE         1015
#define IDC_FORMATPAGE_CHANGEFONT       1016
#define IDC_FORMATPAGE_SYNTAXCOLORS     1017
#define IDC_FORMATPAGE_EDITFILE         1018
#define IDC_FORMATPAGE_CHANGECOLOR      1019
#define IDC_FORMATPAGE_TESTAREA         1020
#define IDC_FORMATPAGE_CLEAR            1021
#define ID_CANCEL_EDIT_CNTR             32768
#define ID_CANCEL_EDIT_SRVR             32769
#define ID_VIEW_WORKSPACE_PANE          32771
#define ID_FILE_OPEN_JAVA               32772
#define ID_VIEW_OUTPUT_PANE             32773
#define ID_FILE_OPEN_CPP                32774
#define ID_FILE_NEW_JAVA                32775
#define ID_FILE_NEW_CPP                 32776
#define ID_TOOLS_FORMAT                 32777
#define ID_HELP_CODE_SAMPLE             32777
#define ID_HELP_JS_WEB                  32778
#define ID_TOOLS_OPTIONS                32781
#define ID_TOOLS_BEAUTIFY               32782
#define ID_BUILD_COMPILE_CPP            32783
#define ID_BUILD_COMPILE_JAVA           32784
#define ID_PROJECT_OPEN                 32785
#define ID_PROJECT_CLOSE                32786
#define ID_PROJECT_SAVE                 32787
#define ID_PROJECT_NEW                  32788
#define ID_DEBUG_GO                     32789
#define ID_DEBUG_STOP                   32790
#define ID_SCRIPT_NEW                   32791
#define ID_SCRIPT_OPEN                  32792
#define ID_SCRIPT_RUN                   32793
#define ID_DEBUG_BREAKPOINTS            32794
#define ID_BUILD_BUILDALL               32795
#define ID_BUILD_RUN                    32796
#define ID_BUILD_COMPILE                32797
#define ID_FILE_NEW_JSCRIPT             32805
#define ID_FILE_OPEN_JSCRIPT            32806
#define ID_BUILD_PARSE_JSCRIPT          32807
#define ID_HELP_WEB_CODE                32808
#define ID_HELP_TUTORIAL                32809
#define IDS_NOTIMPLEMENTED              61446

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        143
#define _APS_NEXT_COMMAND_VALUE         32810
#define _APS_NEXT_CONTROL_VALUE         1022
#define _APS_NEXT_SYMED_VALUE           123
#endif
#endif
