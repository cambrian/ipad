#if !defined(AFX_FORMATPAGE_H__E3623903_B0B5_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_FORMATPAGE_H__E3623903_B0B5_11D2_8EFE_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// FormatPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFormatPage dialog

class CFormatPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CFormatPage)

// Construction
public:
	CFormatPage();
	~CFormatPage();

// Dialog Data
	//{{AFX_DATA(CFormatPage)
	enum { IDD = IDD_FORMATPAGE };
	CComboBox	m_cbFileType;
	CListBox	m_lbSyntaxColors;
	CString	m_strFontFace;
	int		m_nFontSize;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CFormatPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CFormatPage)
	afx_msg void OnFormatpageChangecolor();
	afx_msg void OnFormatpageChangefont();
	afx_msg void OnFormatpageClear();
	afx_msg void OnFormatpageEditfile();
	afx_msg void OnSelchangeFormatpageFiletype();
	afx_msg void OnSelchangeFormatpageSyntaxcolors();
	afx_msg void OnDblclkFormatpageSyntaxcolors();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FORMATPAGE_H__E3623903_B0B5_11D2_8EFE_00A024E0E339__INCLUDED_)
