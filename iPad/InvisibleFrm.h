#if !defined(AFX_INVISIBLEFRM_H__35097262_B213_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_INVISIBLEFRM_H__35097262_B213_11D2_8EFE_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// InvisibleFrm.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInvisibleFrame frame

class CInvisibleFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CInvisibleFrame)
protected:
	CInvisibleFrame();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInvisibleFrame)
	public:
	virtual void ActivateFrame(int nCmdShow = -1);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CInvisibleFrame();

	// Generated message map functions
	//{{AFX_MSG(CInvisibleFrame)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INVISIBLEFRM_H__35097262_B213_11D2_8EFE_00A024E0E339__INCLUDED_)
