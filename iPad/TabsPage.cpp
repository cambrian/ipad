// TabsPage.cpp : implementation file
//

#include "stdafx.h"
#include "ipad.h"
#include "TabsPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTabsPage property page

IMPLEMENT_DYNCREATE(CTabsPage, CPropertyPage)

CTabsPage::CTabsPage() : CPropertyPage(CTabsPage::IDD)
{
	//{{AFX_DATA_INIT(CTabsPage)
	m_nIndentSize = 0;
	m_nTabSize = 0;
	m_bCloseBrace = FALSE;
	m_bOpenBrace = FALSE;
	m_bTabs = FALSE;
	//}}AFX_DATA_INIT
}

CTabsPage::~CTabsPage()
{
}

void CTabsPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTabsPage)
	DDX_Control(pDX, IDC_TABSPAGE_FILETYPE, m_cbFileType);
	DDX_Text(pDX, IDC_TABSPAGE_INDENTSIZE, m_nIndentSize);
	DDV_MinMaxInt(pDX, m_nIndentSize, 0, 20);
	DDX_Text(pDX, IDC_TABSPAGE_TABSIZE, m_nTabSize);
	DDV_MinMaxInt(pDX, m_nTabSize, 0, 20);
	DDX_Check(pDX, IDC_TABSPAGE_SMARTINDENT_CLOSEBRACE, m_bCloseBrace);
	DDX_Check(pDX, IDC_TABSPAGE_SMARTINDENT_OPENBRACE, m_bOpenBrace);
	DDX_Check(pDX, IDC_TABSPAGE_SMARTINDENT_TABS, m_bTabs);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTabsPage, CPropertyPage)
	//{{AFX_MSG_MAP(CTabsPage)
	ON_CBN_SELCHANGE(IDC_TABSPAGE_FILETYPE, OnSelchangeTabspageFiletype)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTabsPage message handlers

void CTabsPage::OnSelchangeTabspageFiletype() 
{
	// TODO: Add your control notification handler code here
	
}

BOOL CTabsPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
    m_cbFileType.AddString(_T("Default"));
    m_cbFileType.AddString(_T("Java"));
    m_cbFileType.AddString(_T("C++"));
    m_cbFileType.SelectString(1,_T("Default"));

    
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
