// BottomDockingView.cpp : implementation file
//

#include "stdafx.h"
#include "iPad.h"
#include "BottomDockingView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDockingView


CBottomDockingView::CBottomDockingView()
{
    m_sizeMin = CSize(32, 32);
    m_sizeHorz = CSize(200, 200);
    m_sizeVert = CSize(200, 200);
    m_sizeFloat = CSize(200, 200);
    m_bTracking = FALSE;
    m_bInRecalcNC = FALSE;
    m_cxEdge = 5;
    m_bDragShowContent = FALSE;
}

CBottomDockingView::~CBottomDockingView()
{
}


BEGIN_MESSAGE_MAP(CBottomDockingView, CIceDockingTabbedView)
	//{{AFX_MSG_MAP(CBottomDockingView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY(TCN_SELCHANGE, IDC_BOTTOMDOCKINGVIEW, OnTabSelChange)
	ON_NOTIFY(TCN_SELCHANGING, IDC_BOTTOMDOCKINGVIEW, OnTabSelChanging)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
//
void CBottomDockingView::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	UpdateDialogControls(pTarget, bDisableIfNoHndler);
}



/////////////////////////////////////////////////////////////////////////////
// CBottomDockingView message handlers

int CBottomDockingView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceDockingTabbedView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	//Create the Tab Control
	CRect rect;
	m_tabctrl.Create(WS_VISIBLE | WS_CHILD 
		| TCS_FOCUSNEVER | TCS_BOTTOM | TCS_FIXEDWIDTH | TCS_FORCEICONLEFT, 
		rect, this, IDC_BOTTOMDOCKINGVIEW);
	
	m_images.Create(IDB_BOTTOMTHUMBTACK, 16, 1, RGB(255,0,255));
	m_tabctrl.SetImageList(&m_images);
	
	// set "normal" GUI-font
	CFont *font = CFont::FromHandle((HFONT)::GetStockObject(DEFAULT_GUI_FONT));
	m_tabctrl.SetFont(font);

	
	return 0;
}

void CBottomDockingView::OnSize(UINT nType, int cx, int cy) 
{
	CIceDockingTabbedView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	
}

void CBottomDockingView::OnTabSelChanging(NMHDR* pNMHDR, LRESULT* pResult)
{
	CIceDockingTabbedView::OnTabSelChanging(pNMHDR, pResult);

	*pResult = FALSE;		// return FALSE to allow seletion to change
}

void CBottomDockingView::OnTabSelChange(NMHDR* pNMHDR, LRESULT* pResult)
{
	CIceDockingTabbedView::OnTabSelChange(pNMHDR, pResult);

	*pResult = 0;
}

