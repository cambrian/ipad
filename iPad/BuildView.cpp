// BuildView.cpp : implementation file
//

#include "stdafx.h"
#include "iPad.h"
#include "BuildView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CBuildView

IMPLEMENT_DYNCREATE(CBuildView, CRichEditView)

CBuildView::CBuildView()
{
	EnableAutomation();
}

CBuildView::~CBuildView()
{
}

void CBuildView::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CRichEditView::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CBuildView, CRichEditView)
	//{{AFX_MSG_MAP(CBuildView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CBuildView, CRichEditView)
	//{{AFX_DISPATCH_MAP(CBuildView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IBuildView to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {E01C5B01-AF1E-11D2-8EFE-00A024E0E339}
static const IID IID_IBuildView =
{ 0xe01c5b01, 0xaf1e, 0x11d2, { 0x8e, 0xfe, 0x0, 0xa0, 0x24, 0xe0, 0xe3, 0x39 } };

BEGIN_INTERFACE_MAP(CBuildView, CRichEditView)
	INTERFACE_PART(CBuildView, IID_IBuildView, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBuildView diagnostics

#ifdef _DEBUG
void CBuildView::AssertValid() const
{
	CRichEditView::AssertValid();
}

void CBuildView::Dump(CDumpContext& dc) const
{
	CRichEditView::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CBuildView message handlers
