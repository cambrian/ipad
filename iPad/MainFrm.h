// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__E01C5ACE_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_MAINFRM_H__E01C5ACE_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_

#include "MainToolBar.h"
#include "DefaultStatusBar.h"
#include "BottomDockingView.h"
#include "LeftDockingView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../icelib/IceMDIFrameWnd.h"

class CMainFrame : public CIceMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void ActivateFrame(int nCmdShow = -1);
	virtual void RecalcLayout(BOOL bNotify = TRUE);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CDefaultStatusBar  m_wndDefaultStatusBar;
	CMainToolBar       m_wndMainToolBar;

public:     // control bar embedded members
	CLeftDockingView     m_wndLeftDockBar;
	CBottomDockingView     m_wndBottomDockBar;


// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnInitMenu(CMenu* pMenu);
	afx_msg void OnTipofthedaymenu();
	afx_msg void OnUpdateTipofthedaymenu(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__E01C5ACE_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
