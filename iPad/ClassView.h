#if !defined(AFX_CLASSVIEW_H__E01C5B10_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_CLASSVIEW_H__E01C5B10_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_

#include <afxcview.h>

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ClassView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CClassView view

class CClassView : public CTreeView
{
protected:
	CClassView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CClassView)

// Attributes
public:
	CImageList			m_ImageList;
    CFont				m_font;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClassView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CClassView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

private:
   	HTREEITEM  m_hTIRoot;

	// Generated message map functions
protected:
	//{{AFX_MSG(CClassView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLASSVIEW_H__E01C5B10_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
