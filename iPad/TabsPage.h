#if !defined(AFX_TABSPAGE_H__E3623902_B0B5_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_TABSPAGE_H__E3623902_B0B5_11D2_8EFE_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TabsPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTabsPage dialog

class CTabsPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CTabsPage)

// Construction
public:
	CTabsPage();
	~CTabsPage();

// Dialog Data
	//{{AFX_DATA(CTabsPage)
	enum { IDD = IDD_TABSPAGE };
	CComboBox	m_cbFileType;
	int		m_nIndentSize;
	int		m_nTabSize;
	BOOL	m_bCloseBrace;
	BOOL	m_bOpenBrace;
	BOOL	m_bTabs;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CTabsPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CTabsPage)
	afx_msg void OnSelchangeTabspageFiletype();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABSPAGE_H__E3623902_B0B5_11D2_8EFE_00A024E0E339__INCLUDED_)
