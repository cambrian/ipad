// CntrItem.cpp : implementation of the CIPadCntrItem class
//

#include "stdafx.h"
#include "iPad.h"

#include "iPadDoc.h"
#include "iPadView.h"
#include "CntrItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIPadCntrItem implementation

IMPLEMENT_SERIAL(CIPadCntrItem, CRichEditCntrItem, 0)

CIPadCntrItem::CIPadCntrItem(REOBJECT* preo, CIPadDoc* pContainer)
	: CRichEditCntrItem(preo, pContainer)
{
	// TODO: add one-time construction code here
	
}

CIPadCntrItem::~CIPadCntrItem()
{
	// TODO: add cleanup code here
	
}

/////////////////////////////////////////////////////////////////////////////
// CIPadCntrItem diagnostics

#ifdef _DEBUG
void CIPadCntrItem::AssertValid() const
{
	CRichEditCntrItem::AssertValid();
}

void CIPadCntrItem::Dump(CDumpContext& dc) const
{
	CRichEditCntrItem::Dump(dc);
}
#endif

/////////////////////////////////////////////////////////////////////////////
