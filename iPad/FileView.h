#if !defined(AFX_FILEVIEW_H__E01C5B11_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_FILEVIEW_H__E01C5B11_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_

#include <afxcview.h>

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// FileView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFileView view

class CFileView : public CTreeView
{
protected:
	CFileView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CFileView)

// Attributes
public:
	CImageList			m_ImageList;
    CFont				m_font;

// Operations
public:
	void AddProject(CString strName);
	void AddFile(CString strName);
	void Initialize(CString strBuffer);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFileView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CFileView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

private:
   	HTREEITEM  m_hTIRoot;

	// Generated message map functions
protected:
	//{{AFX_MSG(CFileView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILEVIEW_H__E01C5B11_AF1E_11D2_8EFE_00A024E0E339__INCLUDED_)
