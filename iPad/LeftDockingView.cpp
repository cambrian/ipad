// LeftDockingView.cpp : implementation file
//

#include "stdafx.h"
#include "iPad.h"
#include "LeftDockingView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDockingView


CLeftDockingView::CLeftDockingView()
{
    m_sizeMin = CSize(32, 32);
    m_sizeHorz = CSize(200, 200);
    m_sizeVert = CSize(200, 200);
    m_sizeFloat = CSize(200, 200);
    m_bTracking = FALSE;
    m_bInRecalcNC = FALSE;
    m_cxEdge = 5;
    m_bDragShowContent = FALSE;
}

CLeftDockingView::~CLeftDockingView()
{
}


BEGIN_MESSAGE_MAP(CLeftDockingView, CIceDockingTabbedView)
	//{{AFX_MSG_MAP(CLeftDockingView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY(TCN_SELCHANGE, IDC_LEFTDOCKINGVIEW, OnTabSelChange)
	ON_NOTIFY(TCN_SELCHANGING, IDC_LEFTDOCKINGVIEW, OnTabSelChanging)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
//
void CLeftDockingView::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	UpdateDialogControls(pTarget, bDisableIfNoHndler);
}



/////////////////////////////////////////////////////////////////////////////
// CLeftDockingView message handlers

int CLeftDockingView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceDockingTabbedView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	//Create the Tab Control
	CRect rect;
	m_tabctrl.Create(WS_VISIBLE | WS_CHILD 
		| TCS_FOCUSNEVER | TCS_BOTTOM | TCS_TOOLTIPS
		| TCS_FORCELABELLEFT, 
		rect, this, IDC_LEFTDOCKINGVIEW);
	
	m_images.Create(IDB_LEFTTHUMBTACK, 16, 1, RGB(255,0,255));
	m_tabctrl.SetImageList(&m_images);
	
	// set "normal" GUI-font
	CFont *font = CFont::FromHandle((HFONT)::GetStockObject(DEFAULT_GUI_FONT));
	m_tabctrl.SetFont(font);

	
	return 0;
}

void CLeftDockingView::OnSize(UINT nType, int cx, int cy) 
{
	CIceDockingTabbedView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	
}

void CLeftDockingView::OnTabSelChanging(NMHDR* pNMHDR, LRESULT* pResult)
{
	CIceDockingTabbedView::OnTabSelChanging(pNMHDR, pResult);

	*pResult = FALSE;		// return FALSE to allow seletion to change
}

void CLeftDockingView::OnTabSelChange(NMHDR* pNMHDR, LRESULT* pResult)
{
	CIceDockingTabbedView::OnTabSelChange(pNMHDR, pResult);

    *pResult = 0;
}

