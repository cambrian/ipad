// InvisibleFrm.cpp : implementation file
//

#include "stdafx.h"
#include "ipad.h"
#include "InvisibleFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInvisibleFrame

IMPLEMENT_DYNCREATE(CInvisibleFrame, CMDIChildWnd)

CInvisibleFrame::CInvisibleFrame()
{
}

CInvisibleFrame::~CInvisibleFrame()
{
}


BEGIN_MESSAGE_MAP(CInvisibleFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CInvisibleFrame)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInvisibleFrame message handlers

void CInvisibleFrame::ActivateFrame(int nCmdShow) 
{
	// TODO: Add your specialized code here and/or call the base class

    // Invisible frame window....
    nCmdShow = SW_HIDE;
	CMDIChildWnd::ActivateFrame(nCmdShow);
}

BOOL CInvisibleFrame::PreCreateWindow(CREATESTRUCT& cs) 
{
    // Invisible frame window....
	return CMDIChildWnd::PreCreateWindow(cs);
}
