// iPad.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "iPad.h"

#include "MainFrm.h"
#include "ChildFrm.h"
#include "SplitFrm.h"
#include "InvisibleFrm.h"
#include "IpFrame.h"
#include "iPadDoc.h"
#include "iPadView.h"
#include "ProjectDoc.h"
#include "ProjectView.h"

#include "../javaPad/javaPadDoc.h"
#include "../javaPad/javaPadView.h"
#include "../cppPad/cppPadDoc.h"
#include "../cppPad/cppPadView.h"
#include "../jsPad/jsPadDoc.h"
#include "../jsPad/jsPadView.h"

#include <dos.h>
#include <direct.h>

#include "TabsPage.h"
#include "FormatPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIPadApp

BEGIN_MESSAGE_MAP(CIPadApp, CWinApp)
	ON_COMMAND(CG_IDS_TIPOFTHEDAY, ShowTipOfTheDay)
	//{{AFX_MSG_MAP(CIPadApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_FILE_NEW_CPP, OnFileNewCpp)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW_CPP, OnUpdateFileNewCpp)
	ON_COMMAND(ID_FILE_NEW_JAVA, OnFileNewJava)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW_JAVA, OnUpdateFileNewJava)
	ON_COMMAND(ID_FILE_OPEN_CPP, OnFileOpenCpp)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN_CPP, OnUpdateFileOpenCpp)
	ON_COMMAND(ID_FILE_OPEN_JAVA, OnFileOpenJava)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN_JAVA, OnUpdateFileOpenJava)
	ON_COMMAND(ID_TOOLS_OPTIONS, OnToolsOptions)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OPTIONS, OnUpdateToolsOptions)
	ON_COMMAND(ID_PROJECT_NEW, OnProjectNew)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_NEW, OnUpdateProjectNew)
	ON_COMMAND(ID_PROJECT_OPEN, OnProjectOpen)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_OPEN, OnUpdateProjectOpen)
	ON_COMMAND(ID_PROJECT_CLOSE, OnProjectClose)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_CLOSE, OnUpdateProjectClose)
	ON_COMMAND(ID_PROJECT_SAVE, OnProjectSave)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_SAVE, OnUpdateProjectSave)
	ON_COMMAND(ID_FILE_NEW_JSCRIPT, OnFileNewJscript)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW_JSCRIPT, OnUpdateFileNewJscript)
	ON_COMMAND(ID_FILE_OPEN_JSCRIPT, OnFileOpenJscript)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN_JSCRIPT, OnUpdateFileOpenJscript)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIPadApp construction

CIPadApp::CIPadApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance

	m_pJavaDocTemplate = NULL;
	m_pCppDocTemplate = NULL;
	m_pJsDocTemplate = NULL;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CIPadApp object

CIPadApp theApp;

// This identifier was generated to be statistically unique for your app.
// You may change it if you prefer to choose a specific identifier.

// {E01C5AC4-AF1E-11D2-8EFE-00A024E0E339}
static const CLSID clsid =
{ 0xe01c5ac4, 0xaf1e, 0x11d2, { 0x8e, 0xfe, 0x0, 0xa0, 0x24, 0xe0, 0xe3, 0x39 } };

/////////////////////////////////////////////////////////////////////////////
// CIPadApp initialization

BOOL CIPadApp::InitInstance()
{
	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	// ????
	AfxInitRichEdit();
	// ????

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(s_profileCompany);

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(
		IDR_IPADTYPE,
		RUNTIME_CLASS(CIPadDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CIPadView));
	pDocTemplate->SetContainerInfo(IDR_IPADTYPE_CNTR_IP);
	pDocTemplate->SetServerInfo(
		IDR_IPADTYPE_SRVR_EMB, IDR_IPADTYPE_SRVR_IP,
		RUNTIME_CLASS(CInPlaceFrame));
	AddDocTemplate(pDocTemplate);


	// Project
	m_pProjectDocTemplate = new CMultiDocTemplate(
		IDR_PROJECTTYPE,
		RUNTIME_CLASS(CProjectDoc),
		RUNTIME_CLASS(CInvisibleFrame), 
		RUNTIME_CLASS(CProjectView));
	AddDocTemplate(m_pProjectDocTemplate);

	// Java
	m_pJavaDocTemplate = new CMultiDocTemplate(
		IDR_JAVATYPE,
		RUNTIME_CLASS(CJavaPadDoc),
		//RUNTIME_CLASS(CSplitFrame), 
		RUNTIME_CLASS(CChildFrame), 
		RUNTIME_CLASS(CJavaPadView));
	AddDocTemplate(m_pJavaDocTemplate);

	// Cpp
	m_pCppDocTemplate = new CMultiDocTemplate(
		IDR_CPPTYPE,
		RUNTIME_CLASS(CCppPadDoc),
		//RUNTIME_CLASS(CSplitFrame), 
		RUNTIME_CLASS(CChildFrame), 
		RUNTIME_CLASS(CCppPadView));
	AddDocTemplate(m_pCppDocTemplate);

	// JavaScript
	m_pJsDocTemplate = new CMultiDocTemplate(
		IDR_JSTYPE,
		RUNTIME_CLASS(CJsPadDoc),
		//RUNTIME_CLASS(CSplitFrame), 
		RUNTIME_CLASS(CChildFrame), 
		RUNTIME_CLASS(CJsPadView));
	AddDocTemplate(m_pJsDocTemplate);


	// Connect the COleTemplateServer to the document template.
	//  The COleTemplateServer creates new documents on behalf
	//  of requesting OLE containers by using information
	//  specified in the document template.
	m_server.ConnectTemplate(clsid, pDocTemplate, FALSE);

	// Register all OLE server factories as running.  This enables the
	//  OLE libraries to create objects from other applications.
	COleTemplateServer::RegisterAll();
		// Note: MDI applications register all server objects without regard
		//  to the /Embedding or /Automation on the command line.

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;

	// Enable drag/drop open
	m_pMainWnd->DragAcceptFiles();

	// Enable DDE Execute open
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// no empty document window on startup
	if(cmdInfo.m_nShellCommand == CCommandLineInfo::FileNew)
	{
		cmdInfo.m_nShellCommand = CCommandLineInfo::FileNothing;
	}

	// Check to see if launched as OLE server
	if (cmdInfo.m_bRunEmbedded || cmdInfo.m_bRunAutomated)
	{
		// Application was run with /Embedding or /Automation.  Don't show the
		//  main window in this case.
		return TRUE;
	}

	// When a server application is launched stand-alone, it is a good idea
	//  to update the system registry in case it has been damaged.
	m_server.UpdateRegistry(OAT_INPLACE_SERVER);
	COleObjectFactory::UpdateRegistryAll();

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// 3D HIGHLIGHT Menu  // ???? Why do I need this ????
	CMenu* pMenu = pMainFrame->GetMenu();
	pMainFrame->iceMenu.RemapMenu(pMenu);
	pMainFrame->SetMenu(pMenu);
	pMainFrame->DrawMenuBar();

	// The main window has been initialized, so show and update it.
	// ??????????????????????????????????????????????????????????
	// pMainFrame->ShowWindow(m_nCmdShow); // ???????????????????
	pMainFrame->ActivateFrame(m_nCmdShow); // ???????????????????
	// ??????????????????????????????????????????????????????????
	pMainFrame->UpdateWindow();

	// CG: This line inserted by 'Tip of the Day' component.
	ShowTipAtStartup();

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual BOOL OnInitDialog();
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CIPadApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CIPadApp commands

int CIPadApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class

	/*
	if(m_pJavaDocTemplate != NULL) {
		delete m_pJavaDocTemplate;
	}
	if(m_pCppDocTemplate != NULL) {
		delete m_pCppDocTemplate;
	}
	if(m_pJsDocTemplate != NULL) {
		delete m_pJsDocTemplate;
	}
	*/
	
	return CWinApp::ExitInstance();
}


void CIPadApp::OnFileNewCpp() 
{
	NewProgramFile("Cpp");
}

void CIPadApp::OnUpdateFileNewCpp(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CIPadApp::OnFileNewJava() 
{
	NewProgramFile("Java");
}

void CIPadApp::OnUpdateFileNewJava(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}


BOOL CIPadApp::NewProgramFile(const CString& strTarget)
{
	CString strDocName;
	CDocTemplate* pSelectedTemplate;
	POSITION pos = GetFirstDocTemplatePosition();
	while (pos != NULL) {
		pSelectedTemplate = (CDocTemplate*) GetNextDocTemplate(pos);
		ASSERT(pSelectedTemplate != NULL);
		ASSERT(pSelectedTemplate->IsKindOf(
			RUNTIME_CLASS(CDocTemplate)));
		pSelectedTemplate->GetDocString(strDocName,
			CDocTemplate::docName);
		if(strDocName == strTarget) {
			pSelectedTemplate->OpenDocumentFile(NULL);
			return TRUE;
		}
	}
	return FALSE;
}


void CIPadApp::Serialize(CArchive& ar) 
{
	if (ar.IsStoring())
	{	// storing code
	}
	else
	{	// loading code
	}
}

BOOL CAboutDlg::OnInitDialog()
{
	CDialog::OnInitDialog();	// CG:  This was added by System Info Component.

	// CG: Following block was added by System Info Component.
	{
		CString strFreeDiskSpace;
		CString strFreeMemory;
		CString strFmt;

		// Fill available memory
		MEMORYSTATUS MemStat;
		MemStat.dwLength = sizeof(MEMORYSTATUS);
		GlobalMemoryStatus(&MemStat);
		strFmt.LoadString(CG_IDS_PHYSICAL_MEM);
		strFreeMemory.Format(strFmt, MemStat.dwTotalPhys / 1024L);

		//TODO: Add a static control to your About Box to receive the memory
		//      information.  Initialize the control with code like this:
		SetDlgItemText(IDC_PHYSICAL_MEM, strFreeMemory);

		// Fill disk free information
		struct _diskfree_t diskfree;
		int nDrive = _getdrive(); // use current default drive
		if (_getdiskfree(nDrive, &diskfree) == 0)
		{
			strFmt.LoadString(CG_IDS_DISK_SPACE);
			strFreeDiskSpace.Format(strFmt,
				(DWORD)diskfree.avail_clusters *
				(DWORD)diskfree.sectors_per_cluster *
				(DWORD)diskfree.bytes_per_sector / (DWORD)1024L,
				nDrive-1 + _T('A'));
		}
		else
			strFreeDiskSpace.LoadString(CG_IDS_DISK_SPACE_UNAVAIL);

		//TODO: Add a static control to your About Box to receive the memory
		//      information.  Initialize the control with code like this:
		SetDlgItemText(IDC_DISK_SPACE, strFreeDiskSpace);
	}

	return TRUE;	// CG:  This was added by System Info Component.

}

void CIPadApp::OnFileOpenCpp() 
{
    //AfxMessageBox(IDS_NOTIMPLEMENTED, MB_OK);	
	// prompt the user 
	CString newName;
	if (!DoPromptFileName(newName, AFX_IDS_OPENFILE,
		OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, TRUE, m_pCppDocTemplate)) {
		return; // open cancelled
	}
	OpenDocumentFile(newName);
}

void CIPadApp::OnUpdateFileOpenCpp(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CIPadApp::OnFileOpenJava() 
{
    //AfxMessageBox(IDS_NOTIMPLEMENTED, MB_OK);	
	// prompt the user 
	CString newName;
	if (!DoPromptFileName(newName, AFX_IDS_OPENFILE,
		OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, TRUE, m_pJavaDocTemplate)) {
		return; // open cancelled
	}
	OpenDocumentFile(newName);
}

void CIPadApp::OnUpdateFileOpenJava(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CIPadApp::OnToolsOptions() 
{
	// TODO: Add your command handler code here
   CPropertySheet optionsSheet("Options");
   CTabsPage   tabsPage;  
   CFormatPage formatPage; 

   // Move member data from the view (or from the currently
   // selected object in the view, for example).
   //pageFirst.m_nMember1 = m_nMember1; 
   //pageFirst.m_nMember2 = m_nMember2;

   optionsSheet.AddPage(&tabsPage);
   optionsSheet.AddPage(&formatPage);

   if (optionsSheet.DoModal() == IDOK)
   {
      //m_nMember1 = pageFirst.m_nMember1;
      //m_nMember2 = pageFirst.m_nMember2;
      //GetDocument()->SetModifiedFlag();
      //GetDocument()->UpdateAllViews(NULL);
   }
	
}

void CIPadApp::OnUpdateToolsOptions(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CIPadApp::ShowTipAtStartup(void)
{
	// CG: This function added by 'Tip of the Day' component.

	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
	if (cmdInfo.m_bShowSplash)
	{
		CTipDlg dlg;
		if (dlg.m_bStartup)
			dlg.DoModal();
	}

}

void CIPadApp::ShowTipOfTheDay(void)
{
	// CG: This function added by 'Tip of the Day' component.

	CTipDlg dlg;
	dlg.DoModal();

}

void CIPadApp::OnProjectNew() 
{
	// TODO: Add your command handler code here

    // temporary
	NewProgramFile("Project");
}

void CIPadApp::OnUpdateProjectNew(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CIPadApp::OnProjectOpen() 
{
	// prompt the user 
	CString newName;
	if (!DoPromptFileName(newName, AFX_IDS_OPENFILE,
		OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, TRUE, m_pProjectDocTemplate)) {
		return; // open cancelled
	}
	OpenDocumentFile(newName);
}

void CIPadApp::OnUpdateProjectOpen(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CIPadApp::OnProjectClose() 
{
    AfxMessageBox(IDS_NOTIMPLEMENTED, MB_OK);	
}

void CIPadApp::OnUpdateProjectClose(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CIPadApp::OnProjectSave() 
{
    AfxMessageBox(IDS_NOTIMPLEMENTED, MB_OK);	
}

void CIPadApp::OnUpdateProjectSave(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CIPadApp::OnFileNewJscript() 
{
	NewProgramFile("JavaScript");
}
void CIPadApp::OnUpdateFileNewJscript(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CIPadApp::OnFileOpenJscript() 
{
	CString newName;
	if (!DoPromptFileName(newName, AFX_IDS_OPENFILE,
		OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, TRUE, m_pJsDocTemplate)) {
		return; // open cancelled
	}
	OpenDocumentFile(newName);
}

void CIPadApp::OnUpdateFileOpenJscript(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}
