// ProjectDoc.cpp : implementation file
//

#include "stdafx.h"
#include "ipad.h"
#include "ProjectDoc.h"
#include "ProjectView.h"
#include "MainFrm.h"
#include "FileView.h"
#include "ClassView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProjectDoc

IMPLEMENT_DYNCREATE(CProjectDoc, CDocument)

CProjectDoc::CProjectDoc()
{
    strBuffer = _T("");
}

BOOL CProjectDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

    // [1] Set default file
    strBuffer.Empty();
    strBuffer = _T("<project name=\"");
    strBuffer += _T("Untitled");   // temporary
    strBuffer += _T("\">\n");
    strBuffer += _T("</project>");

    CMainFrame* pMainFrame = (CMainFrame*) AfxGetApp()->m_pMainWnd;
	CFileView* pFileView = (CFileView*) pMainFrame->m_wndLeftDockBar.GetView(RUNTIME_CLASS(CFileView));
	CClassView* pClassView = (CClassView*) pMainFrame->m_wndLeftDockBar.GetView(RUNTIME_CLASS(CClassView));

    // [2] Initialize fileView
    pFileView->Initialize(strBuffer);

    // [3] Initialize classView
    //pClassView->Initialize(strBuffer);

	return TRUE;
}

CProjectDoc::~CProjectDoc()
{
}


BEGIN_MESSAGE_MAP(CProjectDoc, CDocument)
	//{{AFX_MSG_MAP(CProjectDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProjectDoc diagnostics

#ifdef _DEBUG
void CProjectDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CProjectDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CProjectDoc serialization

void CProjectDoc::Serialize(CArchive& ar)
{
    CMainFrame* pMainFrame = (CMainFrame*) AfxGetApp()->m_pMainWnd;
	CFileView* pFileView = (CFileView*) pMainFrame->m_wndLeftDockBar.GetView(RUNTIME_CLASS(CFileView));
	CClassView* pClassView = (CClassView*) pMainFrame->m_wndLeftDockBar.GetView(RUNTIME_CLASS(CClassView));

	if (ar.IsStoring())
	{	// storing code
	
        // .... update strBuffer from pFileVie wand pClassView
        // ....

		ar.WriteString(strBuffer);
	
    }
	else
	{	// loading code

        // [1] Read in the file
        strBuffer.Empty();       // clean the buffer
		const int nBufMax = 10000;  
		char InBuff[nBufMax];
		BOOL done;
		do 
		{
			int nBuf = ar.Read(InBuff,nBufMax-1);
			done = nBuf < nBufMax-1;
			InBuff[nBuf] = '\0';
		    CString strIn(InBuff);
            strBuffer += strIn;
		} while (!done);

        // [2] Initialize fileView
        pFileView->Initialize(strBuffer);

        // [3] Initialize classView
        //pClassView->Initialize(strBuffer);

    }

    ((CProjectView*) m_viewList.GetHead())->Serialize(ar);

}

/////////////////////////////////////////////////////////////////////////////
// CProjectDoc commands
