// FileView.cpp : implementation file
//

#include "stdafx.h"
#include "iPad.h"
#include "FileView.h"

#include "../expat/xmlparse.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileView

IMPLEMENT_DYNCREATE(CFileView, CTreeView)

CFileView::CFileView()
{
}

CFileView::~CFileView()
{
}


BEGIN_MESSAGE_MAP(CFileView, CTreeView)
	//{{AFX_MSG_MAP(CFileView)
	ON_WM_CREATE()
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileView drawing

void CFileView::OnDraw(CDC* pDC) 
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
	
}

/////////////////////////////////////////////////////////////////////////////
// CFileView diagnostics

#ifdef _DEBUG
void CFileView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CFileView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}
#endif //_DEBUG



/////////////////////////////////////////////////////////////////////////////
// Parser helper functions

void _startElementHandler(void *userData, const char *name, const char **atts)
{
	DWORD*  pAddress = (DWORD*) userData;
	CFileView* pView = (CFileView*) *pAddress;

	// Find element
	CString strElement(name);
    if(strElement == _T("project")) {
	    // Insert attributes
	    for(int i=0;atts[i]!=NULL;i+=2)
	    {
		    CString strAttr(atts[i]);
		    CString strValue(atts[i+1]);
            if(strAttr == _T("name")) {
                pView->AddProject(strValue);
                break;
            }
	    }
    } else if(strElement == _T("file")) {
	    // Insert attributes
	    for(int i=0;atts[i]!=NULL;i+=2)
	    {
		    CString strAttr(atts[i]);
		    CString strValue(atts[i+1]);
            if(strAttr == _T("name")) {
                pView->AddFile(strValue);
                break;
            }
	    }
    }
}

void _endElementHandler(void *userData, const char *name)
{
	DWORD*  pAddress = (DWORD*) userData;
	CFileView* pView = (CFileView*) *pAddress;
}


/////////////////////////////////////////////////////////////////////////////
// CFileView message handlers


int CFileView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CTreeView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CTreeCtrl& treeCtrl = GetTreeCtrl();
	
	// ImageList
	m_ImageList.Create(IDB_FILEVIEW, 16, 1, RGB(255,0,255)); // bitmap background: purple
    
    // Attach ImageList to TreeView
    if (m_ImageList)
        treeCtrl.SetImageList(&m_ImageList,TVSIL_NORMAL);

	return 0;
}

void CFileView::OnInitialUpdate() 
{
	CTreeView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	
}


void CFileView::Initialize(CString strBuffer)
{

    // [2] Create the parser and setup
	XML_Parser parser = ::XML_ParserCreate(NULL);
	DWORD  pFileView = (DWORD) this;
	::XML_SetUserData(parser, &pFileView);
	::XML_SetElementHandler(parser, _startElementHandler, _endElementHandler);
	//::XML_SetCharacterDataHandler(parser, _characterDataHandler);


    // [3] Add Prolog


	// [4] parse document and draw fileView and classView
    int  nBuff = strBuffer.GetLength();
    char* parseBuff = strBuffer.GetBuffer(nBuff);
	if (!::XML_Parse(parser, parseBuff, nBuff, TRUE)) 
	{
		CString  strError;
		strError.Format("%s at line %d",
			::XML_ErrorString(::XML_GetErrorCode(parser)),
			::XML_GetCurrentLineNumber(parser));
		AfxMessageBox(strError);
		return;
	}
    strBuffer.ReleaseBuffer();


	// [5] Add Epilog

    

    // [6] Clean up
	::XML_ParserFree(parser);

}

void CFileView::AddFile(CString strName)
{
    const int maxChar = 64;
	CTreeCtrl& treeCtrl = GetTreeCtrl();
	
	TVITEM tvi;
	TVINSERTSTRUCT tvis;

	tvi.mask = TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE;
	tvi.hItem = NULL;
	tvi.cchTextMax = maxChar;     
	tvi.pszText = (LPTSTR) strName.GetBuffer(maxChar);
	tvi.iImage = 2;
	tvi.iSelectedImage = 3;


	tvis.hParent = m_hTIRoot;
	tvis.hInsertAfter = TVI_LAST;
	tvis.item = tvi;

	treeCtrl.InsertItem(&tvis);
}

void CFileView::AddProject(CString strName)
{
    const int maxChar = 64;
	CTreeCtrl& treeCtrl = GetTreeCtrl();
	
	TVITEM tvi;
	TVINSERTSTRUCT tvis;

	tvi.mask = TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_CHILDREN;
	tvi.hItem = NULL;
	tvi.cchTextMax = maxChar;
    CString strTitle = _T("Project '") + strName + _T("'");
	tvi.pszText = (LPTSTR) strTitle.GetBuffer(maxChar);
	tvi.iImage = 0;
	tvi.iSelectedImage = 1;


	tvis.hParent = TVI_ROOT;
	tvis.hInsertAfter = TVI_LAST;
	tvis.item = tvi;

	m_hTIRoot = treeCtrl.InsertItem(&tvis);
}

void CFileView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CTreeCtrl& treeCtrl = GetTreeCtrl();
	HTREEITEM tiSelected = treeCtrl.HitTest(point, &nFlags);
	if( (nFlags & TVHT_ONITEMRIGHT)  || (nFlags & TVHT_ONITEMINDENT) ||
		(nFlags & TVHT_ONITEM)		|| (nFlags & TVHT_ONITEMBUTTON) ) 
	{
		// ????
	}

	// testing....
    if(!treeCtrl.ItemHasChildren(tiSelected)) {
		//AfxMessageBox(treeCtrl.GetItemText(tiSelected));
        CString strFile = treeCtrl.GetItemText(tiSelected);
	    AfxGetApp()->OpenDocumentFile(strFile);
    }

	CTreeView::OnLButtonDblClk(nFlags, point);
}
