// ClassView.cpp : implementation file
//

#include "stdafx.h"
#include "iPad.h"
#include "ClassView.h"

#include "../expat/xmlparse.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClassView

IMPLEMENT_DYNCREATE(CClassView, CTreeView)

CClassView::CClassView()
{
}

CClassView::~CClassView()
{
}


BEGIN_MESSAGE_MAP(CClassView, CTreeView)
	//{{AFX_MSG_MAP(CClassView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CClassView drawing

void CClassView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CClassView diagnostics

#ifdef _DEBUG
void CClassView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CClassView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CClassView message handlers


int CClassView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CTreeView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CTreeCtrl& treeCtrl = GetTreeCtrl();
	
	// ImageList
	m_ImageList.Create(IDB_CLASSVIEW, 16, 1, RGB(255,0,255)); // bitmap background: purple
    
    // Attach ImageList to TreeView
    if (m_ImageList)
        treeCtrl.SetImageList(&m_ImageList,TVSIL_NORMAL);

	TVITEM tvi, tvi1;
	TVINSERTSTRUCT tvis;
	
	tvi.mask = TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_CHILDREN;
	tvi.hItem = NULL;
	tvi.cchTextMax = 64;     
	tvi.pszText = _T("Classes");
	tvi.iImage = 0;
	tvi.iSelectedImage = 1;

	tvis.hParent = TVI_ROOT;
	tvis.hInsertAfter = TVI_LAST;
	tvis.item = tvi;

	m_hTIRoot = treeCtrl.InsertItem(&tvis);
	
    /////
	tvi1.mask = TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_CHILDREN;
	tvi1.cchTextMax = 64; 
	tvi1.pszText = _T("Class View is under Construction");
	tvi1.iImage = 2;
	tvi1.iSelectedImage = 3;

	tvis.hParent = m_hTIRoot;
	tvis.hInsertAfter = TVI_LAST;
	tvis.item = tvi1;
	treeCtrl.InsertItem(&tvis);
		
	return 0;
}
