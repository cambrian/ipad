# Microsoft Developer Studio Project File - Name="iPad" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=iPad - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "iPad.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "iPad.mak" CFG="iPad - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "iPad - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "iPad - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "iPad - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\icelib ..\htmlhelp\include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D XMLTOKAPI=__declspec(dllimport) /D XMLPARSEAPI=__declspec(dllimport) /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 ..\icelib\Release\ice.lib ..\htmlhelp\lib\htmlhelp.lib ..\expat\xmltok.lib ..\expat\xmlparse.lib /nologo /subsystem:windows /machine:I386 /libpath:"htmlhelp"

!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\icelib ..\htmlhelp\include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D XMLTOKAPI=__declspec(dllimport) /D XMLPARSEAPI=__declspec(dllimport) /Yu"stdafx.h" /FD /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 ..\icelib\Debug\ice.lib ..\htmlhelp\lib\htmlhelp.lib ..\expat\xmltok.lib ..\expat\xmlparse.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"htmlhelp"

!ENDIF 

# Begin Target

# Name "iPad - Win32 Release"
# Name "iPad - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\BottomDockingView.cpp
# End Source File
# Begin Source File

SOURCE=.\BuildView.cpp
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\ClassView.cpp
# End Source File
# Begin Source File

SOURCE=.\CntrItem.cpp
# End Source File
# Begin Source File

SOURCE=..\cppPad\cppPadCntrItem.cpp
# End Source File
# Begin Source File

SOURCE=..\cppPad\cppPadDoc.cpp
# End Source File
# Begin Source File

SOURCE=..\cppPad\cppPadSrvrItem.cpp
# End Source File
# Begin Source File

SOURCE=..\cppPad\cppPadView.cpp
# End Source File
# Begin Source File

SOURCE=.\DebugView.cpp
# End Source File
# Begin Source File

SOURCE=.\DefaultStatusBar.cpp
# End Source File
# Begin Source File

SOURCE=.\FileView.cpp
# End Source File
# Begin Source File

SOURCE=.\FindView.cpp
# End Source File
# Begin Source File

SOURCE=.\FormatPage.cpp
# End Source File
# Begin Source File

SOURCE=.\InfoView.cpp
# End Source File
# Begin Source File

SOURCE=.\InvisibleFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\iPad.cpp
# End Source File
# Begin Source File

SOURCE=.\hlp\iPad.hpj

!IF  "$(CFG)" == "iPad - Win32 Release"

USERDEP__IPAD_="$(ProjDir)\hlp\AfxCore.rtf"	"$(ProjDir)\hlp\AfxPrint.rtf"	
# Begin Custom Build - Making help file...
OutDir=.\Release
ProjDir=.
TargetName=iPad
InputPath=.\hlp\iPad.hpj

"$(OutDir)\$(TargetName).hlp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	call "$(ProjDir)\makehelp.bat"

# End Custom Build

!ELSEIF  "$(CFG)" == "iPad - Win32 Debug"

USERDEP__IPAD_="$(ProjDir)\hlp\AfxCore.rtf"	"$(ProjDir)\hlp\AfxPrint.rtf"	
# Begin Custom Build - Making help file...
OutDir=.\Debug
ProjDir=.
TargetName=iPad
InputPath=.\hlp\iPad.hpj

"$(OutDir)\$(TargetName).hlp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	call "$(ProjDir)\makehelp.bat"

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\iPad.odl
# End Source File
# Begin Source File

SOURCE=.\iPad.rc
# End Source File
# Begin Source File

SOURCE=.\iPadDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\iPadView.cpp
# End Source File
# Begin Source File

SOURCE=.\IpFrame.cpp
# End Source File
# Begin Source File

SOURCE=..\javaPad\javaPadCntrItem.cpp
# End Source File
# Begin Source File

SOURCE=..\javaPad\javaPadDoc.cpp
# End Source File
# Begin Source File

SOURCE=..\javaPad\javaPadSrvrItem.cpp
# End Source File
# Begin Source File

SOURCE=..\javaPad\javaPadView.cpp
# End Source File
# Begin Source File

SOURCE=..\jsPad\jsPadCntrItem.cpp
# End Source File
# Begin Source File

SOURCE=..\jsPad\jsPadDoc.cpp
# End Source File
# Begin Source File

SOURCE=..\jsPad\jsPadSrvrItem.cpp
# End Source File
# Begin Source File

SOURCE=..\jsPad\jsPadView.cpp
# End Source File
# Begin Source File

SOURCE=.\LeftDockingView.cpp
# End Source File
# Begin Source File

SOURCE=.\LogView.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MainToolBar.cpp
# End Source File
# Begin Source File

SOURCE=.\ProjectDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\ProjectView.cpp
# End Source File
# Begin Source File

SOURCE=.\ScriptView.cpp
# End Source File
# Begin Source File

SOURCE=.\SplitFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\SrvrItem.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TabsPage.cpp
# End Source File
# Begin Source File

SOURCE=.\TipDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\BottomDockingView.h
# End Source File
# Begin Source File

SOURCE=.\BuildView.h
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.h
# End Source File
# Begin Source File

SOURCE=.\ClassView.h
# End Source File
# Begin Source File

SOURCE=.\CntrItem.h
# End Source File
# Begin Source File

SOURCE=..\cppPad\cppPadCntrItem.h
# End Source File
# Begin Source File

SOURCE=..\cppPad\cppPadDoc.h
# End Source File
# Begin Source File

SOURCE=..\cppPad\cppPadSrvrItem.h
# End Source File
# Begin Source File

SOURCE=..\cppPad\cppPadView.h
# End Source File
# Begin Source File

SOURCE=.\DebugView.h
# End Source File
# Begin Source File

SOURCE=.\DefaultStatusBar.h
# End Source File
# Begin Source File

SOURCE=.\FileView.h
# End Source File
# Begin Source File

SOURCE=.\FindView.h
# End Source File
# Begin Source File

SOURCE=.\FormatPage.h
# End Source File
# Begin Source File

SOURCE=.\InfoView.h
# End Source File
# Begin Source File

SOURCE=.\InvisibleFrm.h
# End Source File
# Begin Source File

SOURCE=.\iPad.h
# End Source File
# Begin Source File

SOURCE=.\iPadDoc.h
# End Source File
# Begin Source File

SOURCE=.\iPadView.h
# End Source File
# Begin Source File

SOURCE=.\IpFrame.h
# End Source File
# Begin Source File

SOURCE=..\javaPad\javaPadCntrItem.h
# End Source File
# Begin Source File

SOURCE=..\javaPad\javaPadDoc.h
# End Source File
# Begin Source File

SOURCE=..\javaPad\javaPadSrvrItem.h
# End Source File
# Begin Source File

SOURCE=..\javaPad\javaPadView.h
# End Source File
# Begin Source File

SOURCE=..\jsPad\jsPadCntrItem.h
# End Source File
# Begin Source File

SOURCE=..\jsPad\jsPadDoc.h
# End Source File
# Begin Source File

SOURCE=..\jsPad\jsPadSrvrItem.h
# End Source File
# Begin Source File

SOURCE=..\jsPad\jsPadView.h
# End Source File
# Begin Source File

SOURCE=.\LeftDockingView.h
# End Source File
# Begin Source File

SOURCE=.\LogView.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\MainToolBar.h
# End Source File
# Begin Source File

SOURCE=.\ProjectDoc.h
# End Source File
# Begin Source File

SOURCE=.\ProjectView.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\ScriptView.h
# End Source File
# Begin Source File

SOURCE=.\SplitFrm.h
# End Source File
# Begin Source File

SOURCE=.\SrvrItem.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TabsPage.h
# End Source File
# Begin Source File

SOURCE=.\TipDlg.h
# End Source File
# Begin Source File

SOURCE=.\expat\xmlparse.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\botombar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\clasview.bmp
# End Source File
# Begin Source File

SOURCE=.\res\fileview.bmp
# End Source File
# Begin Source File

SOURCE=.\res\idr_cppt.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_java.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_jsty.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_proj.ico
# End Source File
# Begin Source File

SOURCE=.\res\iPad.ico
# End Source File
# Begin Source File

SOURCE=.\res\iPad.rc2
# End Source File
# Begin Source File

SOURCE=.\res\iPadDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\IToolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\leftbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ListItem.bmp
# End Source File
# Begin Source File

SOURCE=.\res\litebulb.bmp
# End Source File
# Begin Source File

SOURCE=..\icelib\res\mainfram.bmp
# End Source File
# Begin Source File

SOURCE=..\icelib\res\MenuChk.bmp
# End Source File
# Begin Source File

SOURCE=..\icelib\res\mrufile.bmp
# End Source File
# Begin Source File

SOURCE=..\icelib\res\reservoi.bmp
# End Source File
# Begin Source File

SOURCE=..\icelib\res\sysbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ThumbTac.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\WrkSpace.bmp
# End Source File
# End Group
# Begin Group "Help Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\hlp\AfxCore.rtf
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\AfxOleCl.rtf
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\AfxOleSv.rtf
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\AfxPrint.rtf
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\AppExit.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\Bullet.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\CurArw2.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\CurArw4.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\CurHelp.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\EditCopy.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\EditCut.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\EditPast.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\EditUndo.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\FileNew.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\FileOpen.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\FilePrnt.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\FileSave.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\HlpSBar.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\HlpTBar.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\iPad.cnt
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\MakeHelp.bat
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\RecFirst.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\RecLast.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\RecNext.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\RecPrev.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\Scmax.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\ScMenu.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\Scmin.bmp
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Source File

SOURCE=.\iPad.reg
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
# Section iPad : {00000000-0000-0000-0000-000000000000}
# 	1:17:CG_IDS_DIDYOUKNOW:119
# 	1:22:CG_IDS_TIPOFTHEDAYMENU:118
# 	1:18:CG_IDS_TIPOFTHEDAY:117
# 	1:22:CG_IDS_TIPOFTHEDAYHELP:122
# 	1:19:CG_IDP_FILE_CORRUPT:121
# 	1:7:IDD_TIP:116
# 	1:13:IDB_LIGHTBULB:115
# 	1:18:CG_IDS_FILE_ABSENT:120
# 	2:17:CG_IDS_DIDYOUKNOW:CG_IDS_DIDYOUKNOW
# 	2:7:CTipDlg:CTipDlg
# 	2:22:CG_IDS_TIPOFTHEDAYMENU:CG_IDS_TIPOFTHEDAYMENU
# 	2:18:CG_IDS_TIPOFTHEDAY:CG_IDS_TIPOFTHEDAY
# 	2:12:CTIP_Written:OK
# 	2:22:CG_IDS_TIPOFTHEDAYHELP:CG_IDS_TIPOFTHEDAYHELP
# 	2:2:BH:
# 	2:19:CG_IDP_FILE_CORRUPT:CG_IDP_FILE_CORRUPT
# 	2:7:IDD_TIP:IDD_TIP
# 	2:8:TipDlg.h:TipDlg.h
# 	2:13:IDB_LIGHTBULB:IDB_LIGHTBULB
# 	2:18:CG_IDS_FILE_ABSENT:CG_IDS_FILE_ABSENT
# 	2:10:TipDlg.cpp:TipDlg.cpp
# End Section
# Section iPad : {00002227-0012-2000-0700-070004000000}
# 	1:17:CG_IDS_DISK_SPACE:105
# 	1:19:CG_IDS_PHYSICAL_MEM:103
# 	1:25:CG_IDS_DISK_SPACE_UNAVAIL:106
# 	2:14:PhysicalMemory:CG_IDS_PHYSICAL_MEM
# 	2:9:DiskSpace:CG_IDS_DISK_SPACE
# 	2:16:SpaceUnavailable:CG_IDS_DISK_SPACE_UNAVAIL
# 	2:7:NewFunc:1
# 	2:10:SysInfoKey:1234
# End Section
