// cppPad.h : main header file for the CPPPAD application
//

#if !defined(AFX_CPPPAD_H__FEF2B179_36FD_11D3_93E1_00207817FF60__INCLUDED_)
#define AFX_CPPPAD_H__FEF2B179_36FD_11D3_93E1_00207817FF60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

////// strings for registry entry
static const TCHAR s_profileCompany[] = _T("BlueCraft");

/////////////////////////////////////////////////////////////////////////////
// CCppPadApp:
// See cppPad.cpp for the implementation of this class
//

class CCppPadApp : public CWinApp
{
public:
	CCppPadApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCppPadApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	COleTemplateServer m_server;
		// Server object for document creation
	//{{AFX_MSG(CCppPadApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CPPPAD_H__FEF2B179_36FD_11D3_93E1_00207817FF60__INCLUDED_)
