// cppPadView.cpp : implementation of the CCppPadView class
//

#include "stdafx.h"
#include "cppPad.h"

#include "cppPadDoc.h"
#include "cppPadCntrItem.h"
#include "cppPadView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCppPadView

IMPLEMENT_DYNCREATE(CCppPadView, CIceEditView)

BEGIN_MESSAGE_MAP(CCppPadView, CIceEditView)
	//{{AFX_MSG_MAP(CCppPadView)
	ON_WM_DESTROY()
	ON_COMMAND(ID_CANCEL_EDIT_SRVR, OnCancelEditSrvr)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CIceEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CIceEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CIceEditView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCppPadView construction/destruction

CCppPadView::CCppPadView()
{
	EnableAutomation();

    m_nWordWrap = WrapNone;

    m_chComment = 1;
	m_bCaseSensitive = TRUE;
	m_bChangeCase = FALSE;

	SetStringQuotes(_T("\""));

	SetTextColor(RGB(0,0,0), FALSE);
	SetKeywordColor(RGB(0,0,255), FALSE);
	SetConstantColor(RGB(0,0,0), TRUE);
	SetCommentColor(RGB(0,128,0), FALSE);
	SetNumberColor(RGB(255,0,255), FALSE);
	SetStringColor(RGB(255,0,0), FALSE);

	m_bInForcedChange = FALSE;
	m_changeType = ctUndo;
	m_crOldSel.cpMin = m_crOldSel.cpMax = 0;
}

CCppPadView::~CCppPadView()
{
}

BOOL CCppPadView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CIceEditView::PreCreateWindow(cs);
}

void CCppPadView::OnInitialUpdate()
{
    //GetRichEditCtrl().SetOptions(ECOOP_OR, ECO_AUTOHSCROLL);

	CIceEditView::OnInitialUpdate();


	// Set the printing margins (720 twips = 1/2 inch).
	SetMargins(CRect(720, 720, 720, 720));
}

/////////////////////////////////////////////////////////////////////////////
// CCppPadView printing

BOOL CCppPadView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}


void CCppPadView::OnDestroy()
{
	// Deactivate the item on destruction; this is important
	// when a splitter view is being used.
   CIceEditView::OnDestroy();
   COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
   if (pActiveItem != NULL && pActiveItem->GetActiveView() == this)
   {
      pActiveItem->Deactivate();
      ASSERT(GetDocument()->GetInPlaceActiveItem(this) == NULL);
   }
}


/////////////////////////////////////////////////////////////////////////////
// OLE Server support

// The following command handler provides the standard keyboard
//  user interface to cancel an in-place editing session.  Here,
//  the server (not the container) causes the deactivation.
void CCppPadView::OnCancelEditSrvr()
{
	GetDocument()->OnDeactivateUI(FALSE);
}

/////////////////////////////////////////////////////////////////////////////
// CCppPadView diagnostics

#ifdef _DEBUG
void CCppPadView::AssertValid() const
{
	CIceEditView::AssertValid();
}

void CCppPadView::Dump(CDumpContext& dc) const
{
	CIceEditView::Dump(dc);
}

CCppPadDoc* CCppPadView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCppPadDoc)));
	return (CCppPadDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCppPadView message handlers

void CCppPadView::Initialize() 
{
	PARAFORMAT pf;
	pf.cbSize = sizeof(PARAFORMAT);
	pf.dwMask = PFM_TABSTOPS ;
	pf.cTabCount = MAX_TAB_STOPS;
	for( int itab = 0 ; itab < pf.cTabCount  ; itab++ )
		pf.rgxTabs[itab] = (itab + 1) * 1440/5 ;

	SetParaFormat( pf );

	CHARFORMAT cfDefault;
	cfDefault.cbSize = sizeof(cfDefault);
	cfDefault.dwEffects = CFE_PROTECTED; 
	cfDefault.dwMask = CFM_BOLD | CFM_FACE | CFM_SIZE | CFM_CHARSET | CFM_PROTECTED;
	cfDefault.yHeight = 200;
	cfDefault.bCharSet = 0xEE; 
	strcpy(cfDefault.szFaceName, _T("Courier New")); 

	GetRichEditCtrl().SetDefaultCharFormat(cfDefault);
	GetRichEditCtrl().SetEventMask(ENM_CHANGE | ENM_SELCHANGE | ENM_PROTECTED);
}

static LPCTSTR szKeywords = " "
"and and_eq asm auto bitand bitor "
"bool break case catch char class "
"compl const const_cast continue defulat delete "
"do double dynamic_cast else enum explicit "
"export extern false float for friend "
"goto if inline int long mutable "
"namespace new not not_eq operator or "
"or_eq private protected public register reinterpret_cast "
"return short signed sizeof static static_cast "
"struct switch template this throw true "
"try typedef typeid typename union unsigned "
"using virtual void volatile wchar_t while "
"xor xor_eq ";
 
static LPCTSTR szConstants = " main "
"include define ifdef undef endif line error pragma ";


int CCppPadView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceEditView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
    Initialize();

    SetCaseSensitive(TRUE);
	SetStringQuotes(_T("\""));
	SetSLComment(_T('/'),_T('/'));
	SetMLOpenComment(_T('/'),_T('*'));
	SetMLCloseComment(_T('*'),_T('/'));

    ClearKeywords();
	AddKeywords(szKeywords);
    ClearConstants();
	AddConstants(szConstants);
	
	return 0;
}

void CCppPadView::OnFinalRelease() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CIceEditView::OnFinalRelease();
}
