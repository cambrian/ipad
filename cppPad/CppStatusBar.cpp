// CppStatusBar.cpp : implementation file
//

#include "stdafx.h"
#include "cppPad.h"
#include "CppStatusBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCppStatusBar

CCppStatusBar::CCppStatusBar()
{
}

CCppStatusBar::~CCppStatusBar()
{
}


BEGIN_MESSAGE_MAP(CCppStatusBar, CIceStatusBar)
	//{{AFX_MSG_MAP(CCppStatusBar)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCppStatusBar message handlers
