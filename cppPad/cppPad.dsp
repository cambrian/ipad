# Microsoft Developer Studio Project File - Name="cppPad" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=cppPad - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "cppPad.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "cppPad.mak" CFG="cppPad - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "cppPad - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "cppPad - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "cppPad - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\icelib ..\htmlhelp\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 ..\icelib\Release\ice.lib ..\htmlhelp\lib\htmlhelp.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "cppPad - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\icelib ..\htmlhelp\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 ..\icelib\Debug\ice.lib ..\htmlhelp\lib\htmlhelp.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "cppPad - Win32 Release"
# Name "cppPad - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\cppPad.cpp
# End Source File
# Begin Source File

SOURCE=.\hlp\cppPad.hpj

!IF  "$(CFG)" == "cppPad - Win32 Release"

# PROP Ignore_Default_Tool 1
USERDEP__CPPPA="hlp\AfxCore.rtf"	"hlp\AfxPrint.rtf"	"hlp\$(TargetName).hm"	
# Begin Custom Build - Making help file...
OutDir=.\Release
TargetName=cppPad
InputPath=.\hlp\cppPad.hpj
InputName=cppPad

"$(OutDir)\$(InputName).hlp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	start /wait hcw /C /E /M "hlp\$(InputName).hpj" 
	if errorlevel 1 goto :Error 
	if not exist "hlp\$(InputName).hlp" goto :Error 
	copy "hlp\$(InputName).hlp" $(OutDir) 
	goto :done 
	:Error 
	echo hlp\$(InputName).hpj(1) : error: 
	type "hlp\$(InputName).log" 
	:done 
	
# End Custom Build

!ELSEIF  "$(CFG)" == "cppPad - Win32 Debug"

# PROP Ignore_Default_Tool 1
USERDEP__CPPPA="hlp\AfxCore.rtf"	"hlp\AfxPrint.rtf"	"hlp\$(TargetName).hm"	
# Begin Custom Build - Making help file...
OutDir=.\Debug
TargetName=cppPad
InputPath=.\hlp\cppPad.hpj
InputName=cppPad

"$(OutDir)\$(InputName).hlp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	start /wait hcw /C /E /M "hlp\$(InputName).hpj" 
	if errorlevel 1 goto :Error 
	if not exist "hlp\$(InputName).hlp" goto :Error 
	copy "hlp\$(InputName).hlp" $(OutDir) 
	goto :done 
	:Error 
	echo hlp\$(InputName).hpj(1) : error: 
	type "hlp\$(InputName).log" 
	:done 
	
# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\cppPad.odl
# End Source File
# Begin Source File

SOURCE=.\cppPad.rc
# End Source File
# Begin Source File

SOURCE=.\cppPadCntrItem.cpp
# End Source File
# Begin Source File

SOURCE=.\cppPadDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\cppPadSrvrItem.cpp
# End Source File
# Begin Source File

SOURCE=.\cppPadView.cpp
# End Source File
# Begin Source File

SOURCE=.\CppStatusBar.cpp
# End Source File
# Begin Source File

SOURCE=.\IpFrame.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\cppPad.h
# End Source File
# Begin Source File

SOURCE=.\cppPadCntrItem.h
# End Source File
# Begin Source File

SOURCE=.\cppPadDoc.h
# End Source File
# Begin Source File

SOURCE=.\cppPadSrvrItem.h
# End Source File
# Begin Source File

SOURCE=.\cppPadView.h
# End Source File
# Begin Source File

SOURCE=.\CppStatusBar.h
# End Source File
# Begin Source File

SOURCE=.\IpFrame.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h

!IF  "$(CFG)" == "cppPad - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Making help include file...
TargetName=cppPad
InputPath=.\Resource.h

"hlp\$(TargetName).hm" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	echo. >"hlp\$(TargetName).hm" 
	echo // Commands (ID_* and IDM_*) >>"hlp\$(TargetName).hm" 
	makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Prompts (IDP_*) >>"hlp\$(TargetName).hm" 
	makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Resources (IDR_*) >>"hlp\$(TargetName).hm" 
	makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Dialogs (IDD_*) >>"hlp\$(TargetName).hm" 
	makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Frame Controls (IDW_*) >>"hlp\$(TargetName).hm" 
	makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\$(TargetName).hm" 
	
# End Custom Build

!ELSEIF  "$(CFG)" == "cppPad - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Making help include file...
TargetName=cppPad
InputPath=.\Resource.h

"hlp\$(TargetName).hm" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	echo. >"hlp\$(TargetName).hm" 
	echo // Commands (ID_* and IDM_*) >>"hlp\$(TargetName).hm" 
	makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Prompts (IDP_*) >>"hlp\$(TargetName).hm" 
	makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Resources (IDR_*) >>"hlp\$(TargetName).hm" 
	makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Dialogs (IDD_*) >>"hlp\$(TargetName).hm" 
	makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\$(TargetName).hm" 
	echo. >>"hlp\$(TargetName).hm" 
	echo // Frame Controls (IDW_*) >>"hlp\$(TargetName).hm" 
	makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\$(TargetName).hm" 
	
# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\cppPad.ico
# End Source File
# Begin Source File

SOURCE=.\res\cppPad.rc2
# End Source File
# Begin Source File

SOURCE=.\res\cppPadDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\IToolbar.bmp
# End Source File
# Begin Source File

SOURCE=..\icelib\res\mrufile.bmp
# End Source File
# Begin Source File

SOURCE=..\icelib\res\sdiframe.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Group "Help Files"

# PROP Default_Filter "cnt;rtf"
# Begin Source File

SOURCE=.\hlp\AfxCore.rtf
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\AfxOleCl.rtf
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\AfxOleSv.rtf
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\AfxPrint.rtf
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\AppExit.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\Bullet.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\cppPad.cnt
# PROP Exclude_From_Build 1
# PROP Ignore_Default_Tool 1
# End Source File
# Begin Source File

SOURCE=.\hlp\CurArw2.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\CurArw4.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\CurHelp.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\EditCopy.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\EditCut.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\EditPast.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\EditUndo.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\FileNew.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\FileOpen.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\FilePrnt.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\FileSave.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\HlpSBar.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\HlpTBar.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\RecFirst.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\RecLast.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\RecNext.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\RecPrev.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\Scmax.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\ScMenu.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\Scmin.bmp
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Source File

SOURCE=.\cppPad.reg
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
