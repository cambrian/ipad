@echo off
REM -- First make map file from Microsoft Visual C++ generated resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by CPPPAD.HPJ. >"hlp\cppPad.hm"
echo. >>"hlp\cppPad.hm"
echo // Commands (ID_* and IDM_*) >>"hlp\cppPad.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\cppPad.hm"
echo. >>"hlp\cppPad.hm"
echo // Prompts (IDP_*) >>"hlp\cppPad.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\cppPad.hm"
echo. >>"hlp\cppPad.hm"
echo // Resources (IDR_*) >>"hlp\cppPad.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\cppPad.hm"
echo. >>"hlp\cppPad.hm"
echo // Dialogs (IDD_*) >>"hlp\cppPad.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\cppPad.hm"
echo. >>"hlp\cppPad.hm"
echo // Frame Controls (IDW_*) >>"hlp\cppPad.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\cppPad.hm"
REM -- Make help for Project CPPPAD


echo Building Win32 Help files
start /wait hcw /C /E /M "hlp\cppPad.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\cppPad.hlp" goto :Error
if not exist "hlp\cppPad.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\cppPad.hlp" Debug
if exist Debug\nul copy "hlp\cppPad.cnt" Debug
if exist Release\nul copy "hlp\cppPad.hlp" Release
if exist Release\nul copy "hlp\cppPad.cnt" Release
echo.
goto :done

:Error
echo hlp\cppPad.hpj(1) : error: Problem encountered creating help file

:done
echo.
