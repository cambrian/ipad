// CntrItem.h : interface of the CCppPadCntrItem class
//

#if !defined(AFX_CNTRITEM_H__FEF2B189_36FD_11D3_93E1_00207817FF60__INCLUDED_)
#define AFX_CNTRITEM_H__FEF2B189_36FD_11D3_93E1_00207817FF60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCppPadDoc;
class CCppPadView;

class CCppPadCntrItem : public CRichEditCntrItem
{
	DECLARE_SERIAL(CCppPadCntrItem)

// Constructors
public:
	CCppPadCntrItem(REOBJECT* preo = NULL, CCppPadDoc* pContainer = NULL);
		// Note: pContainer is allowed to be NULL to enable IMPLEMENT_SERIALIZE.
		//  IMPLEMENT_SERIALIZE requires the class have a constructor with
		//  zero arguments.  Normally, OLE items are constructed with a
		//  non-NULL document pointer.

// Attributes
public:
	CCppPadDoc* GetDocument()
		{ return (CCppPadDoc*)CRichEditCntrItem::GetDocument(); }
	CCppPadView* GetActiveView()
		{ return (CCppPadView*)CRichEditCntrItem::GetActiveView(); }

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCppPadCntrItem)
	public:
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	~CCppPadCntrItem();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CNTRITEM_H__FEF2B189_36FD_11D3_93E1_00207817FF60__INCLUDED_)
