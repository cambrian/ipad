// cppPadCntrItem.cpp : implementation of the CCppPadCntrItem class
//

#include "stdafx.h"
#include "cppPad.h"

#include "cppPadDoc.h"
#include "cppPadView.h"
#include "cppPadCntrItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCppPadCntrItem implementation

IMPLEMENT_SERIAL(CCppPadCntrItem, CRichEditCntrItem, 0)

CCppPadCntrItem::CCppPadCntrItem(REOBJECT* preo, CCppPadDoc* pContainer)
	: CRichEditCntrItem(preo, pContainer)
{
	// TODO: add one-time construction code here
	
}

CCppPadCntrItem::~CCppPadCntrItem()
{
	// TODO: add cleanup code here
	
}

/////////////////////////////////////////////////////////////////////////////
// CCppPadCntrItem diagnostics

#ifdef _DEBUG
void CCppPadCntrItem::AssertValid() const
{
	CRichEditCntrItem::AssertValid();
}

void CCppPadCntrItem::Dump(CDumpContext& dc) const
{
	CRichEditCntrItem::Dump(dc);
}
#endif

/////////////////////////////////////////////////////////////////////////////
