// SrvrItem.h : interface of the CCppPadSrvrItem class
//

#if !defined(AFX_SRVRITEM_H__FEF2B185_36FD_11D3_93E1_00207817FF60__INCLUDED_)
#define AFX_SRVRITEM_H__FEF2B185_36FD_11D3_93E1_00207817FF60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCppPadSrvrItem : public COleServerItem
{
	DECLARE_DYNAMIC(CCppPadSrvrItem)

// Constructors
public:
	CCppPadSrvrItem(CCppPadDoc* pContainerDoc);

// Attributes
	CCppPadDoc* GetDocument() const
		{ return (CCppPadDoc*)COleServerItem::GetDocument(); }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCppPadSrvrItem)
	public:
	virtual BOOL OnDraw(CDC* pDC, CSize& rSize);
	virtual BOOL OnGetExtent(DVASPECT dwDrawAspect, CSize& rSize);
	//}}AFX_VIRTUAL

// Implementation
public:
	~CCppPadSrvrItem();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SRVRITEM_H__FEF2B185_36FD_11D3_93E1_00207817FF60__INCLUDED_)
