// IceEditDoc.cpp : implementation of the CIceEditDoc class
//

#include "stdafx.h"

#include "IceEditDoc.h"
#include "IceEditView.h"
#include "IceEditCntrItem.h"
#include "IceEditSrvrItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceEditDoc

IMPLEMENT_DYNCREATE(CIceEditDoc, CRichEditDoc)

BEGIN_MESSAGE_MAP(CIceEditDoc, CRichEditDoc)
	//{{AFX_MSG_MAP(CIceEditDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Enable default OLE container implementation
	ON_UPDATE_COMMAND_UI(ID_OLE_EDIT_LINKS, CRichEditDoc::OnUpdateEditLinksMenu)
	ON_COMMAND(ID_OLE_EDIT_LINKS, CRichEditDoc::OnEditLinks)
	ON_UPDATE_COMMAND_UI(ID_OLE_VERB_FIRST, CRichEditDoc::OnUpdateObjectVerbMenu)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceEditDoc construction/destruction

CIceEditDoc::CIceEditDoc()
{
	// Use OLE compound files
	EnableCompoundFile();

	// TODO: add one-time construction code here

}

CIceEditDoc::~CIceEditDoc()
{
}

BOOL CIceEditDoc::OnNewDocument()
{
	if (!CRichEditDoc::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	((CIceEditView*)m_viewList.GetHead())->SetWindowText(NULL);

	return TRUE;
}

CRichEditCntrItem* CIceEditDoc::CreateClientItem(REOBJECT* preo) const
{
	// cast away constness of this
	return new CIceEditCntrItem(preo, (CIceEditDoc*) this);
}

/////////////////////////////////////////////////////////////////////////////
// CIceEditDoc server implementation

COleServerItem* CIceEditDoc::OnGetEmbeddedItem()
{
	// OnGetEmbeddedItem is called by the framework to get the COleServerItem
	//  that is associated with the document.  It is only called when necessary.

	CIceEditSrvrItem* pItem = new CIceEditSrvrItem(this);
	ASSERT_VALID(pItem);
	return pItem;
}



/////////////////////////////////////////////////////////////////////////////
// CIceEditDoc serialization

void CIceEditDoc::Serialize(CArchive& ar)
{
	CIceEditView* pView = (CIceEditView*) m_viewList.GetHead();
	ASSERT_VALID(pView);
	ASSERT_KINDOF(CIceEditView, pView);

	CRichEditCtrl& wndRich = pView->GetRichEditCtrl();
	CString str;

	//Write/Read raw data from file to/from RichEditControl
	if (ar.IsStoring())	{
		// TODO: add storing code here

		wndRich.GetWindowText(str);
		//AfxMessageBox(str);

		ar.Write((LPVOID)(LPCTSTR)str, str.GetLength()*sizeof(TCHAR));
		//ar.WriteString(str);

	} else {
		// TODO: add loading code here

		CFile* pFile = ar.GetFile();
		ASSERT(pFile->GetPosition() == 0);
		DWORD nFileSize = pFile->GetLength();
		ar.Read((LPVOID)str.GetBuffer(nFileSize), nFileSize/sizeof(TCHAR));
		wndRich.SetWindowText(str);

	}

	ASSERT_VALID(this);

	// Calling the base class CRichEditDoc enables serialization
	//  of the container document's COleClientItem objects.
	//CRichEditDoc::Serialize(ar);
}

/////////////////////////////////////////////////////////////////////////////
// CIceEditDoc diagnostics

#ifdef _DEBUG
void CIceEditDoc::AssertValid() const
{
	CRichEditDoc::AssertValid();
}

void CIceEditDoc::Dump(CDumpContext& dc) const
{
	CRichEditDoc::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIceEditDoc commands

BOOL CIceEditDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	// TODO: Add your specialized code here and/or call the base class

	CFileException fe;
	CFile* pFile = NULL;
	pFile = GetFile(lpszPathName, CFile::modeCreate |
		CFile::modeReadWrite | CFile::shareExclusive, &fe);
		//CFile::modeReadWrite | CFile::shareDenyNone, &fe);

	if (pFile == NULL)
	{
		ReportSaveLoadException(lpszPathName, &fe,
			TRUE, AFX_IDP_INVALID_FILENAME);
		return FALSE;
	}

	CArchive saveArchive(pFile, CArchive::store | CArchive::bNoFlushOnDelete);
	saveArchive.m_pDocument = this;
	saveArchive.m_bForceFlat = FALSE;

	//
	TRY
	{
		CWaitCursor wait;
		Serialize(saveArchive);     // save me
		saveArchive.Close();
		ReleaseFile(pFile, FALSE);
	}
	CATCH_ALL(e)
	{
		ReleaseFile(pFile, TRUE);

		TRY
		{
			ReportSaveLoadException(lpszPathName, e,
				TRUE, AFX_IDP_FAILED_TO_SAVE_DOC);
		}
		END_TRY
		//DELETE_EXCEPTION(e);
		//????????????????????
		return FALSE;
	}
	END_CATCH_ALL
	//

	//return CRichEditDoc::OnSaveDocument(lpszPathName);

	SetModifiedFlag(FALSE);
	return TRUE;        // success
}

//????????????????????????????????
BOOL CIceEditDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	//if (!CRichEditDoc::OnOpenDocument(lpszPathName))
	//	return FALSE;
	
	// TODO: Add your specialized creation code here
	
	CFileException fe;
	CFile* pFile = NULL;
	pFile = GetFile(lpszPathName, 
		CFile::modeReadWrite | CFile::shareExclusive, &fe);
		//CFile::modeReadWrite | CFile::shareDenyNone, &fe);

	if (pFile == NULL)
	{
		ReportSaveLoadException(lpszPathName, &fe,
			TRUE, AFX_IDP_INVALID_FILENAME);
		return FALSE;
	}

	CArchive openArchive(pFile, CArchive::load | CArchive::bNoFlushOnDelete);
	openArchive.m_pDocument = this;
	openArchive.m_bForceFlat = FALSE;

	//
	TRY
	{
		CWaitCursor wait;
		Serialize(openArchive);     // open me
		openArchive.Close();
		ReleaseFile(pFile, FALSE);
	}
	CATCH_ALL(e)
	{
		ReleaseFile(pFile, TRUE);

		TRY
		{
			ReportSaveLoadException(lpszPathName, e,
				TRUE, AFX_IDP_FAILED_TO_SAVE_DOC);
		}
		END_TRY
		//DELETE_EXCEPTION(e);
		//????????????????????
		return FALSE;
	}
	END_CATCH_ALL
	//

	SetModifiedFlag(FALSE);
	return TRUE;        // success
}
