// IceEditCntrItem.cpp : implementation of the CIceEditCntrItem class
//

#include "stdafx.h"

#include "IceEditDoc.h"
#include "IceEditView.h"
#include "IceEditCntrItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceEditCntrItem implementation

IMPLEMENT_SERIAL(CIceEditCntrItem, CRichEditCntrItem, 0)

CIceEditCntrItem::CIceEditCntrItem(REOBJECT* preo, CIceEditDoc* pContainer)
	: CRichEditCntrItem(preo, pContainer)
{
	// TODO: add one-time construction code here
	
}

CIceEditCntrItem::~CIceEditCntrItem()
{
	// TODO: add cleanup code here
	
}

/////////////////////////////////////////////////////////////////////////////
// CIceEditCntrItem diagnostics

#ifdef _DEBUG
void CIceEditCntrItem::AssertValid() const
{
	CRichEditCntrItem::AssertValid();
}

void CIceEditCntrItem::Dump(CDumpContext& dc) const
{
	CRichEditCntrItem::Dump(dc);
}
#endif

/////////////////////////////////////////////////////////////////////////////
