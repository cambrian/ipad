// IceMDIFrameWnd.cpp : implementation file
//

#include "stdafx.h"
#include "IceMDIFrameWnd.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

UINT __SetSafeTimer(CWnd * pWnd, const UINT uiElapse)
{
	ASSERT(pWnd && pWnd->GetSafeHwnd());
	UINT iTimer = 1;
	while (iTimer < 10000)
	{
		UINT res = pWnd->SetTimer(1, uiElapse, NULL);
		if (res) return res;
		iTimer ++;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CIceMDIFrameWnd

IMPLEMENT_DYNCREATE(CIceMDIFrameWnd, CMDIFrameWnd)

CIceMDIFrameWnd::CIceMDIFrameWnd()
{
	oldMenuBarRc.SetRect(0,0,0,0);
	bMenuLooping = false;
	uiTimerId = 0;

	iceMenu.LoadToolBarResource(IDR_MAINFRAME);
	iceMenu.AddToolBarResource(IDR_SYSTEMBAR);
	iceMenu.AddToolBarResource(IDR_RESERVOIR);
	iceMenu.AddToolBarResource(IDR_MRUFILE);
}

CIceMDIFrameWnd::~CIceMDIFrameWnd()
{
}


BEGIN_MESSAGE_MAP(CIceMDIFrameWnd, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CIceMDIFrameWnd)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_NCHITTEST()
	ON_WM_TIMER()
	ON_WM_INITMENU()
	ON_WM_INITMENUPOPUP()
	ON_WM_MENUCHAR()
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
	ON_WM_NCPAINT()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
	ON_WM_ENTERMENULOOP()
	ON_WM_EXITMENULOOP()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceMDIFrameWnd message handlers

BOOL CIceMDIFrameWnd::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CMDIFrameWnd::PreCreateWindow(cs);
}

int CIceMDIFrameWnd::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	uiTimerId = __SetSafeTimer(this, 100);
	
	return 0;
}

void CIceMDIFrameWnd::OnDestroy() 
{
	CMDIFrameWnd::OnDestroy();
	
	// TODO: Add your message handler code here

	if (uiTimerId) KillTimer(uiTimerId);
	//CMDIFrameWnd::OnDestroy();
	
}


void CIceMDIFrameWnd::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if (!iceMenu.DrawItem(lpDrawItemStruct))
		CMDIFrameWnd::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CIceMDIFrameWnd::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	if (!iceMenu.MeasureItem(lpMeasureItemStruct))
		CMDIFrameWnd::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CIceMDIFrameWnd::OnInitMenu(CMenu* pMenu) 
{
	iceMenu.RemapMenu(pMenu);	
	CMDIFrameWnd::OnInitMenu(pMenu);
}

void CIceMDIFrameWnd::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu) 
{
	iceMenu.RemapMenu(pPopupMenu);
	CMDIFrameWnd::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
}

LRESULT CIceMDIFrameWnd::OnMenuChar(UINT nChar, UINT nFlags, CMenu* pMenu) 
{
	LRESULT lRes = CMDIFrameWnd::OnMenuChar(nChar, nFlags, pMenu);
	if (nFlags != MF_SYSMENU)
	{
		if (HIWORD(lRes) != 2) iceMenu.FindKeyboardShortcut(nChar, nFlags, pMenu, lRes);
	}
	return lRes;
}

UINT CIceMDIFrameWnd::OnNcHitTest(CPoint point) 
{
	UINT res = CMDIFrameWnd::OnNcHitTest(point);
	if (!bMenuLooping && res == HTMENU)
	{
		CMenu * pMenu = GetMenu();
		ASSERT(pMenu);

		UINT mifp = MenuItemFromPoint(GetSafeHwnd(), pMenu->GetSafeHmenu(), point);
		if (mifp != (UINT) -1)
		{
			CRect rc, wrc;
			if (GetMenuItemRect(GetSafeHwnd(), pMenu->GetSafeHmenu(), mifp, &rc))
			{
				if (iceMenu.IsIceMenu(pMenu, mifp, true))
				{
					GetWindowRect(wrc);
					rc.top -= wrc.top;
					rc.bottom -= wrc.top;
					rc.left -= wrc.left;
					rc.right -= wrc.left;
					if (oldMenuBarRc != rc || oldMenuBarRc.IsRectEmpty())
					{
						CWindowDC dc(this);
						if (!(oldMenuBarRc.IsRectEmpty()))
						{
							dc.Draw3dRect(oldMenuBarRc,iceMenu.crMenu, iceMenu.crMenu);
						}
						dc.Draw3dRect(rc,iceMenu.cr3dHilight, iceMenu.cr3dShadow);
						oldMenuBarRc = rc;
					}
				}
				else
				{
					if (!(oldMenuBarRc.IsRectEmpty()))
					{
						CWindowDC dc(this);
						dc.Draw3dRect(oldMenuBarRc,iceMenu.crMenu, iceMenu.crMenu);
						oldMenuBarRc.SetRect(0,0,0,0);
					}
				}
			}
		}
	}
	else
	{
		if (!(oldMenuBarRc.IsRectEmpty()))
		{
			CWindowDC dc(this);
			dc.Draw3dRect(oldMenuBarRc,iceMenu.crMenu, iceMenu.crMenu);
			oldMenuBarRc.SetRect(0,0,0,0);
		}
	}
	
	return res;
}

void CIceMDIFrameWnd::OnTimer(UINT nIDEvent) 
{
	if (uiTimerId == nIDEvent && !bMenuLooping)
	{
		CPoint pt;
		GetCursorPos(&pt);
		LRESULT res = SendMessage(WM_NCHITTEST, 0, MAKELONG(pt.x, pt.y));
	}
	CMDIFrameWnd::OnTimer(nIDEvent);
}

void CIceMDIFrameWnd::OnEnterMenuLoop(BOOL bIsTrackPopupMenu)
{
	if (!(oldMenuBarRc.IsRectEmpty()))
	{
		CWindowDC dc(this);
		dc.Draw3dRect(oldMenuBarRc,iceMenu.crMenu, iceMenu.crMenu);
		oldMenuBarRc.SetRect(0,0,0,0);
	}

	bMenuLooping = true;
}

void CIceMDIFrameWnd::OnExitMenuLoop(BOOL bIsTrackPopupMenu)
{
	bMenuLooping = false;
}



void CIceMDIFrameWnd::OnNcPaint() 
{
	// TODO: Add your message handler code here
	
	// where do I get the source of CMDIFrameWnd::OnNcPaint()????

	// Do not call CMDIFrameWnd::OnNcPaint() for painting messages
	CMDIFrameWnd::OnNcPaint();
}

void CIceMDIFrameWnd::RecalcLayout(BOOL bNotify) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	// HACK: Left docking bar doesn't resize when bottom docking bar is resized!
	CMDIFrameWnd::RecalcLayout(bNotify);
	CMDIFrameWnd::RecalcLayout(bNotify);
	// HACK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
}

// Hack: ???????????????????????????????????????????????????
// In the main application's InitInstance()
// Call mainframe's ActivateFrame(nCmdShow) instead of ShowWindow(m_nCmdShow)
// Hack !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
void CIceMDIFrameWnd::ActivateFrame(int nCmdShow) 
{
	// TODO: Add your specialized code here and/or call the base class
	
    // Default values: used only for the first time
    int h = int(::GetSystemMetrics(SM_CYSCREEN) * 0.8); 
    int w = int(::GetSystemMetrics(SM_CXSCREEN) * 0.8); 
    int y = (::GetSystemMetrics(SM_CYSCREEN) - h) / 2; 
    int x = (::GetSystemMetrics(SM_CXSCREEN) - w) / 2;
    //

    BOOL bIconic, bIconicDefault = FALSE;
    BOOL bMaximized, bMaximizedDefault = FALSE;
    CString  strRect, strRectDefault;
    strRectDefault.Format("%04d %04d %04d %04d", x, y, x+w, y+h);

    CWinApp* pApp = AfxGetApp();
    bIconic = pApp->GetProfileInt(s_profileFrameWindow,
        s_profileIconic, bIconicDefault);
    bMaximized = pApp->GetProfileInt(s_profileFrameWindow,
        s_profileMaximized, bMaximizedDefault);
	strRect = pApp->GetProfileString(s_profileFrameWindow,
        s_profileRectangle, strRectDefault);

    UINT    nFlags;
    CRect   rect;
    WINDOWPLACEMENT   wndpl;

    rect.left = atoi((const char*) strRect);
    rect.top = atoi((const char*) strRect.Mid(5,5));
    rect.right = atoi((const char*) strRect.Mid(10,5));
    rect.bottom = atoi((const char*) strRect.Mid(15,5));

    if(bIconic)
    {
        nCmdShow = SW_SHOWMINNOACTIVE;
        if(bMaximized)
            nFlags = WPF_RESTORETOMAXIMIZED;
        else
            nFlags = WPF_SETMINPOSITION;
    }
    else 
    {
        if(bMaximized)
        {
            nCmdShow = SW_SHOWMAXIMIZED;
            nFlags = WPF_RESTORETOMAXIMIZED;
        }
        else
        {
            nCmdShow = SW_NORMAL;
            nFlags = WPF_SETMINPOSITION;
        }
    }
    wndpl.length = sizeof(WINDOWPLACEMENT);
    wndpl.showCmd = nCmdShow;
    wndpl.flags = nFlags;
    wndpl.ptMinPosition = CPoint(0,0);
    wndpl.ptMaxPosition =
        CPoint(-::GetSystemMetrics(SM_CXBORDER),
        -::GetSystemMetrics(SM_CYBORDER));
    wndpl.rcNormalPosition = rect;
    LoadBarState(AfxGetApp()->m_pszProfileName);
    SetWindowPlacement(&wndpl);

	CMDIFrameWnd::ActivateFrame(nCmdShow);
}

void CIceMDIFrameWnd::OnClose() 
{
	// TODO: Add your message handler code here and/or call default

    CString  strRect;
    BOOL  bIconic, bMaximized;

    WINDOWPLACEMENT   wndpl;
    wndpl.length = sizeof(WINDOWPLACEMENT);
    GetWindowPlacement(&wndpl);

    switch(wndpl.showCmd)
    {
    case  SW_SHOWNORMAL:
        bIconic = FALSE;
        bMaximized = FALSE;
        break;
    case  SW_SHOWMAXIMIZED:
        bIconic = FALSE;
        bMaximized = TRUE;
        break;
    case  SW_SHOWMINIMIZED:
        bIconic = TRUE;
        if(wndpl.flags)
            bMaximized = TRUE;
        else
            bMaximized = FALSE;
        break;
    default:
        break;
    }

    strRect.Format("%04d %04d %04d %04d",
        wndpl.rcNormalPosition.left,
        wndpl.rcNormalPosition.top,
        wndpl.rcNormalPosition.right,
        wndpl.rcNormalPosition.bottom);

    CWinApp* pApp = AfxGetApp();
    pApp->WriteProfileInt(s_profileFrameWindow,
        s_profileIconic, (int) bIconic);
    pApp->WriteProfileInt(s_profileFrameWindow,
        s_profileMaximized, (int) bMaximized);
	pApp->WriteProfileString(s_profileFrameWindow,
        s_profileRectangle, strRect);
    SaveBarState(pApp->m_pszProfileName);

	CMDIFrameWnd::OnClose();
}

