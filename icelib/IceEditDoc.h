// iceEditDoc.h : interface of the CIceEditDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ICEEDITDOC_H__C7AA060D_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_ICEEDITDOC_H__C7AA060D_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


class CIceEditSrvrItem;

class CIceEditDoc : public CRichEditDoc
{
protected: // create from serialization only
	CIceEditDoc();
	DECLARE_DYNCREATE(CIceEditDoc)

// Attributes
public:
	CIceEditSrvrItem* GetEmbeddedItem()
		{ return (CIceEditSrvrItem*)CRichEditDoc::GetEmbeddedItem(); }

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceEditDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	protected:
	virtual COleServerItem* OnGetEmbeddedItem();
	//}}AFX_VIRTUAL
	virtual CRichEditCntrItem* CreateClientItem(REOBJECT* preo) const;

// Implementation
public:
	virtual ~CIceEditDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CIceEditDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICEEDITDOC_H__C7AA060D_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_)
