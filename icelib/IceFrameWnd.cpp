// IceFrameWnd.cpp : implementation file
//

#include "stdafx.h"
#include "IceFrameWnd.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


UINT _SetSafeTimer(CWnd * pWnd, const UINT uiElapse)
{
	ASSERT(pWnd && pWnd->GetSafeHwnd());
	UINT iTimer = 1;
	while (iTimer < 10000)
	{
		UINT res = pWnd->SetTimer(1, uiElapse, NULL);
		if (res) return res;
		iTimer ++;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CIceFrameWnd

IMPLEMENT_DYNCREATE(CIceFrameWnd, CFrameWnd)

CIceFrameWnd::CIceFrameWnd()
{
	oldMenuBarRc.SetRect(0,0,0,0);
	bMenuLooping = false;
	uiTimerId = 0;

    m_bFirstTime = true;

	iceMenu.LoadToolBarResource(IDR_MAINFRAME);
	iceMenu.AddToolBarResource(IDR_SYSTEMBAR);
	iceMenu.AddToolBarResource(IDR_RESERVOIR);
}

CIceFrameWnd::~CIceFrameWnd()
{
}


BEGIN_MESSAGE_MAP(CIceFrameWnd, CFrameWnd)
	//{{AFX_MSG_MAP(CIceFrameWnd)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_NCHITTEST()
	ON_WM_TIMER()
	ON_WM_INITMENU()
	ON_WM_INITMENUPOPUP()
	ON_WM_MENUCHAR()
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
	ON_WM_NCPAINT()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
	ON_WM_ENTERMENULOOP()
	ON_WM_EXITMENULOOP()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceFrameWnd message handlers

BOOL CIceFrameWnd::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CFrameWnd::PreCreateWindow(cs);
}

int CIceFrameWnd::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	uiTimerId = _SetSafeTimer(this, 100);
	
	return 0;
}


void CIceFrameWnd::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if (!iceMenu.DrawItem(lpDrawItemStruct))
		CFrameWnd::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CIceFrameWnd::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	if (!iceMenu.MeasureItem(lpMeasureItemStruct))
		CFrameWnd::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CIceFrameWnd::OnInitMenu(CMenu* pMenu) 
{
	CFrameWnd::OnInitMenu(pMenu);
	iceMenu.RemapMenu(pMenu);	
}

void CIceFrameWnd::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu) 
{
	CFrameWnd::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
	iceMenu.RemapMenu(pPopupMenu);
}

LRESULT CIceFrameWnd::OnMenuChar(UINT nChar, UINT nFlags, CMenu* pMenu) 
{
	LRESULT lRes = CFrameWnd::OnMenuChar(nChar, nFlags, pMenu);
	if (nFlags != MF_SYSMENU)
	{
		if (HIWORD(lRes) != 2) iceMenu.FindKeyboardShortcut(nChar, nFlags, pMenu, lRes);
	}
	return lRes;
}

UINT CIceFrameWnd::OnNcHitTest(CPoint point) 
{
	UINT res = CFrameWnd::OnNcHitTest(point);
	if (!bMenuLooping && res == HTMENU)
	{
		CMenu * pMenu = GetMenu();
		ASSERT(pMenu);

		UINT mifp = MenuItemFromPoint(GetSafeHwnd(), pMenu->GetSafeHmenu(), point);
		if (mifp != (UINT) -1)
		{
			CRect rc, wrc;
			if (GetMenuItemRect(GetSafeHwnd(), pMenu->GetSafeHmenu(), mifp, &rc))
			{
				if (iceMenu.IsIceMenu(pMenu, mifp, true))
				{
					GetWindowRect(wrc);
					rc.top -= wrc.top;
					rc.bottom -= wrc.top;
					rc.left -= wrc.left;
					rc.right -= wrc.left;
					if (oldMenuBarRc != rc || oldMenuBarRc.IsRectEmpty())
					{
						CWindowDC dc(this);
						if (!(oldMenuBarRc.IsRectEmpty()))
						{
							dc.Draw3dRect(oldMenuBarRc,iceMenu.crMenu, iceMenu.crMenu);
						}
						dc.Draw3dRect(rc,iceMenu.cr3dHilight, iceMenu.cr3dShadow);
						oldMenuBarRc = rc;
					}
				}
				else
				{
					if (!(oldMenuBarRc.IsRectEmpty()))
					{
						CWindowDC dc(this);
						dc.Draw3dRect(oldMenuBarRc,iceMenu.crMenu, iceMenu.crMenu);
						oldMenuBarRc.SetRect(0,0,0,0);
					}
				}
			}
		}
	}
	else
	{
		if (!(oldMenuBarRc.IsRectEmpty()))
		{
			CWindowDC dc(this);
			dc.Draw3dRect(oldMenuBarRc,iceMenu.crMenu, iceMenu.crMenu);
			oldMenuBarRc.SetRect(0,0,0,0);
		}
	}
	
	return res;
}

void CIceFrameWnd::OnTimer(UINT nIDEvent) 
{
	if (uiTimerId == nIDEvent && !bMenuLooping)
	{
		CPoint pt;
		GetCursorPos(&pt);
		LRESULT res = SendMessage(WM_NCHITTEST, 0, MAKELONG(pt.x, pt.y));
	}
	CFrameWnd::OnTimer(nIDEvent);
}

void CIceFrameWnd::OnEnterMenuLoop(BOOL bIsTrackPopupMenu)
{
	if (!(oldMenuBarRc.IsRectEmpty()))
	{
		CWindowDC dc(this);
		dc.Draw3dRect(oldMenuBarRc,iceMenu.crMenu, iceMenu.crMenu);
		oldMenuBarRc.SetRect(0,0,0,0);
	}

	bMenuLooping = true;
}

void CIceFrameWnd::OnExitMenuLoop(BOOL bIsTrackPopupMenu)
{
	bMenuLooping = false;
}



void CIceFrameWnd::OnNcPaint() 
{
	// Do not call CFrameWnd::OnNcPaint() for painting messages
	CFrameWnd::OnNcPaint();
}

void CIceFrameWnd::RecalcLayout(BOOL bNotify) 
{
	CFrameWnd::RecalcLayout(bNotify);
}

void CIceFrameWnd::ActivateFrame(int nCmdShow) 
{
    if(m_bFirstTime)
    {
        m_bFirstTime = false;

        // Default values: used only for the first time
        int h = int(::GetSystemMetrics(SM_CYSCREEN) * 0.8); 
        int w = int(::GetSystemMetrics(SM_CXSCREEN) * 0.8); 
        int y = (::GetSystemMetrics(SM_CYSCREEN) - h) / 2; 
        int x = (::GetSystemMetrics(SM_CXSCREEN) - w) / 2;
        //

        BOOL bIconic, bIconicDefault = FALSE;
        BOOL bMaximized, bMaximizedDefault = FALSE;
        CString  strRect, strRectDefault;
        strRectDefault.Format("%04d %04d %04d %04d", x, y, x+w, y+h);

        CWinApp* pApp = AfxGetApp();
        bIconic = pApp->GetProfileInt(s_profileFrameWindow,
            s_profileIconic, bIconicDefault);
        bMaximized = pApp->GetProfileInt(s_profileFrameWindow,
            s_profileMaximized, bMaximizedDefault);
	    strRect = pApp->GetProfileString(s_profileFrameWindow,
            s_profileRectangle, strRectDefault);

        UINT    nFlags;
        CRect   rect;
        WINDOWPLACEMENT   wndpl;

        rect.left = atoi((const char*) strRect);
        rect.top = atoi((const char*) strRect.Mid(5,5));
        rect.right = atoi((const char*) strRect.Mid(10,5));
        rect.bottom = atoi((const char*) strRect.Mid(15,5));

        if(bIconic)
        {
            nCmdShow = SW_SHOWMINNOACTIVE;
            if(bMaximized)
                nFlags = WPF_RESTORETOMAXIMIZED;
            else
                nFlags = WPF_SETMINPOSITION;
        }
        else 
        {
            if(bMaximized)
            {
                nCmdShow = SW_SHOWMAXIMIZED;
                nFlags = WPF_RESTORETOMAXIMIZED;
            }
            else
            {
                nCmdShow = SW_NORMAL;
                nFlags = WPF_SETMINPOSITION;
            }
        }
        wndpl.length = sizeof(WINDOWPLACEMENT);
        wndpl.showCmd = nCmdShow;
        wndpl.flags = nFlags;
        wndpl.ptMinPosition = CPoint(0,0);
        wndpl.ptMaxPosition =
            CPoint(-::GetSystemMetrics(SM_CXBORDER),
            -::GetSystemMetrics(SM_CYBORDER));
        wndpl.rcNormalPosition = rect;
        LoadBarState(AfxGetApp()->m_pszProfileName);
        SetWindowPlacement(&wndpl);
    }

	CFrameWnd::ActivateFrame(nCmdShow);
}

void CIceFrameWnd::OnClose() 
{
	CFrameWnd::OnClose();
}

void CIceFrameWnd::OnDestroy() 
{
    CString  strRect;
    BOOL  bIconic, bMaximized;

    WINDOWPLACEMENT   wndpl;
    wndpl.length = sizeof(WINDOWPLACEMENT);
    GetWindowPlacement(&wndpl);

    switch(wndpl.showCmd)
    {
    case  SW_SHOWNORMAL:
        bIconic = FALSE;
        bMaximized = FALSE;
        break;
    case  SW_SHOWMAXIMIZED:
        bIconic = FALSE;
        bMaximized = TRUE;
        break;
    case  SW_SHOWMINIMIZED:
        bIconic = TRUE;
        if(wndpl.flags)
            bMaximized = TRUE;
        else
            bMaximized = FALSE;
        break;
    default:
        break;
    }

    strRect.Format("%04d %04d %04d %04d",
        wndpl.rcNormalPosition.left,
        wndpl.rcNormalPosition.top,
        wndpl.rcNormalPosition.right,
        wndpl.rcNormalPosition.bottom);

    CWinApp* pApp = AfxGetApp();
    pApp->WriteProfileInt(s_profileFrameWindow,
        s_profileIconic, (int) bIconic);
    pApp->WriteProfileInt(s_profileFrameWindow,
        s_profileMaximized, (int) bMaximized);
	pApp->WriteProfileString(s_profileFrameWindow,
        s_profileRectangle, strRect);
    SaveBarState(pApp->m_pszProfileName);

    ///
	CFrameWnd::OnDestroy();
    ///
	if (uiTimerId) KillTimer(uiTimerId);
	//CFrameWnd::OnDestroy();
	
}


/////////////////////////////////////////////////////////////////////////////
// CIceFrameWnd message handlers
