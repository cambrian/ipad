// IceDockingTabbedView.cpp : implementation file
//

#include "stdafx.h"
#include "IceDockingTabbedView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceDockingTabbedView

CIceDockingTabbedView::CIceDockingTabbedView()
{
	m_nActiveTab = 0;
}

CIceDockingTabbedView::~CIceDockingTabbedView()
{
	while(!m_views.IsEmpty())
	{
		TCB_ITEM *pMember=m_views.RemoveHead();
		delete pMember;
	}
}


BEGIN_MESSAGE_MAP(CIceDockingTabbedView, CIceDockingControlBar)
	//{{AFX_MSG_MAP(CIceDockingTabbedView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_NCHITTEST()
	ON_NOTIFY(TCN_SELCHANGE, IDC_DOCKINGTABBEDVIEW, OnTabSelChange)
	ON_NOTIFY(TCN_SELCHANGING, IDC_DOCKINGTABBEDVIEW, OnTabSelChanging)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()




void CIceDockingTabbedView::SetActiveView(int nNewTab)
{
	ASSERT_VALID(this);
	ASSERT(nNewTab >= 0);

	if (nNewTab!=-1 && nNewTab!=m_nActiveTab)
	{
        TCB_ITEM *newMember=m_views.GetAt(m_views.FindIndex(nNewTab));
        TCB_ITEM *oldMember=NULL;
		
        //newMember->pWnd->EnableWindow(TRUE);
        newMember->pWnd->ShowWindow(SW_SHOW);
        if (m_nActiveTab!=-1)
        {
            oldMember=m_views.GetAt(m_views.FindIndex(m_nActiveTab));
            oldMember->pWnd->ShowWindow(SW_HIDE);
            //oldMember->pWnd->EnableWindow(FALSE);
        }
        newMember->pWnd->SetFocus();

        m_pActiveView = (CView *)newMember->pWnd;
		// ????????????????????
		//((CFrameWnd *)GetParent())->SetActiveView(m_pActiveView);

        m_nActiveTab = nNewTab;
		// ???????????????????????????
		// select the tab (if tab programmatically changed)
		m_tabctrl.SetCurSel(m_nActiveTab);
    }
}

void CIceDockingTabbedView::SetActiveView(CRuntimeClass *pViewClass)
{
	ASSERT_VALID(this);
	ASSERT(pViewClass != NULL);
	ASSERT(pViewClass->IsDerivedFrom(RUNTIME_CLASS(CWnd)));
	ASSERT(AfxIsValidAddress(pViewClass, sizeof(CRuntimeClass), FALSE));

	int nNewTab = 0;
	for (POSITION pos=m_views.GetHeadPosition(); pos; m_views.GetNext(pos))
	{
		TCB_ITEM *pMember=m_views.GetAt(pos);
		if (pMember->pWnd->IsKindOf(pViewClass))
		{
			//first hide old first view
            //m_pActiveView->EnableWindow(FALSE);
            m_pActiveView->ShowWindow(SW_HIDE);
			
			// set new active view
			m_pActiveView = (CView*)pMember->pWnd;
			// enable, show, set focus to new view
			//m_pActiveView->EnableWindow(TRUE);
			m_pActiveView->ShowWindow(SW_SHOW);
			m_pActiveView->SetFocus();
			
			// ????????????????????????
			//((CFrameWnd *)GetParent())->SetActiveView(m_pActiveView);

	        m_nActiveTab = nNewTab;
			// select the tab
			m_tabctrl.SetCurSel(m_nActiveTab);

			break;
		}
		nNewTab++;
    }
}

CView* CIceDockingTabbedView::GetActiveView()
{
	return m_pActiveView;
}

CView* CIceDockingTabbedView::GetView(int nView)
{
	ASSERT_VALID(this);
	ASSERT(nView >= 0);

	if (nView!=-1)
	{
        TCB_ITEM *pMember=m_views.GetAt(m_views.FindIndex(nView));
		return (CView*)pMember->pWnd;
	}
	else
		return NULL;
}

CView* CIceDockingTabbedView::GetView(CRuntimeClass *pViewClass)
{
	ASSERT_VALID(this);
	ASSERT(pViewClass != NULL);
	ASSERT(pViewClass->IsDerivedFrom(RUNTIME_CLASS(CWnd)));
	ASSERT(AfxIsValidAddress(pViewClass, sizeof(CRuntimeClass), FALSE));

	for (POSITION pos=m_views.GetHeadPosition(); pos; m_views.GetNext(pos))
	{
		TCB_ITEM *pMember=m_views.GetAt(pos);
		if (pMember->pWnd->IsKindOf(pViewClass))
		{
			return (CView*)pMember->pWnd;
		}
    }
	return NULL;
}


BOOL CIceDockingTabbedView::AddView(LPCTSTR lpszLabel, CRuntimeClass *pViewClass, CCreateContext *pContext)
{	

#ifdef _DEBUG
	ASSERT_VALID(this);
	ASSERT(pViewClass != NULL);
	ASSERT(pViewClass->IsDerivedFrom(RUNTIME_CLASS(CWnd)));
	ASSERT(AfxIsValidAddress(pViewClass, sizeof(CRuntimeClass), FALSE));
#endif

	CCreateContext context;
	if (pContext == NULL)
	{
		// if no context specified, generate one from the currently selected
		//  client if possible
		CView* pOldView = NULL;
		if (pOldView != NULL && pOldView->IsKindOf(RUNTIME_CLASS(CView)))
		{
			// set info about last pane
			ASSERT(context.m_pCurrentFrame == NULL);
			context.m_pLastView = pOldView;
			context.m_pCurrentDoc = pOldView->GetDocument();
			if (context.m_pCurrentDoc != NULL)
				context.m_pNewDocTemplate =
				context.m_pCurrentDoc->GetDocTemplate();
		}
		pContext = &context;
	}
	
	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
			AfxThrowMemoryException();
	}
	CATCH_ALL(e)
	{
		TRACE0("Out of memory creating a view.\n");
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL
		
    ASSERT_KINDOF(CWnd, pWnd);
	ASSERT(pWnd->m_hWnd == NULL);       // not yet created
	
	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	CRect rect;
	// Create with the right size and position
	if (!pWnd->Create(NULL, NULL, dwStyle, rect, this, 0, pContext))
	{
		TRACE0("Warning: couldn't create client pane for view.\n");
		// pWnd will be cleaned up by PostNcDestroy
		return FALSE;
	}
	m_pActiveView = (CView*) pWnd;

	TCB_ITEM *pMember=new TCB_ITEM;
	pMember->pWnd=pWnd;
	strcpy(pMember->szLabel, lpszLabel);
	m_views.AddTail(pMember);

	int nViews = m_views.GetCount();
	if (nViews!=1)
	{
		//pWnd->EnableWindow(FALSE);
		pWnd->ShowWindow(SW_HIDE);
	}
	else
	{
		pWnd->ShowWindow(SW_SHOW);
		//((CFrameWnd *)GetParent())->SetActiveView((CView *)m_pActiveView);
	}

	TC_ITEM tci;
	tci.mask = TCIF_TEXT | TCIF_IMAGE;
	tci.pszText = (LPTSTR)(LPCTSTR)lpszLabel;
	tci.iImage = nViews-1;
	m_tabctrl.InsertItem(nViews, &tci);

	return TRUE;
}

void CIceDockingTabbedView::RemoveView(int nView)
{
	ASSERT_VALID(this);
	ASSERT(nView >= 0);

	// remove the page from internal list
	m_views.RemoveAt(m_views.FindIndex(nView));
}


/////////////////////////////////////////////////////////////////////////////
// CIceDockingTabbedView message handlers

int CIceDockingTabbedView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceDockingControlBar::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here

	/*
	//Create the Tab Control
	CRect rect;
	m_tabctrl.Create(WS_VISIBLE | WS_CHILD 
		| TCS_FOCUSNEVER | TCS_BOTTOM | TCS_FIXEDWIDTH | TCS_FORCEICONLEFT, 
		rect, this, IDC_DOCKINGTABBEDVIEW);
	
	m_images.Create(IDB_THUMBTACK, 16, 1, RGB(255,0,255));
	m_tabctrl.SetImageList(&m_images);
	
	// set "normal" GUI-font
	CFont *font = CFont::FromHandle((HFONT)::GetStockObject(DEFAULT_GUI_FONT));
	m_tabctrl.SetFont(font);
	*/

	return 0;
}

void CIceDockingTabbedView::OnSize(UINT nType, int cx, int cy) 
{
	CIceDockingControlBar::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	
	int bottom = (IsHorzDocked() || IsFloating()) ? cy - 14 : cy - 18;

	m_tabctrl.MoveWindow(7, 7, cx - 14, bottom);
	
	CWnd *pWnd;
    for (POSITION pos=m_views.GetHeadPosition(); pos; m_views.GetNext(pos))
    {
         pWnd=m_views.GetAt(pos)->pWnd;
         pWnd->MoveWindow(9, 9, cx-19, bottom-26);
    }
	
}

void CIceDockingTabbedView::OnTabSelChanging(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: Add your control notification handler code here
	// Call GetCurSel() here will return the index of the page that
	// will be hidden soon.  This is NOT the newly selected page when 
	// OnTabSelChange() is called.
	
    TCB_ITEM *oldMember=NULL;
    oldMember=m_views.GetAt(m_views.FindIndex(m_tabctrl.GetCurSel()));
    oldMember->pWnd->ShowWindow(SW_HIDE);
    //oldMember->pWnd->EnableWindow(FALSE);

	*pResult = FALSE;		// return FALSE to allow seletion to change
}

void CIceDockingTabbedView::OnTabSelChange(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: Add your control notification handler code here
	// Call GetCurSel() will return the index of the page that is newly
	// selected.  This page will be visible soon.
	
    TCB_ITEM *newMember=m_views.GetAt(m_views.FindIndex(m_tabctrl.GetCurSel()));
	
    //newMember->pWnd->EnableWindow(TRUE);
    newMember->pWnd->ShowWindow(SW_SHOW);
    newMember->pWnd->SetFocus();

    m_pActiveView = (CView *)newMember->pWnd;
	// ????????????????????
	//((CFrameWnd *)GetParent())->SetActiveView(m_pActiveView);

    m_nActiveTab = m_tabctrl.GetCurSel();
    
    //Why do I need this???????????
    Invalidate();

	*pResult = 0;
}

BOOL CIceDockingTabbedView::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CIceDockingControlBar::PreCreateWindow(cs);
}

UINT CIceDockingTabbedView::OnNcHitTest(CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	return CIceDockingControlBar::OnNcHitTest(point);
}
