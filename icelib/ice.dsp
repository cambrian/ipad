# Microsoft Developer Studio Project File - Name="ice" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=ice - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ice.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ice.mak" CFG="ice - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ice - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "ice - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ice - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "ice - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "ice - Win32 Release"
# Name "ice - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ice.rc
# End Source File
# Begin Source File

SOURCE=.\IceButton.cpp
# End Source File
# Begin Source File

SOURCE=.\IceDockingControlBar.cpp
# End Source File
# Begin Source File

SOURCE=.\IceDockingTabbedView.cpp
# End Source File
# Begin Source File

SOURCE=.\IceEditCntrItem.cpp
# End Source File
# Begin Source File

SOURCE=.\IceEditDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\IceEditSrvrItem.cpp
# End Source File
# Begin Source File

SOURCE=.\IceEditView.cpp
# End Source File
# Begin Source File

SOURCE=.\IceFrameWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\IceMDIChildWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\IceMDIFrameWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\IceMenu.cpp
# End Source File
# Begin Source File

SOURCE=.\IceStatic.cpp
# End Source File
# Begin Source File

SOURCE=.\IceStatusBar.cpp
# End Source File
# Begin Source File

SOURCE=.\IceToolBar.cpp
# End Source File
# Begin Source File

SOURCE=.\ISBPaneInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\ISBPaneText.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\IceButton.h
# End Source File
# Begin Source File

SOURCE=.\IceDockingControlBar.h
# End Source File
# Begin Source File

SOURCE=.\IceDockingTabbedView.h
# End Source File
# Begin Source File

SOURCE=.\IceEditCntrItem.h
# End Source File
# Begin Source File

SOURCE=.\IceEditDoc.h
# End Source File
# Begin Source File

SOURCE=.\IceEditSrvrItem.h
# End Source File
# Begin Source File

SOURCE=.\IceEditView.h
# End Source File
# Begin Source File

SOURCE=.\IceFrameWnd.h
# End Source File
# Begin Source File

SOURCE=.\IceMDIChildWnd.h
# End Source File
# Begin Source File

SOURCE=.\IceMDIFrameWnd.h
# End Source File
# Begin Source File

SOURCE=.\IceMenu.h
# End Source File
# Begin Source File

SOURCE=.\IceStatic.h
# End Source File
# Begin Source File

SOURCE=.\IceStatusBar.h
# End Source File
# Begin Source File

SOURCE=.\IceToolBar.h
# End Source File
# Begin Source File

SOURCE=.\ISBPaneInfo.h
# End Source File
# Begin Source File

SOURCE=.\ISBPaneText.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\res\mainfram.bmp
# End Source File
# Begin Source File

SOURCE=.\res\MenuChk.bmp
# End Source File
# Begin Source File

SOURCE=.\res\mrufile.bmp
# End Source File
# Begin Source File

SOURCE=.\res\reservoi.bmp
# End Source File
# Begin Source File

SOURCE=.\res\sdiframe.bmp
# End Source File
# Begin Source File

SOURCE=.\res\sysbar.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\Readme.txt
# End Source File
# End Target
# End Project
