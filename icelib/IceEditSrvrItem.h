// SrvrItem.h : interface of the CIceEditSrvrItem class
//

#if !defined(AFX_SRVRITEM_H__C7AA0612_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_SRVRITEM_H__C7AA0612_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CIceEditSrvrItem : public COleServerItem
{
	DECLARE_DYNAMIC(CIceEditSrvrItem)

// Constructors
public:
	CIceEditSrvrItem(CIceEditDoc* pContainerDoc);

// Attributes
	CIceEditDoc* GetDocument() const
		{ return (CIceEditDoc*)COleServerItem::GetDocument(); }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceEditSrvrItem)
	public:
	virtual BOOL OnDraw(CDC* pDC, CSize& rSize);
	virtual BOOL OnGetExtent(DVASPECT dwDrawAspect, CSize& rSize);
	//}}AFX_VIRTUAL

// Implementation
public:
	~CIceEditSrvrItem();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SRVRITEM_H__C7AA0612_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_)
