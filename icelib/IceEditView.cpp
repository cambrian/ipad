// IceEditView.cpp : implementation of the CIceEditView class
//

#include "stdafx.h"

#include "IceEditDoc.h"
#include "IceEditCntrItem.h"
#include "IceEditView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


static LPCTSTR szKeywords = " Call Const Dim Do Loop Erase Exit For To Step Next "
"Each Function If Then Else On Error Resume Option Explicit Private Public Randomize "
"ReDim Rem Select Case Set Sub End While Wend "
"And Eqv Imp Is Mod Not Or Xor ";

static LPCTSTR szConstants = " Empty False True Nothing Null "
"vbBlack vbRed vbGreen vbYellow vbBlue vbMagenta vbCyan vbWhite "
"vbBinaryCompare vbTextCompare vbDatabaseCompare "
"vbGeneralDate vbLongDate vbShortDate vbLongTime vbShortTime "
"vbSunday vbMonday vbTuesday vbWednesday vbThursday vbFriday vbSaturday "
"vbFirstJan1 vbFirstFourDays vbFirstFullWeek vbUseSystem vbUseSystemDayOfWeek "
"vbObjectError vbOKOnly vbOKCancel vbAbortRetryIgnore vbYesNoCancel vbYesNo "
"vbRetryCancel vbCritical vbQuestion vbExclamation vbInformation "
"vbDefaultButton1 vbDefaultButton2 vbDefaultButton3 vbDefaultButton4 "
"vbApplicationModal vbSystemModal "
"vbCr vbCrLf vbFormFeed vbLf vbNewLine vbNullChar vbNullString vbTab vbVerticalTab "
"vbEmpty vbNull vbInteger vbLong vbSingle vbDouble vbCurrency vbDate vbString "
"vbObject vbError vbBoolean vbVariant vbDataObject vbDecimal vbByte vbArray ";



/////////////////////////////////////////////////////////////////////////////
// CIceEditView

IMPLEMENT_DYNCREATE(CIceEditView, CRichEditView)

BEGIN_MESSAGE_MAP(CIceEditView, CRichEditView)
	//{{AFX_MSG_MAP(CIceEditView)
	ON_WM_DESTROY()
	//ON_COMMAND(ID_CANCEL_EDIT_SRVR, OnCancelEditSrvr)
	ON_CONTROL_REFLECT(EN_CHANGE, OnChange)
	ON_WM_CREATE()
	ON_WM_CHAR()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CRichEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CRichEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CRichEditView::OnFilePrintPreview)

	ON_NOTIFY_REFLECT(EN_PROTECTED, OnProtected)
	ON_MESSAGE(WM_SETTEXT, OnSetText)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceEditView construction/destruction

CIceEditView::CIceEditView()
{
    m_nWordWrap = WrapToTargetDevice;

	m_chComment = 1;
	m_bCaseSensitive = FALSE;
	m_bChangeCase = TRUE;

	SetStringQuotes(_T("\""));

	SetTextColor(RGB(0,0,0), FALSE);
	SetKeywordColor(RGB(0,0,255), FALSE);
	SetConstantColor(RGB(0,0,0), TRUE);
	SetCommentColor(RGB(0,128,0), FALSE);
	SetNumberColor(RGB(255,0,255), FALSE);
	SetStringColor(RGB(255,0,0), FALSE);

	m_bInForcedChange = FALSE;
	m_changeType = ctUndo;
	m_crOldSel.cpMin = m_crOldSel.cpMax = 0;
}

CIceEditView::~CIceEditView()
{
}

BOOL CIceEditView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

    /*
    cs.style |= ES_MULTILINE ;
	cs.style |= WS_HSCROLL;       // No word-wrapping !!!!!
	cs.style |= WS_VSCROLL;
    cs.style |= ES_AUTOHSCROLL ;
    cs.style |= ES_AUTOVSCROLL ;
    */

    BOOL bPreCreated = CRichEditView::PreCreateWindow(cs);

	return bPreCreated;
}

void CIceEditView::OnInitialUpdate()
{
    //GetRichEditCtrl().SetOptions(ECOOP_OR, ECO_AUTOHSCROLL);

	CRichEditView::OnInitialUpdate();

	// Set the printing margins (720 twips = 1/2 inch).
	SetMargins(CRect(720, 720, 720, 720));
}

/////////////////////////////////////////////////////////////////////////////
// CIceEditView printing

BOOL CIceEditView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}


void CIceEditView::OnDestroy()
{
	// Deactivate the item on destruction; this is important
	// when a splitter view is being used.
   CRichEditView::OnDestroy();
   COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
   if (pActiveItem != NULL && pActiveItem->GetActiveView() == this)
   {
      pActiveItem->Deactivate();
      ASSERT(GetDocument()->GetInPlaceActiveItem(this) == NULL);
   }
}

int CIceEditView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CRichEditView::OnCreate(lpCreateStruct) == -1)
		return -1;

	Initialize();

	SetCaseSensitive(FALSE);
	SetStringQuotes(_T("\""));
	SetSLComment(_T('\''));
	SetSLComment(_T("REM"));
	SetMLOpenComment(_T('/'),_T('*'));
	SetMLCloseComment(_T('*'),_T('/'));
	AddKeywords(szKeywords);
	AddConstants(szConstants);
	
	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// OLE Server support

// The following command handler provides the standard keyboard
//  user interface to cancel an in-place editing session.  Here,
//  the server (not the container) causes the deactivation.
void CIceEditView::OnCancelEditSrvr()
{
	GetDocument()->OnDeactivateUI(FALSE);
}

/////////////////////////////////////////////////////////////////////////////
// CIceEditView diagnostics

#ifdef _DEBUG
void CIceEditView::AssertValid() const
{
	CRichEditView::AssertValid();
}

void CIceEditView::Dump(CDumpContext& dc) const
{
	CRichEditView::Dump(dc);
}

CIceEditDoc* CIceEditView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CIceEditDoc)));
	return (CIceEditDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIceEditView message handlers


void CIceEditView::Initialize() 
{
	PARAFORMAT pf;
	pf.cbSize = sizeof(PARAFORMAT);
	pf.dwMask = PFM_TABSTOPS ;
	pf.cTabCount = MAX_TAB_STOPS;
	for( int itab = 0 ; itab < pf.cTabCount  ; itab++ )
		pf.rgxTabs[itab] = (itab + 1) * 1440/5 ;

	SetParaFormat( pf );

	CHARFORMAT cfDefault;
	cfDefault.cbSize = sizeof(cfDefault);
	cfDefault.dwEffects = CFE_PROTECTED; 
	cfDefault.dwMask = CFM_BOLD | CFM_FACE | CFM_SIZE | CFM_CHARSET | CFM_PROTECTED;
	cfDefault.yHeight = 200;
	cfDefault.bCharSet = 0xEE; 
	strcpy(cfDefault.szFaceName, _T("Courier New")); 

	GetRichEditCtrl().SetDefaultCharFormat(cfDefault);
	GetRichEditCtrl().SetEventMask(ENM_CHANGE | ENM_SELCHANGE | ENM_PROTECTED);
}

void CIceEditView::SetSLComment(TCHAR chComment, TCHAR chComment2)
{
	m_chComment = chComment;
	m_chComment2 = chComment2;
}

void CIceEditView::SetSLComment(LPCTSTR lpszComment)
{
	m_strComment = lpszComment;
}

void CIceEditView::SetMLOpenComment(TCHAR chMLOpenComment, TCHAR chMLOpenComment2, TCHAR chMLOpenComment3)
{
	m_chMLOpenComment = chMLOpenComment;
	m_chMLOpenComment2 = chMLOpenComment2;
	m_chMLOpenComment3 = chMLOpenComment3;
}

void CIceEditView::SetMLCloseComment(TCHAR chMLCloseComment, TCHAR chMLCloseComment2)
{
	m_chMLCloseComment = chMLCloseComment;
	m_chMLCloseComment2 = chMLCloseComment2;
}

void CIceEditView::AddKeywords(LPCTSTR lpszKwd)
{
	m_strKeywords = m_strKeywords + lpszKwd;
	m_strKeywordsLower = m_strKeywords;
	if (!m_bCaseSensitive)
		m_strKeywordsLower.MakeLower();
}

void CIceEditView::AddKeywordsFromFile(CString fileName)
{
    //LPCTSTR lpszKwd;
    //AddKeywords(lpszKwd);
}

void CIceEditView::ClearKeywords()
{
	m_strKeywords.Empty();
	m_strKeywordsLower.Empty();
}							  

void CIceEditView::AddConstants(LPCTSTR lpszConst)
{
	m_strConstants = m_strConstants + lpszConst;
	m_strConstantsLower = m_strConstants;
	if (!m_bCaseSensitive)
		m_strConstantsLower.MakeLower();
}

void CIceEditView::ClearConstants()
{
	m_strConstants.Empty();
	m_strConstantsLower.Empty();
}							  

void CIceEditView::SetCaseSensitive(BOOL bSensitive)
{
	m_bCaseSensitive = bSensitive;
}

void CIceEditView::SetChangeCase(BOOL bChange)
{
	m_bChangeCase = bChange;
}

void CIceEditView::SetStringQuotes(LPCTSTR lpszStrQ)
{
	m_strStringQuotes = lpszStrQ;
}

void CIceEditView::SetTextColor(COLORREF clr, BOOL bBold)
{
	m_icText.clrColor = clr;
	m_icText.bBold = bBold;
}

void CIceEditView::SetKeywordColor(COLORREF clr, BOOL bBold)
{
	m_icKeyword.clrColor = clr;
	m_icKeyword.bBold = bBold;
}

void CIceEditView::SetConstantColor(COLORREF clr, BOOL bBold)
{
	m_icConstant.clrColor = clr;
	m_icConstant.bBold = bBold;
}

void CIceEditView::SetCommentColor(COLORREF clr, BOOL bBold)
{
	m_icComment.clrColor = clr;
	m_icComment.bBold = bBold;
}

void CIceEditView::SetNumberColor(COLORREF clr, BOOL bBold)
{
	m_icNumber.clrColor = clr;
	m_icNumber.bBold = bBold;
}

void CIceEditView::SetStringColor(COLORREF clr, BOOL bBold)
{
	m_icString.clrColor = clr;
	m_icString.bBold = bBold;
}
int CIceEditView::IsKeyword(LPCTSTR lpszSymbol)
{
	CString strSymbol; strSymbol.Format(" %s ", lpszSymbol);
	if (!m_bCaseSensitive) 
		strSymbol.MakeLower();
				
	return m_strKeywordsLower.Find(strSymbol);
}

int CIceEditView::IsConstant(LPCTSTR lpszSymbol)
{
	CString strSymbol; strSymbol.Format(" %s ", lpszSymbol);
	if (!m_bCaseSensitive) 
		strSymbol.MakeLower();
				
	return m_strConstantsLower.Find(strSymbol);
}

BOOL CIceEditView::IsStringQuote(TCHAR ch)
{
	return (m_strStringQuotes.Find(ch) >= 0);
}


void CIceEditView::SetFormatRange(int nStart, int nEnd, BOOL bBold, COLORREF clr)
{
	if (nStart >= nEnd)
		return;

	GetRichEditCtrl().SetSel(nStart, nEnd);

	DWORD dwEffects = bBold?CFE_BOLD:0;

	CHARFORMAT cfm;
	cfm.cbSize = sizeof(cfm);
    GetRichEditCtrl().GetSelectionCharFormat(cfm);
	
	if ((cfm.dwMask & CFM_COLOR)  && cfm.crTextColor == clr && 
		(cfm.dwMask & CFM_BOLD) && (cfm.dwEffects & CFE_BOLD) == dwEffects)
		return;

	cfm.dwEffects = dwEffects;
	cfm.crTextColor = clr;
	cfm.dwMask = CFM_BOLD | CFM_COLOR;

	GetRichEditCtrl().SetSelectionCharFormat(cfm);
}

void CIceEditView::ChangeCase(int nStart, int nEnd, LPCTSTR lpsz)
{
	ASSERT((nEnd - nStart) == (int)_tcslen(lpsz));

	if (!m_bCaseSensitive && m_bChangeCase) {
		GetRichEditCtrl().SetSel(nStart, nEnd);
		GetRichEditCtrl().ReplaceSel(lpsz);
	}
}

void CIceEditView::FormatTextRange(int nStart, int nEnd)
{
	if (nStart >= nEnd)
		return;
    if(nStart < 0)
        nStart = 0;
    if(nEnd > GetTextLength())
        nEnd = GetTextLength();

	m_bInForcedChange = TRUE;

    CHARRANGE crOldSel;

	GetRichEditCtrl().GetSel(crOldSel);
	LockWindowUpdate();
	GetRichEditCtrl().HideSelection(TRUE, FALSE);

	TCHAR *pBuffer = NULL;
	try {
		GetRichEditCtrl().SetSel(nStart, nEnd);
		pBuffer = new TCHAR[nEnd - nStart + 1];
		long nLen = GetRichEditCtrl().GetSelText(pBuffer);

		ASSERT(nLen <= nEnd - nStart);

		pBuffer[nLen] = 0;

		TCHAR *pStart, *pPtr;
		pStart = pPtr = pBuffer;

		TCHAR* pSymbolStart = NULL;
		SymbolColor ic;

		while (*pPtr != 0) {
			TCHAR ch = *pPtr;
			TCHAR chprev;

            if ((ch == m_chMLOpenComment && pPtr[1] == m_chMLOpenComment2 
                && (m_chMLOpenComment3 == 0 || pPtr[2] == m_chMLOpenComment3))) {
                pSymbolStart = pPtr;
				do {
					chprev = *(pPtr);
					ch = *(++pPtr);
				} while (ch != 0  && (ch != m_chMLCloseComment2 || chprev != m_chMLCloseComment)); 
				ic = m_icComment;
                if(ch != 0) {
				    ++pPtr;
                }
            /*  //???????????????
            } else if ((ch == m_chMLCloseComment && pPtr[1] == m_chMLCloseComment2)) {
                pSymbolStart = pPtr;
				do {
					chprev = *(pPtr);
					ch = *(++pPtr);
				} while (ch != 0  && (ch != '\n')); 
				ic = m_icText;
            */
            } else if (ch == m_chComment && (m_chComment2 == 0 || pPtr[1] == m_chComment2)) {
				pSymbolStart = pPtr;
				do {
					ch = *(++pPtr);
				} while (ch != 0 && (ch != '\n')); // or (ch != '\r')????
				ic = m_icComment;
			} else if (IsStringQuote(ch)) { // Process strings
				pSymbolStart = pPtr;
				TCHAR ch1 = ch;
				do {
					ch = *(++pPtr);
				} while (ch != 0 && ch != ch1 && (ch != '\n')); // or (ch != '\r')????
				if (ch == ch1) pPtr++;
				ic = m_icString;
			} else if (_istdigit(ch)) { // Process numbers
				pSymbolStart = pPtr;
				_tcstod(pSymbolStart, &pPtr);
				ic = m_icNumber;
			} else if (_istalpha(ch) || ch == '_') { // Process keywords
				pSymbolStart = pPtr;
				do {
					ch = *(++pPtr);
				} while (_istalnum(ch) || ch == '_');
				*pPtr = 0;
				int nPos = IsKeyword(pSymbolStart);
				if (nPos >= 0) {
					ChangeCase(nStart + pSymbolStart - pBuffer, nStart + pPtr - pBuffer, 
								m_strKeywords.Mid(nPos+1, pPtr - pSymbolStart));
					if (_tcsicmp(m_strComment, pSymbolStart) == 0) {
						*pPtr = ch;
						*pSymbolStart = m_chComment;
						if (pSymbolStart[1] != 0 && m_chComment2 != 0)
							pSymbolStart[1] = m_chComment2;
						pPtr = pSymbolStart;
						pSymbolStart = NULL;
						continue;
					}
					ic = m_icKeyword;
				} else {
					nPos = IsConstant(pSymbolStart);
					if (nPos >= 0) {
							ChangeCase(nStart + pSymbolStart - pBuffer, nStart + pPtr - pBuffer, 
										m_strConstants.Mid(nPos+1, pPtr - pSymbolStart));
						ic = m_icConstant;
					} else {
						pSymbolStart = NULL;
					}
				}
				*pPtr = ch;
			} else {
				pPtr++;
			}

			if (pSymbolStart != NULL) {
				ASSERT(pSymbolStart < pPtr);
				SetFormatRange(nStart + pStart - pBuffer, nStart + pSymbolStart - pBuffer, FALSE, RGB(0,0,0));
				SetFormatRange(nStart + pSymbolStart - pBuffer, nStart + pPtr - pBuffer, ic.bBold, ic.clrColor);
				pStart = pPtr;
				pSymbolStart = 0;
			} else if (*pPtr == 0)
				SetFormatRange(nStart + pStart - pBuffer, nStart + pPtr - pBuffer, FALSE, RGB(0,0,0));
		}

	} catch(...){}

	delete [] pBuffer;

	GetRichEditCtrl().SetSel(crOldSel);
	GetRichEditCtrl().HideSelection(FALSE, FALSE);
	UnlockWindowUpdate();

	m_bInForcedChange = FALSE;

}



void CIceEditView::FormatTextLines(int nCharStart, int nCharEnd)
{
    long nStart = GetRichEditCtrl().LineIndex(GetRichEditCtrl().LineFromChar(nCharStart));
    long nEnd = GetRichEditCtrl().LineIndex(GetRichEditCtrl().LineFromChar(nCharEnd));
    nEnd += GetRichEditCtrl().LineLength(nCharEnd);

    //FormatTextRange(nStart, nEnd);

    /////////////////////////////////////////////////////////
    CHARRANGE crresult;
    crresult.cpMin = 0;
    crresult.cpMax = GetTextLength();
    TCHAR mlOpen[] = {m_chMLOpenComment, m_chMLOpenComment2, '\0'};
    TCHAR mlClose[] = {m_chMLCloseComment, m_chMLCloseComment2, '\0'};

    // Find mStart: beginnng of formatting
    // Find the closest "/ *" before nStart
    CHARRANGE crs;
    FINDTEXTEX  fts;
    crs.cpMin = 0;
    crs.cpMax = nStart;
    fts.chrg = crs;
    fts.lpstrText = (LPSTR) mlClose;
    fts.chrgText = crresult;

    long mStart;
    long rsStartO, rsStartC;
    long mso = GetRichEditCtrl().FindText(FR_MATCHCASE, &fts); 
    if(mso >= 0)
    {
        while(mso < nStart)
        {
            rsStartC = (mso+2 < GetTextLength()) ? mso+2 : GetTextLength();
            crs.cpMin = mso+2;
            fts.chrg = crs;
            mso = GetRichEditCtrl().FindText(FR_MATCHCASE, &fts);
            if(mso < 0)
                break;
        }
    }
    else
    {
        rsStartC = 0;
    }

    crs.cpMin = rsStartC;
    crs.cpMax = nStart;
    fts.chrg = crs;
    fts.lpstrText = (LPSTR) mlOpen;
    fts.chrgText = crresult;

    long msc = GetRichEditCtrl().FindText(FR_MATCHCASE, &fts); 
    if(msc >= 0)
    {
        while(msc < nStart)
        {
            rsStartO = msc;
            crs.cpMin = msc+2;
            fts.chrg = crs;
            msc = GetRichEditCtrl().FindText(FR_MATCHCASE, &fts);
            if(msc < 0)
                break;
        }
    }
    else
    {
        rsStartO = rsStartC;
    }

    if(rsStartC == 0 && rsStartO == rsStartC)
    {
        mStart = nStart;
    }
    else
    {
        mStart = rsStartO;
    }



    // Find mEnd: end of formatting
    // Find closest "* /" after nEnd
    CHARRANGE cre;
    FINDTEXTEX  fte;
    cre.cpMin = nEnd;
    cre.cpMax = GetTextLength();
    fte.chrg = cre;
    fte.lpstrText = (LPSTR) mlOpen;
    fte.chrgText = crresult;

    long mEnd;
    long rsEnd;
    long meo = GetRichEditCtrl().FindText(FR_MATCHCASE, &fte); 
    if(meo >= 0)
    {
        rsEnd = meo;
    }
    else
    {
        rsEnd = GetTextLength();
    }

    cre.cpMin = nEnd;
    crs.cpMax = rsEnd;
    fte.chrg = cre;
    fte.lpstrText = (LPSTR) mlClose;
    fte.chrgText = crresult;

    long mec = GetRichEditCtrl().FindText(FR_MATCHCASE, &fte); 
    if(mec >= 0)
    {
        mEnd = (mec+2 < GetTextLength()) ? mec+2 : GetTextLength();
    }
    else
    {
        mEnd = rsEnd;
    }

    
    // If the range becomes too big, the program starts to be slowing down
    //long maxSpan = 5000;
    //if(mEnd > nEnd + maxSpan)
    //    mEnd = nEnd + maxSpan;

    // Finally format the range
	FormatTextRange(mStart, mEnd);

}

// This function is currently not used!!!!!
void CIceEditView::FormatTextRangeEx(long nStart, long nEnd)
{
    CHARRANGE crresult;
    crresult.cpMin = 0;
    crresult.cpMax = GetTextLength();
    char mlOpen[] = {m_chMLOpenComment, m_chMLOpenComment2, '\0'};
    char mlClose[] = {m_chMLCloseComment, m_chMLCloseComment2, '\0'};

    // Find mStart: beginnng of formatting
    // Find the closest "/ *" before nStart
    CHARRANGE crs;
    FINDTEXTEX  fts;
    crs.cpMin = 0;
    crs.cpMax = nStart;
    fts.chrg = crs;
    fts.lpstrText = (LPSTR) mlClose;
    fts.chrgText = crresult;

    long mStart;
    long rsStartO, rsStartC;
    long mso = GetRichEditCtrl().FindText(FR_MATCHCASE, &fts); 
    if(mso >= 0)
    {
        while(mso < nStart)
        {
            rsStartC = (mso+2 < GetTextLength()) ? mso+2 : GetTextLength();
            crs.cpMin = mso+2;
            fts.chrg = crs;
            mso = GetRichEditCtrl().FindText(FR_MATCHCASE, &fts);
            if(mso < 0)
                break;
        }
    }
    else
    {
        rsStartC = 0;
    }

    crs.cpMin = rsStartC;
    crs.cpMax = nStart;
    fts.chrg = crs;
    fts.lpstrText = (LPSTR) mlOpen;
    fts.chrgText = crresult;

    long msc = GetRichEditCtrl().FindText(FR_MATCHCASE, &fts); 
    if(msc >= 0)
    {
        while(msc < nStart)
        {
            rsStartO = msc;
            crs.cpMin = msc+2;
            fts.chrg = crs;
            msc = GetRichEditCtrl().FindText(FR_MATCHCASE, &fts);
            if(msc < 0)
                break;
        }
    }
    else
    {
        rsStartO = rsStartC;
    }

    if(rsStartC == 0 && rsStartO == rsStartC)
    {
        mStart = nStart;
    }
    else
    {
        mStart = rsStartO;
    }



    // Find mEnd: end of formatting
    // Find closest "* /" after nEnd
    CHARRANGE cre;
    FINDTEXTEX  fte;
    cre.cpMin = nEnd;
    cre.cpMax = GetTextLength();
    fte.chrg = cre;
    fte.lpstrText = (LPSTR) mlOpen;
    fte.chrgText = crresult;

    long mEnd;
    long rsEnd;
    long meo = GetRichEditCtrl().FindText(FR_MATCHCASE, &fte); 
    if(meo >= 0)
    {
        rsEnd = meo;
    }
    else
    {
        rsEnd = GetTextLength();
    }

    cre.cpMin = nEnd;
    crs.cpMax = rsEnd;
    fte.chrg = cre;
    fte.lpstrText = (LPSTR) mlClose;
    fte.chrgText = crresult;

    long mec = GetRichEditCtrl().FindText(FR_MATCHCASE, &fte); 
    if(mec >= 0)
    {
        mEnd = (mec+2 < GetTextLength()) ? mec+2 : GetTextLength();
    }
    else
    {
        mEnd = rsEnd;
    }

    
    // If the range becomes too big, the program starts to be slowing down
    //long maxSpan = 5000;
    //if(mEnd > nEnd + maxSpan)
    //    mEnd = nEnd + maxSpan;


    // Finally format the range
	FormatTextRange(mStart, mEnd);

}


void CIceEditView::FormatAll()
{
	FormatTextRange(0, GetTextLength());
}

LRESULT CIceEditView::OnSetText(WPARAM wParam, LPARAM lParam)
{
	LRESULT res = Default();
	//FormatAll();
	return res;	
}

void CIceEditView::OnChange() 
{
	if (m_bInForcedChange)
		return;

	CHARRANGE crCurSel; 
	GetRichEditCtrl().GetSel(crCurSel);

	if (m_changeType == ctMove && crCurSel.cpMin == crCurSel.cpMax) {
		// cut was canceled, so this is paste operation
		m_changeType = ctPaste;
	}

	switch(m_changeType) {
	case ctReplSel:// old=(x,y) -> cur=(x+len,x+len)
	case ctPaste:  // old=(x,y) -> cur=(x+len,x+len)
		FormatTextLines(m_crOldSel.cpMin, crCurSel.cpMax);
		break;
	case ctDelete: // old=(x,y) -> cur=(x,x)
	case ctBack:   // old=(x,y) -> cur=(x,x), newline del => old=(x,x+1) -> cur=(x-1,x-1)
	case ctCut:    // old=(x,y) -> cur=(x,x)
		FormatTextLines(crCurSel.cpMin, crCurSel.cpMax);
		break;
	case ctUndo:   // old=(?,?) -> cur=(x,y)
		FormatTextLines(crCurSel.cpMin, crCurSel.cpMax);
		break;
	case ctMove:   // old=(x,x+len) -> cur=(y-len,y) | cur=(y,y+len)
		FormatTextLines(crCurSel.cpMin, crCurSel.cpMax);
		if (crCurSel.cpMin > m_crOldSel.cpMin) // move after
			FormatTextLines(m_crOldSel.cpMin, m_crOldSel.cpMin);
		else // move before
			FormatTextLines(m_crOldSel.cpMax, m_crOldSel.cpMax);
		break;
	default:
		FormatAll();
		break;
	}

	//undo action does not call OnProtected, so make it default
	m_changeType = ctUndo;
}

void CIceEditView::OnProtected(NMHDR* pNMHDR, LRESULT* pResult)
{
	ENPROTECTED* pEP = (ENPROTECTED*)pNMHDR;

	// determine type of change will occur

	switch (pEP->msg) {
	case WM_KEYDOWN:
		switch (pEP->wParam) {
		case VK_DELETE:
			m_changeType = ctDelete;
			break;
		case VK_BACK:
			m_changeType = ctBack;
			break;
		default:
			m_changeType = ctUnknown;
			break;
		}
		break;
	case EM_REPLACESEL:
	case WM_CHAR:
		m_changeType = ctReplSel;
		break;
	case WM_PASTE:
		m_changeType = (m_changeType == ctCut)?ctMove:ctPaste;
		break;
	case WM_CUT:
		m_changeType = ctCut;
		break;
	case EM_SETCHARFORMAT:
		// Ignore this
		break;
	default:
		m_changeType = ctUnknown;
		break;
	};

	if (pEP->msg != EM_SETCHARFORMAT && m_changeType != ctMove)
		m_crOldSel = pEP->chrg;

	*pResult = FALSE;
}

void CIceEditView::Serialize(CArchive& ar)
{
	if (ar.IsStoring())	{
		// TODO: add storing code here
	} else {
		// TODO: add loading code here
	}
}


void CIceEditView::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    UINT i;

    switch(nChar) 
    {
    /*
    case VK_RETURN:
        // Remove '\r' from "\r\n"!!!!!!!! 
        for(i=0;i<nRepCnt;i++) {
            GetRichEditCtrl().ReplaceSel(_T("\n"),TRUE);
        }
        break;
    */
    case VK_TAB:
        // (1) If there's no selection, insert spaces instead! 
        for(i=0;i<nRepCnt;i++) {
            GetRichEditCtrl().ReplaceSel(_T("    "),TRUE);
        }
        // (2) If some text is selected, indent
        break;

    default:
        CRichEditView::OnChar(nChar, nRepCnt, nFlags);
        break;
    }
}


