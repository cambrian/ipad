// iceEditView.h : interface of the CIceEditView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ICEEDITVIEW_H__C7AA060F_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_ICEEDITVIEW_H__C7AA060F_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CIceEditCntrItem;
class CIceEditDoc;

class CIceEditView : public CRichEditView
{
protected: // create from serialization only
	CIceEditView();
	DECLARE_DYNCREATE(CIceEditView)

	virtual void Initialize();

// Attributes
public:
	CIceEditDoc* GetDocument();

	void SetCaseSensitive(BOOL bSensitive);
	void SetChangeCase(BOOL bChange);

	void SetSLComment(TCHAR chComment, TCHAR chComment2 = 0);
	void SetSLComment(LPCTSTR lpszComment);
	void SetMLOpenComment(TCHAR chMLOpenComment, TCHAR chMLOpenComment2, TCHAR chMLOpenComment3 = 0);
	void SetMLCloseComment(TCHAR chMLCloseComment, TCHAR chMLCloseComment2);

	void SetStringQuotes(LPCTSTR lpszStrQ);

	void AddKeywords(LPCTSTR lpszKwd);
	void AddConstants(LPCTSTR lpszKwd);
	void ClearKeywords();
	void ClearConstants();

	void SetTextColor(COLORREF clr, BOOL bBold);
	void SetKeywordColor(COLORREF clr, BOOL bBold);
	void SetConstantColor(COLORREF clr, BOOL bBold);
	void SetCommentColor(COLORREF clr, BOOL bBold);
	void SetNumberColor(COLORREF clr, BOOL bBold);
	void SetStringColor(COLORREF clr, BOOL bBold);


// Operations
public:
	void FormatAll();


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceEditView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void Serialize(CArchive& ar);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	void FormatTextRangeEx(long nStart, long nEnd);
	void AddKeywordsFromFile(CString fileName);
	virtual ~CIceEditView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

	BOOL IsStringQuote(TCHAR ch);
	int IsKeyword(LPCTSTR lpszSymbol);
	int IsConstant(LPCTSTR lpszSymbol);

	void SetFormatRange(int nStart, int nEnd, BOOL bBold, COLORREF clr);
	void FormatTextRange(int nStart, int nEnd);
	void FormatTextLines(int nStart, int nEnd);
	void ChangeCase(int nStart, int nEnd, LPCTSTR lpszStr);


	struct SymbolColor {
		COLORREF clrColor;
		BOOL bBold;
	};

	enum ChangeType {ctUndo, ctUnknown, ctReplSel, ctDelete, ctBack, ctCut, ctPaste, ctMove};

	BOOL m_bCaseSensitive;
	BOOL m_bChangeCase;

	TCHAR m_chComment;
	TCHAR m_chComment2;
	CString m_strComment;
	TCHAR m_chMLOpenComment;
	TCHAR m_chMLOpenComment2;
	TCHAR m_chMLOpenComment3;
	TCHAR m_chMLCloseComment;
	TCHAR m_chMLCloseComment2;
	TCHAR m_chMLCloseComment3;

	CString m_strStringQuotes;
	CString m_strKeywords;
	CString m_strKeywordsLower;
	CString m_strConstants;
	CString m_strConstantsLower;

	SymbolColor m_icText;
	SymbolColor m_icComment;
	SymbolColor m_icNumber;
	SymbolColor m_icString;
	SymbolColor m_icKeyword;
	SymbolColor m_icConstant;

	BOOL m_bInForcedChange;
	ChangeType m_changeType;
	CHARRANGE m_crOldSel;


// Generated message map functions
protected:
	//{{AFX_MSG(CIceEditView)
	afx_msg void OnDestroy();
	afx_msg void OnCancelEditSrvr();
	afx_msg void OnChange();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	afx_msg LRESULT OnSetText(WPARAM wParam, LPARAM lParam);
	afx_msg void OnProtected(NMHDR*, LRESULT* pResult);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in iceEditView.cpp
inline CIceEditDoc* CIceEditView::GetDocument()
   { return (CIceEditDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICEEDITVIEW_H__C7AA060F_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_)
