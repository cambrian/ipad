// IceToolBar.cpp : implementation file
//

#include "stdafx.h"
#include "IceToolBar.h"

#define ROP_DSPDxax  0x00E20746L
#define ROP_PSDPxax  0x00B8074AL

static  HDC  hDCImg = NULL;
static  HDC  hDCMsk = NULL;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//////////////////////////////////////////////////////////////////////
// CreateDitherBitmap()

static HBITMAP AFXAPI CreateDitherBitmap()
{
	struct  // BITMAPINFO with 16 colors
	{
		BITMAPINFOHEADER bmiHeader;
		RGBQUAD      bmiColors[16];
	} bmi;
	memset(&bmi, 0, sizeof(bmi));

	bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biWidth = 8;
	bmi.bmiHeader.biHeight = 8;
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = 1;
	bmi.bmiHeader.biCompression = BI_RGB;

	COLORREF clr = ::GetSysColor(COLOR_BTNFACE);
	bmi.bmiColors[0].rgbBlue = GetBValue(clr);
	bmi.bmiColors[0].rgbGreen = GetGValue(clr);
	bmi.bmiColors[0].rgbRed = GetRValue(clr);

	clr = ::GetSysColor(COLOR_BTNHIGHLIGHT);
	bmi.bmiColors[1].rgbBlue = GetBValue(clr);
	bmi.bmiColors[1].rgbGreen = GetGValue(clr);
	bmi.bmiColors[1].rgbRed = GetRValue(clr);

	// initialize the brushes
	long patGray[8];
	for (int i = 0; i < 8; i++)
	   patGray[i] = (i & 1) ? 0xAAAA5555L : 0x5555AAAAL;

	HDC hDC = GetDC(NULL);
	HBITMAP hbm = CreateDIBitmap(hDC, &bmi.bmiHeader, CBM_INIT,
		(LPBYTE)patGray, (LPBITMAPINFO)&bmi, DIB_RGB_COLORS);
	ReleaseDC(NULL, hDC);

	return hbm;
}

/////////////////////////////////////////////////////////////////////////////
// CIceToolBar

CIceToolBar::CIceToolBar()
{
	m_nBarGripper = 1;  // default: one bar gripper

	m_bTimer    = FALSE;
	m_nHitPrev	= -1;
	m_hPenGray  = CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_BTNFACE));
	m_hPenWhite = CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_BTNHILIGHT));
	m_hPenDgray = CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_BTNSHADOW));
	m_hBrushGray  = CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));
	m_hBrushDgray = CreateSolidBrush(::GetSysColor(COLOR_BTNSHADOW));
	m_hBrushWhite = CreateSolidBrush(::GetSysColor(COLOR_BTNHILIGHT));

	m_cyTopBorder = 4;
	m_cyBottomBorder = 4;

	HBITMAP hbmGray = ::CreateDitherBitmap();
	if (hbmGray != NULL)
	{
		m_hBrushDither = ::CreatePatternBrush(hbmGray);
		DeleteObject((HGDIOBJ*)&hbmGray);
	}
	hDCImg = CreateCompatibleDC(NULL);
    hDCMsk = CreateCompatibleDC(NULL);

}

CIceToolBar::~CIceToolBar()
{
	DeleteObject(m_hPenWhite);
	DeleteObject(m_hPenDgray);
	DeleteObject(m_hPenGray);
	DeleteObject(m_hBrushDither);
	DeleteObject(m_hBrushGray);
	DeleteObject(m_hBrushDgray);
	DeleteObject(m_hBrushWhite);

	DeleteDC(hDCImg);
	DeleteDC(hDCMsk);
}

//IMPLEMENT_DYNAMIC(CIceToolBar, CToolBar)

BEGIN_MESSAGE_MAP(CIceToolBar, CToolBar)
	//{{AFX_MSG_MAP(CIceToolBar)
	ON_WM_PAINT()
	ON_WM_NCPAINT()
	ON_WM_NCCALCSIZE()
	ON_WM_WINDOWPOSCHANGING()
	ON_WM_NCHITTEST()
	ON_WM_TIMER()
	ON_WM_SYSCOLORCHANGE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////
// Operations

void CIceToolBar::DrawButtonBorder(HDC hDC, CRect rcBorder, int iStyle)
{
	rcBorder.right--;
	rcBorder.bottom--;
	HPEN hPenOld;

	if(iStyle == 1)
	{
		hPenOld = (HPEN)SelectObject(hDC, m_hPenGray);
		MoveToEx(hDC, rcBorder.left,    rcBorder.top, NULL);
		LineTo(  hDC, rcBorder.left,    rcBorder.bottom);
		LineTo(  hDC, rcBorder.right, rcBorder.bottom);
		LineTo(  hDC, rcBorder.right, rcBorder.top);
		LineTo(  hDC, rcBorder.left,    rcBorder.top);
		SelectObject(hDC, hPenOld);
	}
	else if(iStyle == 2)
	{
		hPenOld = (HPEN)SelectObject(hDC, m_hPenWhite);
		MoveToEx(hDC, rcBorder.right, rcBorder.top, NULL);
		LineTo(hDC, rcBorder.left,  rcBorder.top);
		LineTo(hDC, rcBorder.left,  rcBorder.bottom);

		SelectObject(hDC, m_hPenDgray);
		MoveToEx(hDC, rcBorder.right, rcBorder.top, NULL);
		LineTo(hDC, rcBorder.right, rcBorder.bottom);
		LineTo(hDC, rcBorder.left,  rcBorder.bottom);
		SelectObject(hDC, hPenOld);
	}
	else if(iStyle == 3)
	{
		hPenOld = (HPEN)SelectObject(hDC, m_hPenDgray);
		MoveToEx(hDC, rcBorder.right, rcBorder.top, NULL);
		LineTo(hDC, rcBorder.left,  rcBorder.top);
		LineTo(hDC, rcBorder.left,  rcBorder.bottom);

		SelectObject(hDC, m_hPenWhite);
		MoveToEx(hDC, rcBorder.right, rcBorder.top, NULL);
		LineTo(hDC, rcBorder.right, rcBorder.bottom);
		LineTo(hDC, rcBorder.left,  rcBorder.bottom);
		SelectObject(hDC, hPenOld);
	}
}

void CIceToolBar::DrawButton(HDC hDC, BOOL bHorz)
{
	int   iImage;
	UINT  nID, nStyle;
	CRect rcClip, rcTest, rcBtn;

	HPEN   hPenOld;
	HBRUSH hbrOld;

	HBITMAP hbmMonoOld;
	HBITMAP hbmMono   = CreateBitmap(m_sizeButton.cx, m_sizeButton.cy, 1, 1, NULL);
	HBITMAP hbmImgOld = (HBITMAP)SelectObject(hDCImg, m_hbmImageWell);

	int nWidth  = m_sizeImage.cx;
	int nHeight = m_sizeImage.cy;

	int xOffset = (m_sizeButton.cx - nWidth  - 1) >> 1;
	int yOffset = (m_sizeButton.cy - nHeight - 1) >> 1;

	GetClipBox(hDC, &rcClip);

	for(int i = 0; i < GetItemCount(); i++)
	{
		GetItemRect(i, &rcBtn);
		FillRect(hDC, &rcBtn, m_hBrushGray);
//		if(!rcTest.IntersectRect(&rcClip, &rcBtn))
//			continue;

		GetButtonInfo(i, nID, nStyle, iImage);

		if(nStyle & TBBS_SEPARATOR)
		{
			if(bHorz)
			{
				hPenOld = (HPEN)SelectObject(hDC, m_hPenDgray);
				MoveToEx(hDC, rcBtn.left + 3, rcBtn.top, NULL);
				LineTo(hDC, rcBtn.left + 3, rcBtn.bottom);

				SelectObject(hDC, m_hPenWhite);
				MoveToEx(hDC, rcBtn.left + 4, rcBtn.top, NULL);
				LineTo(hDC, rcBtn.left + 4, rcBtn.bottom);
			
				SelectObject(hDC, hPenOld);
			}
			else
			{
				hPenOld = (HPEN)SelectObject(hDC, m_hPenDgray);
				MoveToEx(hDC, 0, rcBtn.bottom+1, NULL);
				LineTo(hDC, rcBtn.right, rcBtn.bottom+1);

				SelectObject(hDC, m_hPenWhite);
				MoveToEx(hDC, 0, rcBtn.bottom+2, NULL);
				LineTo(hDC, rcBtn.right, rcBtn.bottom+2);
			
				SelectObject(hDC, hPenOld);
			}

			continue;
		}

		if(nStyle & (TBBS_PRESSED | TBBS_CHECKED))
		{
			hPenOld = (HPEN)SelectObject(hDC, m_hPenDgray);
			MoveToEx(hDC, rcBtn.right-1, rcBtn.top, NULL);
			LineTo(hDC, rcBtn.left, rcBtn.top);
			LineTo(hDC, rcBtn.left, rcBtn.bottom);

			SelectObject(hDC, m_hPenWhite);
			MoveToEx(hDC, rcBtn.left, rcBtn.bottom-1, NULL);
			LineTo(hDC, rcBtn.right-1, rcBtn.bottom-1);
			LineTo(hDC, rcBtn.right-1, rcBtn.top-1);
			
			SelectObject(hDC, hPenOld);
			rcBtn.OffsetRect(1, 1);
		}

		if(nStyle & TBBS_PRESSED || !(nStyle & TBBS_DISABLED))
		{
			BitBlt(hDC, rcBtn.left + xOffset, rcBtn.top + yOffset,
				nWidth, nHeight, hDCImg, iImage * nWidth, 0, SRCCOPY);

			if(nStyle & TBBS_PRESSED)
				continue;
		}

		if(nStyle & (TBBS_DISABLED | TBBS_INDETERMINATE))
		{
			hbmMonoOld = (HBITMAP)SelectObject(hDCMsk, hbmMono);
			PatBlt(hDCMsk, 0, 0, nWidth, nHeight, WHITENESS);

			// gray to transparent
			SetBkColor(hDCImg, (COLORREF)::GetSysColor(COLOR_BTNFACE));
			BitBlt(hDCMsk, 0, 0, nWidth, nHeight, hDCImg, iImage*nWidth, 0, SRCCOPY);

			// white to transparent  // colors other than white and gray to black
			SetBkColor(hDCImg, (COLORREF)::GetSysColor(COLOR_BTNHILIGHT));
			BitBlt(hDCMsk, 0, 0, nWidth, nHeight, hDCImg, iImage*nWidth, 0, SRCPAINT);

			SetTextColor(hDC, (COLORREF)0L);
			SetBkColor(hDC, (COLORREF)0x00FFFFFFL);

			if(nStyle & TBBS_DISABLED)
			{
				// black to white // gray to gray
				hbrOld = (HBRUSH)SelectObject(hDC, m_hBrushWhite);
				BitBlt(hDC, rcBtn.left+xOffset+1, rcBtn.top+yOffset+1, nWidth, nHeight, hDCMsk, 0, 0, ROP_PSDPxax);
				SelectObject(hDC, hbrOld);
			}

			// black to dark gray
			hbrOld = (HBRUSH)SelectObject(hDC, m_hBrushDgray);
			BitBlt(hDC, rcBtn.left+xOffset, rcBtn.top+yOffset, nWidth, nHeight, hDCMsk, 0, 0, ROP_PSDPxax);

			SelectObject(hDC, hbrOld);
			SelectObject(hDCMsk, hbmMonoOld);
		}

		if(nStyle & (TBBS_CHECKED | TBBS_INDETERMINATE))
		{
			hbrOld = (HBRUSH)SelectObject(hDC, m_hBrushDither);
			SetTextColor(hDC, (COLORREF)0L);
			SetBkColor(hDC, (COLORREF)0x00FFFFFFL);

			hbmMonoOld = (HBITMAP)SelectObject(hDCMsk, hbmMono);
			PatBlt(hDCMsk, 0, 0, rcBtn.Width()-2, rcBtn.Height()-2, WHITENESS);

			// gray to transparent
			SetBkColor(hDCImg, (COLORREF)::GetSysColor(COLOR_BTNFACE));
			BitBlt(hDCMsk, xOffset-1, yOffset-1, nWidth, nHeight, hDCImg, iImage*nWidth, 0, SRCCOPY);

			BitBlt(hDC, rcBtn.left+1, rcBtn.top+1, rcBtn.Width()-4, rcBtn.Height()-4,
				hDCMsk, 0, 0, ROP_DSPDxax);
	
			SelectObject(hDC, hbrOld);
			SelectObject(hDCMsk, hbmMonoOld);
		}
	}

	SelectObject(hDCImg, hbmImgOld);
	DeleteObject(hbmMono);
}


/////////////////////////////////////////////////////////////////////////////
// CIceToolBar message handlers

void CIceToolBar::OnPaint() 
{
	BOOL bHorz = TRUE;

	if (m_bDelayedButtonLayout)
	{
		m_bDelayedButtonLayout = FALSE;
		bHorz = (m_dwStyle & CBRS_ORIENT_HORZ) != 0;
		if ((m_dwStyle & CBRS_FLOATING) && (m_dwStyle & CBRS_SIZE_DYNAMIC))
			CalcDynamicLayout(0, LM_HORZ | LM_MRUWIDTH | LM_COMMIT);
		else if (bHorz)
			CalcDynamicLayout(0, LM_HORZ | LM_HORZDOCK | LM_COMMIT);
		else
			CalcDynamicLayout(0, LM_VERTDOCK | LM_COMMIT);
	}

	CPaintDC dc(this);
	HDC hDC = dc.GetSafeHdc();

	DrawButton(hDC, bHorz);
	m_nHitPrev = -1;
//	Default();

	// Do not call CToolBar::OnPaint() for painting messages
}

void CIceToolBar::OnNcPaint() 
{
	EraseNonClient();

	int x1, y1, x2, y2;
	HDC       hDC;
	HPEN      hPenOld;
	CRect     rcWindow;
	CWindowDC dc(this);

	// Draw two top or left lines.
	if(!IsFloating())
	{
		GetWindowRect( &rcWindow);
		ScreenToClient(&rcWindow);
		hDC = dc.GetSafeHdc();

		if(m_dwStyle & (CBRS_ALIGN_TOP | CBRS_ALIGN_BOTTOM))
		{
			rcWindow.OffsetRect(-rcWindow.left + 3, -rcWindow.top + 3);

			x1 = rcWindow.left;
			y1 = rcWindow.top;
			y2 = rcWindow.bottom - 7;

			hPenOld = (HPEN)SelectObject(hDC, m_hPenWhite);
			if(m_nBarGripper == 1 || m_nBarGripper == 2)
			{
				MoveToEx(hDC, x1 + 1, y1, NULL);
				LineTo(hDC, x1, y1);
				LineTo(hDC, x1, y2);
				if(m_nBarGripper == 2)
				{
					MoveToEx(hDC, x1 + 5, y1, NULL);
					LineTo(hDC, x1 + 4, y1);
					LineTo(hDC, x1 + 4, y2);
				}
				SelectObject(hDC, m_hPenDgray);
				MoveToEx(hDC, x1+2, y1, NULL);
				LineTo(hDC, x1+2, y2);
				LineTo(hDC, x1-1, y2);
				if(m_nBarGripper == 2)
				{
					MoveToEx(hDC, x1 + 6, y1, NULL);
					LineTo(hDC, x1 + 6, y2);
					LineTo(hDC, x1 + 3, y2);
				}
			}
			SelectObject(hDC, hPenOld);
		}
		else if(m_dwStyle & (CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT))
		{
			rcWindow.OffsetRect(-rcWindow.left + 3, -rcWindow.top + 3);

			x1 = rcWindow.left;
			y1 = rcWindow.top;
			x2 = rcWindow.right - 7;

			hPenOld = (HPEN)SelectObject(hDC, m_hPenWhite);
			if(m_nBarGripper == 1 || m_nBarGripper == 2)
			{
				MoveToEx(hDC, x1, y1+1, NULL);
				LineTo(hDC, x1, y1);
				LineTo(hDC, x2, y1);
				if(m_nBarGripper == 2)
				{
					MoveToEx(hDC, x1, y1+5, NULL);
					LineTo(hDC, x1, y1 + 4);
					LineTo(hDC, x2, y1 + 4);
				}	
				SelectObject(hDC, m_hPenDgray);
				MoveToEx(hDC, x1, y1+2, NULL);
				LineTo(hDC, x2, y1+2);
				LineTo(hDC, x2, y1);
				if(m_nBarGripper == 2)
				{
					MoveToEx(hDC, x1, y1+6, NULL);
					LineTo(hDC, x2, y1+6);
					LineTo(hDC, x2, y1+4);
				}
			}
			SelectObject(hDC, hPenOld);
		}
	}
	
	// Do not call CToolBar::OnNcPaint() for painting messages
}

void CIceToolBar::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp) 
{
	// calculate border space (will add to top/bottom, subtract from right/bottom)
	CRect rect; rect.SetRectEmpty();
	BOOL bHorz = (m_dwStyle & CBRS_ORIENT_HORZ) != 0;
	CControlBar::CalcInsideRect(rect, bHorz);
	ASSERT(rect.top >= 2);

	// We need some space to draw line
	if(!IsFloating())
	{
		if(m_dwStyle & (CBRS_ALIGN_TOP | CBRS_ALIGN_BOTTOM))
		{
			if(m_nBarGripper == 1)
			{
				rect.left  += 2;
				rect.right += 2;
			}
			else if(m_nBarGripper == 2)
			{
				rect.left  += 4;
				rect.right += 4;
			}
		}
		else
		{
			if(m_nBarGripper == 1)
			{
				rect.top    += 2;
				rect.bottom += 2;
			}
			else if(m_nBarGripper == 2)
			{
				rect.top    += 4;
				rect.bottom += 4;
			}
		}
	}

	// adjust non-client area for border space
	lpncsp->rgrc[0].left   += rect.left;
	lpncsp->rgrc[0].top    += rect.top - 2;
	lpncsp->rgrc[0].right  += rect.right;
	lpncsp->rgrc[0].bottom += rect.bottom;

	// TODO: Add your message handler code here and/or call default
    // CToolBar::OnNcCalcSize(bCalcValidRects, lpncsp);
}

void CIceToolBar::OnWindowPosChanging(WINDOWPOS FAR* lpwndpos) 
{
    // We will get WM_NCCALCSIZE

	// not necessary to invalidate the borders
	DWORD dwStyle = m_dwStyle;
	m_dwStyle &= ~(CBRS_BORDER_ANY);
	lpwndpos->flags |= SWP_FRAMECHANGED;
    CToolBar::OnWindowPosChanging(lpwndpos);
	m_dwStyle = dwStyle;

	// If we can resize while floating
	if (dwStyle & CBRS_SIZE_DYNAMIC)
	{
		// And we are resizing
//		if (lpwndpos->flags & SWP_NOSIZE)
//			return;

		// Then redraw the buttons
		//InvalidateRect(NULL);
		Invalidate();
	}

    // Also post WM_NCPAINT!!! 
    PostMessage(WM_NCPAINT);
}

UINT CIceToolBar::OnNcHitTest(CPoint point) 
{
	CRect     rect;
	TBBUTTON  tb;
	int       nHit  = -1;
	HDC       hDC   = ::GetDC(m_hWnd);

	CPoint ptCur;
	GetCursorPos(&ptCur);
	GetWindowRect(&rect);

	if(!rect.PtInRect(ptCur))
	{
		nHit = -1;
	}
	else
	{
		ScreenToClient(&point);
		int nCount = GetItemCount();
		for(int i = 0; i < nCount; i++)
		{
			GetItemRect(i, &rect);
			if(rect.PtInRect(point))
				break;
		}
		nHit = (i == nCount) ? -1: i;
	}

	if(nHit >= 0 && nHit != m_nHitPrev)
	{
		if(m_bTimer == 0)
			m_bTimer = SetTimer(3567, 100, NULL);

		if(m_nHitPrev >= 0)
		{
			// Get button information
			GetItemRect(m_nHitPrev, &rect);
			DefWindowProc(TB_GETBUTTON, m_nHitPrev, (LPARAM)&tb);

			if(tb.fsState & (TBSTATE_CHECKED | TBSTATE_INDETERMINATE))
				InvalidateRect(&rect);
			else
				DrawButtonBorder(hDC, rect, 1);
		}

		m_nHitPrev = -1;

		// Get button information
		DefWindowProc(TB_GETBUTTON, nHit, (LPARAM)&tb);
		if(!(tb.fsStyle & TBSTYLE_SEP) && (tb.fsState & TBSTATE_ENABLED))
		{
			m_nHitPrev = nHit;
			GetItemRect(nHit, &rect);
			int xOffset = (m_sizeButton.cx - m_sizeImage.cx - 1) >> 1;
			int yOffset = (m_sizeButton.cy - m_sizeImage.cy - 1) >> 1;

			if(tb.fsState & TBSTATE_CHECKED)
			{
				rect.DeflateRect(1, 1);
				HBITMAP hbmOld = (HBITMAP)SelectObject(hDCImg, m_hbmImageWell);
				FillRect(hDC, &rect, m_hBrushGray);
				BitBlt(hDC, rect.left + xOffset, rect.top + yOffset, m_sizeImage.cx,
					m_sizeImage.cy, hDCImg, tb.iBitmap * m_sizeImage.cx, 0, SRCCOPY);
				SelectObject(hDCImg, hbmOld);
			}
			else if(tb.fsState & TBSTATE_INDETERMINATE)
			{
				HBITMAP hbmOld = (HBITMAP)SelectObject(hDCImg, m_hbmImageWell);
				FillRect(hDC, &rect, m_hBrushGray);
				BitBlt(hDC, rect.left + xOffset, rect.top + yOffset, m_sizeImage.cx,
					m_sizeImage.cy, hDCImg, tb.iBitmap * m_sizeImage.cx, 0, SRCCOPY);
				SelectObject(hDCImg, hbmOld);
				DrawButtonBorder(hDC, rect, 2);
			}
			else
			{
				DrawButtonBorder(hDC, rect, 2);
			}
		}
	}
	else if(nHit < 0 && m_nHitPrev >= 0)
	{
		GetItemRect(m_nHitPrev, &rect);
		DefWindowProc(TB_GETBUTTON, m_nHitPrev, (LPARAM)&tb);

		if(tb.fsState & (TBSTATE_CHECKED | TBSTATE_INDETERMINATE))
			InvalidateRect(&rect);
		else
			DrawButtonBorder(hDC, rect, 1);

		m_nHitPrev = -1;
	}
	::ReleaseDC(m_hWnd, hDC);

	return HTCLIENT;

	// TODO: Add your message handler code here and/or call default
    // return CToolBar::OnNcHitTest(point);
}

void CIceToolBar::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent == 3567)
	{
		CRect  rect;
		CPoint point;
		GetCursorPos(&point);
		GetWindowRect(&rect);

		if(!rect.PtInRect(point) && m_nHitPrev >= 0)
		{
			TBBUTTON tb;
			HDC hDC = ::GetDC(m_hWnd);
			GetItemRect(m_nHitPrev, &rect);
			DefWindowProc(TB_GETBUTTON, m_nHitPrev, (LPARAM)&tb);

			if(tb.fsState & (TBSTATE_CHECKED | TBSTATE_INDETERMINATE))
				InvalidateRect(&rect);
			else
				DrawButtonBorder(hDC, rect, 1);

			m_nHitPrev = -1;

			ASSERT(KillTimer(3567));
			m_bTimer = FALSE;
		}
	}

	CToolBar::OnTimer(nIDEvent);
}

void CIceToolBar::OnSysColorChange() 
{
	CToolBar::OnSysColorChange();

	DeleteObject(m_hPenWhite);
	DeleteObject(m_hPenDgray);
	DeleteObject(m_hPenGray);
	DeleteObject(m_hBrushGray);
	DeleteObject(m_hBrushDgray);
	DeleteObject(m_hBrushWhite);
	DeleteObject(m_hBrushDither);

	DeleteDC(hDCImg);
	DeleteDC(hDCMsk);

	m_hPenGray  = CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_BTNFACE));
	m_hPenWhite = CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_BTNHILIGHT));
	m_hPenDgray = CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_BTNSHADOW));
	m_hBrushGray  = CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));
	m_hBrushDgray = CreateSolidBrush(::GetSysColor(COLOR_BTNSHADOW));
	m_hBrushWhite = CreateSolidBrush(::GetSysColor(COLOR_BTNHILIGHT));

	HBITMAP hbmGray = ::CreateDitherBitmap();
	if (hbmGray != NULL)
	{
		m_hBrushDither = ::CreatePatternBrush(hbmGray);
		DeleteObject((HGDIOBJ*)&hbmGray);
	}

	hDCImg = CreateCompatibleDC(NULL);
	hDCMsk = CreateCompatibleDC(NULL);
	
}
