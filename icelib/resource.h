//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ice.rc
//
#define IDR_MAINFRAME                   128
#define IDB_MENUCHECK                   131
#define IDR_MRUFILE                     149
#define IDR_RESERVOIR                   150
#define IDR_SYSTEMBAR                   151
#define IDR_SDIFRAME                    152
#define ID_VIEW_WORKSPACE_PANE          32771
#define ID_VIEW_OUTPUT_PANE             32773
#define ID_BUILD_COMPILE_CPP            32783
#define ID_BUILD_COMPILE_JAVA           32784
#define ID_PROJECT_OPEN                 32785
#define ID_PROJECT_SAVE                 32787
#define ID_DEBUG_GO                     32789
#define ID_DEBUG_STOP                   32790
#define ID_SCRIPT_NEW                   32791
#define ID_SCRIPT_RUN                   32793
#define ID_DEBUG_BREAKPOINTS            32794
#define ID_BUILD_RUN                    32796
#define ID_BUILD_COMPILE                32797

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        106
#define _APS_NEXT_COMMAND_VALUE         40002
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
