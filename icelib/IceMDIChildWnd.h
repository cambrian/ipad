#if !defined(AFX_ICEMDICHILDWND_H__D4AD6CC0_51ED_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICEMDICHILDWND_H__D4AD6CC0_51ED_11D2_852E_00A024E0E339__INCLUDED_

#include "IceMenu.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceMDIChildWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceMDIChildWnd frame

class CIceMDIChildWnd : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CIceMDIChildWnd)
protected:
	CIceMDIChildWnd();           // protected constructor used by dynamic creation

// Attributes
public:
	CIceMenu	iceMenu;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceMDIChildWnd)
	public:
	virtual void ActivateFrame(int nCmdShow = -1);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CIceMDIChildWnd();

	// Generated message map functions
	//{{AFX_MSG(CIceMDIChildWnd)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg LRESULT OnMenuChar(UINT nChar, UINT nFlags, CMenu* pMenu);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICEMDICHILDWND_H__D4AD6CC0_51ED_11D2_852E_00A024E0E339__INCLUDED_)
