#if !defined(AFX_ICESTATUSBAR_H__E0DF03E1_531B_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICESTATUSBAR_H__E0DF03E1_531B_11D2_852E_00A024E0E339__INCLUDED_

#include "ISBPaneInfo.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceStatusBar.h : header file
//

#define IDC_JRLIB_STATUSBAR_TIMER	20000


/////////////////////////////////////////////////////////////////////////////
// CIceStatusBar window

class CIceStatusBar : public CStatusBar
{
// Construction
public:
	CIceStatusBar();

// Attributes
public:
	static CIceStatusBar	*aktBar;

	CArray<ISBPaneInfo, ISBPaneInfo&>			paneInfo;	// Liste mit den Zusatzinformationen
	CMap<int, int, ISBPaneInfo, ISBPaneInfo&>	buffer;		// Zwischenpuffer f�r jeweils ein Pane
	UINT									timerID;	// ID des Refresh-Timers
	bool									on;			// Aktueller Pane aktiv oder inaktiv?
	CWnd									*pParent;	// Vaterfenster der Statusbar

	bool			CreateStatusBar(CWnd *pParent, const UINT* lpIDArray, int nIDCount, UINT pane0Style = SBPS_STRETCH | SBPS_NOBORDERS);
	ISBPaneInfo&		GetISBPaneInfo(int ix);
	CProgressCtrl	*GetProgressCtrl(int ix)				{ return GetISBPaneInfo(ix).GetProgressCtrl();		}
	void			SavePane(int ix);
	void			RestorePane(int ix);
	int				GetPaneAtPosition(CPoint& point);

// Operations
public:
	void SetFgColor(int ix, COLORREF on, COLORREF off = -1)	{ _XPI(ix).SetFgColor(on, off);	Invalidate(FALSE);	}
	void SetBkColor(int ix, COLORREF on, COLORREF off = -1)	{ _XPI(ix).SetBkColor(on, off);	Invalidate(FALSE);	}
	void SetBitmap(int ix, LPCSTR on, LPCSTR off = "")		{ _XPI(ix).SetBitmap(on, off);	Invalidate(FALSE);	}
	void SetText(int ix, LPCSTR on, LPCSTR off = "")		{ _XPI(ix).SetText(on, off);	Invalidate(FALSE);	}
	void SetNumber(int ix, int on, int off = 0)				{ _XPI(ix).SetNumber(on, off);	Invalidate(FALSE);	}
	void SetFont(int ix, LOGFONT& newFont)					{ _XPI(ix).SetFont(newFont);	Invalidate(FALSE);	}
	void SetFont(int ix, CFont& newFont)					{ _XPI(ix).SetFont(newFont);	Invalidate(FALSE);	}
	void SetFont(int ix, CFont *newFont)					{ _XPI(ix).SetFont(newFont);	Invalidate(FALSE);	}
	void SetFont(int ix, LPCSTR name, int size)				{ _XPI(ix).SetFont(name, size);	Invalidate(FALSE);	}
	void SetFontSize(int ix, int size)						{ _XPI(ix).SetFontSize(size);	Invalidate(FALSE);	}
	void SetFontName(int ix, LPCSTR name)					{ _XPI(ix).SetFontName(name);	Invalidate(FALSE);	}
	void SetMode(int ix, int newMode)						{ _XPI(ix).SetMode(newMode); 	Invalidate(FALSE);	}
	void SetRange(int ix, int nLow, int nUp)				{ _XPI(ix).SetRange(nLow, nUp);	Invalidate(FALSE);	}

	void EnablePane(int ix, bool enable = true)				{ SetPaneStyle(ix, enable ? 0 : SBPS_DISABLED);		}
	void CheckKey(int ix, int nVirtKey)						{ EnablePane(ix, ::GetKeyState(nVirtKey) & 1);			}

	int  Increment(int ix, bool on = true)					{ int ret = _XPI(ix).Increment(on);	Invalidate(FALSE); return ret; }
	int  Decrement(int ix, bool on = true)					{ int ret = _XPI(ix).Decrement(on);	Invalidate(FALSE); return ret; }
	int	 SetPos(int ix, int nPos)							{ return _XPI(ix).SetPos(nPos);		}
	int	 OffsetPos(int ix, int nPos)						{ return _XPI(ix).OffsetPos(nPos);	}
	int	 SetStep(int ix, int nStep)							{ return _XPI(ix).SetStep(nStep);	}
	int	 StepIt(int ix)										{ return _XPI(ix).StepIt();			}
	UINT SetStyle(int ix, UINT style);
	int  SetWidth(int ix, int width);

	COLORREF	GetFgColor(int ix, bool on = true)			{ return _XPI(ix).GetFgColor(on);	}
	COLORREF	GetBkColor(int ix, bool on = true)			{ return _XPI(ix).GetBkColor(on);	}
	CString		GetBitmap(int ix, bool on = true)			{ return _XPI(ix).GetBitmap(on);	}
	CString		GetText(int ix, bool on = true)				{ return _XPI(ix).GetText(on);		}
	int			GetNumber(int ix, bool on = true)			{ return _XPI(ix).GetNumber(on);	}

	LOGFONT&	GetFont(int ix)								{ return _XPI(ix).GetFont();		}
	CString		GetFontName(int ix)							{ return _XPI(ix).GetFontName();	}
	int			GetFontSize(int ix)							{ return _XPI(ix).GetFontSize();	}
	int			GetMode(int ix)								{ return _XPI(ix).GetMode();		}
	bool		IsHScroll(int ix)							{ return _XPI(ix).IsHScroll();		}
	bool		IsVScroll(int ix)							{ return _XPI(ix).IsVScroll();		}
	UINT		GetStyle(int ix);
	int			GetWidth(int ix);
	UINT		GetID(int ix);

protected:
	ISBPaneInfo& _XPI(int ix)									{ return GetISBPaneInfo(ix);			}
	void DrawSizing(CDC *pDC);
	void DrawTextPane(CDC *pDC, int ix, CRect& rect, ISBPaneInfo& aktPane);
	void DrawBitmapPane(CDC *pDC, int ix, CRect& rect, ISBPaneInfo& aktPane);
	void DrawProgressPane(CDC *pDC, int ix, CRect& rect, ISBPaneInfo& aktPane);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceStatusBar)
	virtual BOOL SetIndicators(const UINT* lpIDArray, int nIDCount);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIceStatusBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceStatusBar)
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnPaint();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICESTATUSBAR_H__E0DF03E1_531B_11D2_852E_00A024E0E339__INCLUDED_)
