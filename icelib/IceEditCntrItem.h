// CntrItem.h : interface of the CIceEditCntrItem class
//

#if !defined(AFX_CNTRITEM_H__C7AA0616_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_)
#define AFX_CNTRITEM_H__C7AA0616_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CIceEditDoc;
class CIceEditView;

class CIceEditCntrItem : public CRichEditCntrItem
{
	DECLARE_SERIAL(CIceEditCntrItem)

// Constructors
public:
	CIceEditCntrItem(REOBJECT* preo = NULL, CIceEditDoc* pContainer = NULL);
		// Note: pContainer is allowed to be NULL to enable IMPLEMENT_SERIALIZE.
		//  IMPLEMENT_SERIALIZE requires the class have a constructor with
		//  zero arguments.  Normally, OLE items are constructed with a
		//  non-NULL document pointer.

// Attributes
public:
	CIceEditDoc* GetDocument()
		{ return (CIceEditDoc*)CRichEditCntrItem::GetDocument(); }
	CIceEditView* GetActiveView()
		{ return (CIceEditView*)CRichEditCntrItem::GetActiveView(); }

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceEditCntrItem)
	public:
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	~CIceEditCntrItem();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CNTRITEM_H__C7AA0616_AFD1_11D2_8EFE_00A024E0E339__INCLUDED_)
