#if !defined(AFX_ICEFRAMEWND_H__BE152740_3768_11D3_93E5_00207817FF60__INCLUDED_)
#define AFX_ICEFRAMEWND_H__BE152740_3768_11D3_93E5_00207817FF60__INCLUDED_

#include "IceMenu.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IceFrameWnd.h : header file
//

/////////////////////////////////////////////////////////////////
////// strings for registry entry
static const TCHAR s_profileFrameWindow[] = _T("Frame Window"); // Profile Section Name
static const TCHAR s_profileIconic[] = _T("Iconic"); 
static const TCHAR s_profileMaximized[] = _T("Maximized"); 
static const TCHAR s_profileRectangle[] = _T("Rectangle"); 


/////////////////////////////////////////////////////////////////////////////
// CIceFrameWnd frame

class CIceFrameWnd : public CFrameWnd
{
	DECLARE_DYNCREATE(CIceFrameWnd)
protected:
	CIceFrameWnd();           // protected constructor used by dynamic creation

// Attributes
public:
	CIceMenu	iceMenu;
	CRect oldMenuBarRc;
	bool bMenuLooping;
	UINT uiTimerId;

private:
    BOOL  m_bFirstTime;

// Operations
public:


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceFrameWnd)
	public:
	virtual void RecalcLayout(BOOL bNotify = TRUE);
	virtual void ActivateFrame(int nCmdShow = -1);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CIceFrameWnd();

	// Generated message map functions
	//{{AFX_MSG(CIceFrameWnd)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg UINT OnNcHitTest(CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnInitMenu(CMenu* pMenu);
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg LRESULT OnMenuChar(UINT nChar, UINT nFlags, CMenu* pMenu);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnNcPaint();
	afx_msg void OnClose();
	//}}AFX_MSG
	afx_msg void OnEnterMenuLoop(BOOL bIsTrackPopupMenu);
	afx_msg void OnExitMenuLoop(BOOL bIsTrackPopupMenu);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICEFRAMEWND_H__BE152740_3768_11D3_93E5_00207817FF60__INCLUDED_)
